//const BASE_URL = "http://localhost:3000/";         // LOCAL
const BASE_URL = "http://52.33.73.91:3000/";       // LIVE
// const BASE_URL = "http://52.41.223.29:3000/";      // STAGING
const VERSION = Date.now();

const MAIL_FROM = 'Synrgy<synrgynotification@gmail.com>';
const MAIL_FROM_AUTH = 'synrgynotification@gmail.com';
const MAIL_PASSWORD = 'Synrgy@123456789';
const MAIL_HOST = 'smtp.gmail.com';
const MAIL_PORT = '465';
const MAIL_METHOD = 'SMTP';
const MAIL_SECURE = true;

const API_VERSION = 'v1';
const FB_DATABASE = 'https://fitnessapp-d4ed8.firebaseio.com';
const FB_DATABASE_KEY = '4hl5AGLvbUGgppj3whgtHE1ohlbRZo261kVDaOik';


const PROFILE_PIC = 'images/profile-pic/';
const COVER_PIC = 'images/cover-pic/';
const PDF = 'pdf/userInformation/';
const CERTIFICATE = 'certificates/';

const FCM_SERVER_KEY = 'AIzaSyASgNpeiyokoeVdCDHBBv2Muny_xEwrAqA';

const PAYTABS_CHARGES_PERCENTAGE = 3.33;
const SYNERGY_COMISSION = 0.00;

module.exports = {
    BASE_URL: BASE_URL,
    VERSION: VERSION,
    MAIL_FROM: MAIL_FROM,
    MAIL_FROM_AUTH: MAIL_FROM_AUTH,
    MAIL_PASSWORD: MAIL_PASSWORD,
    MAIL_HOST: MAIL_HOST,
    MAIL_PORT: MAIL_PORT,
    MAIL_METHOD: MAIL_METHOD,
    MAIL_SECURE: MAIL_SECURE,
    API_VERSION: API_VERSION,
    PROFILE_PIC: PROFILE_PIC,
    COVER_PIC: COVER_PIC,
    PDF: PDF,
    CERTIFICATE: CERTIFICATE,
    FCM_SERVER_KEY: FCM_SERVER_KEY,
    FB_DATABASE : FB_DATABASE,
    FB_DATABASE_KEY : FB_DATABASE_KEY,
    PAYTABS_CHARGES_PERCENTAGE : PAYTABS_CHARGES_PERCENTAGE,
    SYNERGY_COMISSION: SYNERGY_COMISSION
};