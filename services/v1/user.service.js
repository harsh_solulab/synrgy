const user = require('../../models/user.model');
const userPictures = require('../../models/user_pictures.model');
const follow = require('../../models/follow.model');
const notification = require('../../models/notification.model');
const subscriptionPlan = require('../../models/subscription_plan.model');
const userDashboard = require('../../models/user_dashboard.model');
const userPlan = require('../../models/user_plans.model');
const userSession = require('../../models/user_session.model');
var async = require('async');
const _ = require('lodash');
const bcrypt = require('bcryptjs');
const constants = require('../../config/constants');
const ptdtService = require('../../services/v1/ptdt.service');
const firebaseFunction = require('../../modules/firebase_functions');
const moment = require("moment");

exports.signup = function (req, callback) {
    var profilePic = "";
    if (req.file !== undefined) {
        profilePic = constants.BASE_URL + constants.PROFILE_PIC + req.file.filename;
    }
    async.parallel([
        function (hashCallback) {
            genPassHashs(req.body.password, function (arrData) {
                hashCallback(null, arrData);
            });
        },
        function (randomCallback) {
            genRandomStrings(5, function (arrData) {
                randomCallback(null, arrData);
            });
        }
    ], function (err, results) {
        password = results[0];
        verificationToken = results[1];
        var newUser = {
            nickname: req.body.nickname,
            email: req.body.email.toLowerCase(),
            password: password,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            gender: req.body.gender,
            dob: moment(req.body.dob).valueOf(),
            height: "",
            weight: "",
            country: req.body.country,
            city: req.body.city,
            location: {"type": "Point", "coordinates": [0.000, 0.000]},
            verificationToken: verificationToken,
            emailVerified: true,
            profilePic: profilePic,
            type: 0, // 0-user 1-dt 2-pt
            isCertifiedAndApproved: 1, // 0-inactive 1-active 2-declined
            activity: "",
            isBlocked: false,
            isDeleted: false,
            isFreezed: false,
        };

        // Save user.
        user(newUser).save(function (err, result) {
            if (err || result == null) {
                console.log(err);
                callback("err");
            } else {
                firebaseFunction.addUserInFirebase(result._id, function (data) {
                    callback(result);
                });
                //callback(result);
            }
        });
    });
};

// Generates a password hash.
var genPassHashs = function (plainTextPassword, callback) {
    const saltRounds = 10;
    bcrypt.genSalt(saltRounds, function (err, salt) {
        bcrypt.hash(plainTextPassword, salt, function (err, hash) {
            // Store hash in your password DB.
            callback(hash);
        });
    });
};
exports.genPassHashs = genPassHashs;

// Generates a random string.
var genRandomStrings = function (length, callback) {
    var text = "";
    var characterSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (var i = 0; i < length; i++) {
        text += characterSet.charAt(Math.floor(Math.random() * characterSet.length));
    }
    callback(Date.now() + text);
};
exports.genRandomStrings = genRandomStrings;


// Generates a random string.
function genRandomString(length) {
    var text = "";
    var characterSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (var i = 0; i < length; i++) {
        text += characterSet.charAt(Math.floor(Math.random() * characterSet.length));
    }
    return Date.now() + text;
}

// calculate age from dob
var calculateAge = function (dob, callback) {
    dob = new Date(dob);
    var today = new Date();
    var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
    callback(age);
};
exports.calculateAge = calculateAge;

var getUserInfo = function (req, callback) {
    user.findOne({'_id': req.body.userId}, {verificationToken: 0, password: 0}).populate('country city', 'name sortname').lean().exec(function (err, result) {
        if (err || result == null) {
            callback('err');
        } else {
            var stringDate = new Date(result.dob).toISOString().slice(0, 10);
            result.dob = stringDate;
            calculateAge(result.dob, function (result2) {
                result.age = result2;
            })
            if (req.body.isMine == 'true') {
                var data = result.accounts.filter((account) => {
                    return account.accessToken == req.authToken;
                })
                delete result.accounts;
                result.accounts = data;
                if (result.type > 0) {
                    ptdtService.chkPtDtSubscriptionExpiry(result._id, function (data) {
                        result.isSubscribed = data;
                        callback(result);
                    });
                } else {
                    callback(result);
                }
            } else if (req.body.isMine == 'noti') {
                callback(result);
            } else {
                follow.findOne({'followedBy': req.authenticationId, 'followedTo': req.body.userId}, function (err1, follower) {
                    if (err1) {
                        callback('err');
                    } else {
                        if (follower) {
                            result.isFollow = true;
                        } else {
                            result.isFollow = false;
                        }
                        delete result.accounts;
                        callback(result);
                    }
                })
            }
        }
    })
};
exports.getUserInfo = getUserInfo;

var verifyEmail = function (req, callback) {
    user.findOne({verificationToken: req.params.token}, function (err, result) {
        if (!result || err) {
            callback('err');
        } else {
            genRandomStrings(5, function (verificationToken) {
                user.findOneAndUpdate({_id: result._id}, {
                    $set: {
                        emailVerified: true,
                        verificationToken: verificationToken,
                    }
                }, {new : true},
                        function (err, result) {
                            if (err || result == null) {
                                callback('err');
                            } else {
//                            firebaseFunction.addUserInFirebase(result._id, function (data) {
//                                callback(result);
//                            });
                                callback(result)
                            }
                        })
            });
        }
    });
}
exports.verifyEmail = verifyEmail;

exports.signinData = async function (userData) {
    try {
        // check user.        
        var checkedUser = await user.findOne({'email': userData.email}).lean();
        return checkedUser;
    } catch (exception) {
        // return an error message describing the reason.
        throw Error("Error while getting data");
    }
};

exports.getPass = async function (userData) {
    try {
        // check user.        
        var checkedUser = await user.findOne({'email': userData.email}, 'password');
        return checkedUser;
    } catch (exception) {
        // return an error message describing the reason.
        throw Error("Error while getting data");
    }
};

exports.chkSignIn = async function (result, password, pass, deviceId, latitude, longitude, deviceType, fcmToken) {
    try {
        var accessToken = await genRandomString(5);
        var obj = {};
        if (result.emailVerified == false) {
            obj.error = 1;
            obj.msg = "Your email is not verified. Please verify your email to continue.";
            return obj;
        }
        if (result.isCertifiedAndApproved == 0) {
            obj.error = 1;
            obj.msg = "Your account is currently waiting for approval. Please contact administrator for further support.";
            return obj;
        }
        if (result.isCertifiedAndApproved == 2) {
            obj.error = 1;
            obj.msg = "Your registration request has been declined. Please contact administrator for further support";
            return obj;
        }
        if (result.isDeleted == true) {
            obj.error = 1;
            obj.msg = "You have deleted your account. To restore,please contact administrator for further support";
            return obj;
        }
        if (result.isBlocked == true) {
            obj.error = 1;
            obj.msg = "Your account has been blocked by administrator. Please contact administrator for further support";
            return obj;
        }
        var loc = {
            "type": "Point",
            //"coordinates": [parseFloat(longitude), parseFloat(latitude)]
            "coordinates": [0.00, 0.00]
        };
        if (result != null) {
            // if (result.emailVerified != false) {
            /* compare the password */
            if (bcrypt.compareSync(password, pass) === true) {
                var acc = {
                    loginType: "manual",
                    accessToken: accessToken,
                    deviceId: deviceId,
                    fcmToken: fcmToken,
                    googleId: null,
                    deviceType: deviceType,
                    location: loc
                };
                // UnFreezUser when login after freeze account
                if (result.isFreezed == true) {
                    user.updateOne({_id: result._id}, {$set: {isFreezed: false}, $unset: {accountFreezedDate: 1}}, {new : true}, function (err, unFreez) {
                        if (err || unFreez == null) {
                            obj.error = 1;
                            obj.msg = "Something Went Wrong";
                            return obj;
                        } else {
                            follow.updateMany({$or: [{followedBy: result._id}, {followedTo: result._id}]}, {$set: {isFreezed: false}}, function (err1, result1) {
                                if (err1 || result1 == null) {
                                    obj.error = 1;
                                    obj.msg = "Something Went Wrong";
                                    return obj;
                                } else {
                                    notification.updateMany({$or: [{to: result._id}, {for : result._id}]}, {$set: {isDeleted: false}}, function (err2, result2) {
                                        if (err2 || result2 == null) {
                                            obj.error = 1;
                                            obj.msg = "Something Went Wrong";
                                            return obj;
                                        } else {
                                            console.log('Success');
                                        }
                                    })
                                }
                            })
                        }
                    });
                }
                var updatedData = await user.findByIdAndUpdate(result._id, {$push: {accounts: acc}},
                        {new : true, fields: {password: 0, verificationToken: 0}})
                        .populate('city country', 'name sortname').lean();
                if (updatedData) {
                    var addTokenFB = await firebaseFunction.addTokenInFirebase(result._id);
                    // check PT/DT Subscription
                    if (updatedData.type > 0) {
                        var result1 = await ptdtService.chkPtDtSubscriptionExpiryAsync(updatedData._id);
                        updatedData.isSubscribed = result1;
                        delete updatedData.accounts;
                        updatedData.accounts = [acc];
                        obj.error = 0;
                        obj.data = updatedData;
                        return obj;
                    } else {
                        obj.error = 0;
                        delete updatedData.accounts;
                        updatedData.accounts = [acc];
                        obj.data = updatedData;
                        return obj;
                    }
                } else {
                    obj.data = updatedData;
                    obj.error = 1;
                    obj.msg = "something went wrong.";
                    return obj;
                }
            } else {
                obj.error = 1;
                obj.msg = "Invalid Password";
                return obj;
            }
            // } else {
            //     obj.error = 1;
            //     obj.msg = "Email not verified";
            //     return obj;
            // }
        } else {
            obj.error = 1;
            obj.msg = "User doesn't exist";
            return obj;
        }

    } catch (exception) {
        console.log(exception)
        // return an error message describing the reason.
        throw Error("Error while creating user");
    }
};

//Account will be freez if user not login till 6  month it will be deleted
exports.freezUserAccount = function (req, callback) {
    try {
        var freezDate = new Date();
        freezDate.setDate(freezDate.getDate() + 180);
        user.findOneAndUpdate({_id: req.authenticationId}, {$set: {isFreezed: true, accountFreezedDate: moment(freezDate).valueOf(), accounts: []}}, {new : true}, function (err, accountFreez) {
            if (accountFreez) {
                follow.updateMany({$or: [{followedBy: req.authenticationId}, {followedTo: req.authenticationId}]}, {$set: {isFreezed: true}}, function (err1, result) {
                    if (result) {
                        notification.updateMany({$or: [{to: req.authenticationId}, {for : req.authenticationId}]}, {$set: {isDeleted: true}}, function (err2, result1) {
                            if (result1) {
                                callback(null, accountFreez);
                            } else {
                                callback('err', null);
                            }
                        })
                    } else {
                        callback('err', null);
                    }
                })
            } else {
                callback('err', null);
            }
        });
    } catch (exception) {
        console.log(exception)
        // return an error message describing the reason.
        throw Error("Error while freezing account");
    }
};

//Instant delete account
exports.deleteUserAccount = function (req, callback) {
    try {
        var deletedDate = new Date();
        deletedDate.setDate(deletedDate.getDate() + 180);
        user.findOneAndUpdate({_id: req.authenticationId}, {$set: {isDeleted: true, accounts: [], accountDeletedDate: moment(deletedDate).valueOf()}}, {new : true}, function (err, accountDeleted) {
            if (accountDeleted) {
                follow.updateMany({$or: [{followedBy: req.authenticationId}, {followedTo: req.authenticationId}]}, {$set: {isDeleted: true}}, function (err1, result) {
                    if (result) {
                        notification.updateMany({$or: [{to: req.authenticationId}, {for : req.authenticationId}]}, {$set: {isDeleted: true}}, function (err2, result1) {
                            if (result1) {
                                callback(null, accountDeleted);
                            } else {
                                callback('err', null);
                            }
                        })
                    } else {
                        callback('err', null);
                    }
                })
            } else {
                callback('err', null);
            }
        });
    } catch (exception) {
        console.log(exception)
        // return an error message describing the reason.
        throw Error("Error while Deleteing account");
    }
};

exports.forgotPwd = function (req, callback) {
    try {
        if (req.body.email != undefined) {
            user.findOne({email: req.body.email}, function (err, result) {
                if (!result || err) {
                    callback('err');
                } else {
                    callback(result);
                }
            })
        } else {
            callback('err');
        }
    } catch (exception) {
        console.log(exception)
        // return an error message describing the reason.
        throw Error("Error while forgot password");
    }
}

exports.checkForgotPwdToken = function (req, callback) {
    try {
        user.findOne({verificationToken: req.params.token, isDeleted: false}, function (err, result) {
            if (!result || err) {
                callback('err');
            } else {
                callback(result);
            }
        })
    } catch (exception) {
        console.log(exception)
        // return an error message describing the reason.
        throw Error("Error while checking forgot password token");
    }
}

//reset password while enter password and confirm password
exports.resetPwd = function (req, callback) {
    try {
        if (req.body.newpwd != undefined) {
            //update password and verificationToken
            genPassHashs(req.body.newpwd, function (arrData) {
                genRandomStrings(5, function (verificationToken) {
                    user.findOneAndUpdate({_id: req.params.userId, verificationToken: req.params.token},
                            {
                                $set: {password: arrData, verificationToken: verificationToken},
                                $pull: {accounts: {accessToken: {$ne: req.authToken}}}
                            },
                            function (err, result) {
                                if (err) {
                                    callback('err');
                                }
                                if (!result) {
                                    callback('err');
                                } else {
                                    console.log("Result", result);
                                    callback(result);
                                }
                            })
                })
            });
        } else {
            callback('err');
        }
    } catch (exception) {
        console.log(exception)
        // return an error message describing the reason.
        throw Error("Error while reset password");
    }
}
// Logout funtion remove token for specific user and according to device id 
exports.logout = function (req, callback) {
    try {
        if (req.body.deviceId == undefined) {
            callback(err, null);
        } else {
            user.findOneAndUpdate({_id: req.authenticationId}, {$pull: {accounts: {accessToken: req.authToken, deviceId: req.body.deviceId}}}, {new : true}, function (err, logedOut) {
                console.log('Error............Auth Id..............DeviceID............Logout...........accesstoken', err, req.authenticationId, req.body.deviceId, logedOut, req.authToken);
                if (err || logedOut == null) {
                    callback('err', null);
                } else {
                    firebaseFunction.addTokenInFirebase(req.authenticationId);
                    callback(null, logedOut);
                }
            });
        }
    } catch (exception) {
        console.log(exception)
        // return an error message describing the reason.
        throw Error("Error while logout");
    }
};

exports.addMeasurement = function (req, callback) {
    try {
        user.findOneAndUpdate({_id: req.authenticationId}, {$set: {isMeasurements: true}, measurements: {height: req.body.height, weight: req.body.weight, activity: req.body.activity}}, function (err, measurements) {
            if (err || measurements == null) {
                callback('err', null);
            } else {
                callback(null, measurements);
            }
        });
    } catch (exception) {
        console.log(exception)
        // return an error message describing the reason.
        throw Error("Error while adding measurements");
    }
};

var saveUserImage = function (data, callback) {
    userPictures(data).save(function (err, result) {
        if (err || result == null) {
            console.log("picture", err);
            callback("err");
        } else {
            callback(result);
        }
    });
};
exports.saveUserImage = saveUserImage;

var updateUser = function (req, callback) {
    var obj = {
        nickname: req.body.nickname,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        gender: req.body.gender,
        dob: moment(req.body.dob).valueOf(),
        country: req.body.country,
        city: req.body.city,

    };
    if (req.file !== undefined) {
        obj.profilePic = constants.BASE_URL + constants.PROFILE_PIC + req.file.filename;
    }
    user.findOneAndUpdate({_id: req.authenticationId},
            {$set: obj}, {new : true, fields: {password: 0, verificationToken: 0}})
            .lean().populate('city country', 'name sortname').exec(function (err, result) {
        if (err || result == null) {
            console.log(err)
            callback("err", null);
        } else {
            var data = result.accounts.filter((account) => {
                return account.accessToken == req.authToken;
            })
            delete result.accounts;
            result.accounts = data;
            if (req.file !== undefined) {
                var saveImage = {
                    userId: req.authenticationId,
                    image: constants.BASE_URL + constants.PROFILE_PIC + req.file.filename,
                };
                saveUserImage(saveImage, function (data) {
                    if (data == "err") {
                        callback('err', null);
                    } else {
                        callback(null, result);
                    }
                });
            } else {
                callback(null, result);
            }
        }
    });
};
exports.updateUser = updateUser;

var saveDashboardData = function (req, callback) {
    var type = parseInt(req.body.type);   //0-week,1-month
    var category = req.body.category;   //weight ,heart,fat,steps,distance,calories,all

    var data = JSON.parse(req.body.data);
    var dashboardObj = {};
    if (type == 0) {
        if (category == "weight") {
            dashboardObj.weightWeekData = data.weight;
        } else if (category == "heart") {
            dashboardObj.heartRateWeekData = data.heartRate;
        } else if (category == "fat") {
            dashboardObj.bodyFatWeekData = data.bodyFat;
        } else if (category == "steps") {
            dashboardObj.stepsWeekData = data.steps;
        } else if (category == "distance") {
            dashboardObj.distanceWeekData = data.distance;
        } else if (category == "calories") {
            dashboardObj.caloriesWeekData = data.calories;
        } else {
            dashboardObj.weightWeekData = data.weight;
            dashboardObj.heartRateWeekData = data.heartRate;
            dashboardObj.bodyFatWeekData = data.bodyFat;
            dashboardObj.stepsWeekData = data.steps;
            dashboardObj.distanceWeekData = data.distance;
            dashboardObj.caloriesWeekData = data.calories;
        }
    } else {
        if (category == "weight") {
            dashboardObj.weightMonthData = data.weight;
        } else if (category == "heart") {
            dashboardObj.heartRateMonthData = data.heartRate;
        } else if (category == "fat") {
            dashboardObj.bodyFatMonthData = data.bodyFat;
        } else if (category == "steps") {
            dashboardObj.stepsMonthData = data.steps;
        } else if (category == "distance") {
            dashboardObj.distanceMonthData = data.distance;
        } else if (category == "calories") {
            dashboardObj.caloriesMonthData = data.calories;
        } else {
            dashboardObj.weightMonthData = data.weight;
            dashboardObj.heartRateMonthData = data.heartRate;
            dashboardObj.bodyFatMonthData = data.bodyFat;
            dashboardObj.stepsMonthData = data.steps;
            dashboardObj.distanceMonthData = data.distance;
            dashboardObj.caloriesMonthData = data.calories;
        }
    }

    console.log("dashboardObj==============>", dashboardObj);

    userDashboard.findOneAndUpdate({'userId': req.authenticationId}, {$set: dashboardObj}, {upsert: true, new : true, strict: true}, function (err, result) {
        if (err || result == null) {
            console.log(err);
            callback('err', null);
        } else {
            callback(null, result);
        }
    })
}
exports.saveDashboardData = saveDashboardData;

var getDashboardData = function (req, callback) {
    userDashboard.findOne({'userId': req.body.userId}, function (err, result) {
        if (err) {
            callback('err', null);
        } else {
            if (result != null) {
                callback(null, result);
            } else {
                callback(null, {});
            }
        }
    })
}
exports.getDashboardData = getDashboardData;

var userDashboardsList = function (req, callback) {
    // var page = parseInt(req.body.skip * req.body.limit);
    // var limit = parseInt(req.body.limit);
    userPlan.aggregate([
        {$match: {'ptdtId': req.authenticationId}},
        {$group: {'_id': '$userId'}},
        // { $skip: page },
        // { $limit: limit },
        {$lookup: {from: "user", localField: "_id", foreignField: "_id", as: "user"}},
        {$project: {'user._id': 1, 'user.firstName': 1, 'user.lastName': 1, 'user.profilePic': 1}},
        {$unwind: "$user"}
    ], function (err, result) {
        if (err) {
            console.log(err);
            callback('err', null);
        } else {
            callback(null, result);
            //            userPlan.distinct('userId', {ptdtId: req.authenticationId}, function (err1, result1) {
            //                if (err1) {
            //                    console.log(err1);
            //                    callback('err', null);
            //                } else {
            //                    var responseObj = {
            //                        result: result,
            //                        total_count: result1.length
            //                    };
            //                    callback(null, responseObj);
            //                }
            //            })
        }
    })
}
exports.userDashboardsList = userDashboardsList;

var updateUserSession = async function (req, callback) {
    var session = await userSession.findOne({'_id': req.body.sessionId}).lean();
    session.currentServerTime = _.now();
    var type = parseInt(req.body.type); // 1-start 2-pause 3-end
    if (type == 1) {
        if (session.status == 1) {
            callback("started", null);
        } else if (session.status == 3) {
            callback("ended", null);
        } else {
            var result = await updateSession(req, 1);
            callback(null, result);
        }
    } else if (type == 2) {
        if (session.status == 1) {
            var result = await updateSession(req, 2);
            callback(null, result);
        } else {
            callback("can't pause", session);
        }
    } else {
        if (session.status == 1) {
            var result = await updateSession(req, 3);
            callback(null, result);
        } else {
            callback("can't end", session);
        }
    }
}
exports.updateUserSession = updateUserSession;

var updateSession = async function (req, type) {
    try {
        if (type == 1) {
            var session = await userSession.findOne({'_id': req.body.sessionId}).lean();
            var momentOfTime = new Date();
            momentOfTime.setMilliseconds(momentOfTime.getMilliseconds() + session.remainingDuration);
            sessonObj = {
                status: 1,
                startTime: _.now(),
                endTime: moment(momentOfTime).valueOf()
            }
        } else if (type == 2) {
            var session = await userSession.findOne({'_id': req.body.sessionId}).lean();
            var startTime = new Date(session.startTime);
            var currentTime = new Date(Date.now());
            var timeProgress = Math.abs(currentTime.getTime() - startTime.getTime());
            var remainingDuration = session.remainingDuration - timeProgress;
            sessonObj = {
                status: 2,
                remainingDuration: remainingDuration,
            }
        } else {
            sessonObj = {
                status: 3,
                remainingDuration: 0,
            }
        }

        var result = await userSession.findOneAndUpdate({'_id': req.body.sessionId}, {$set: sessonObj}, {new : true}).populate('sessionId', 'title description').lean();
        // var session = await userSession.findOne({ '_id': req.body.sessionId }).lean();
        var sessionData = await userSession.find({userPlanId: result.userPlanId, status: {$lt: 3}})
        if (sessionData.length <= 0) {
            var updateSession = await userPlan.findOneAndUpdate({_id: result.userPlanId}, {$set: {isExpired: true}})
            console.log("updateSession", updateSession);
        }
        result.currentServerTime = _.now();
        return result;
    } catch (exception) {
        console.log(exception)
        throw Error("Error while saving data");
    }
}
exports.updateSession = updateSession;

var getSessionDetails = function (req, callback) {
    userSession.findOne({'sessionId': req.body.id, 'userPlanId': req.body.userPlanId}).populate('sessionId', 'title description').lean().exec(function (err, result) {
        if (err || result == null) {
            console.log(err);
            callback('err', null);
        } else {
            result.currentServerTime = _.now();
            callback(null, result);
        }
    })
}
exports.getSessionDetails = getSessionDetails;