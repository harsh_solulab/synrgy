const user = require('../../models/user.model');
const trainingProgram = require('../../models/program.model');
const trainingSessions = require('../../models/session.model');
const dietProgram = require('../../models/diet_programs.model');
const userPlan = require('../../models/user_plans.model');
const subscriptionPlan = require('../../models/subscription_plan.model');
const ptdtSubscriptionPlan = require('../../models/ptdt_subscription.model');
const programsDietGoals = require('../../models/programs_diets_goals.model');
const follow = require('../../models/follow.model');
const meals = require('../../models/meals.model');
const programsType = require('../../models/programs_types.model');
const invoice = require('../../models/invoice.model');
const userSession = require('../../models/user_session.model');
const userMeals = require('../../models/user_meals.model');
var async = require('async');
const _ = require('lodash');
const bcrypt = require('bcryptjs');
const userService = require('../../services/v1/user.service');
const commonService = require('../../services/v1/common.service');
const constants = require('../../config/constants');
const fs = require('fs');
const pdf = require('dynamic-html-pdf');
const mailer = require('../../modules/mailer.helper');
const noti = require('../../modules/push_notifications');
var moment = require('moment');

var getNotificaitonsDetails = function (req, userId, ptdtId, callback) {
    async.parallel([
        function (callback1) {
            var ptdtData = {
                body: {
                    userId: ptdtId,
                    isMine: "noti"
                },
                authenticationId: ptdtId
            };
            userService.getUserInfo(ptdtData, function (ptdtAccountData) {
                console.log("ptdtAccountData", ptdtAccountData);
                if (ptdtAccountData == 'err') {
                    callback1(err, null);
                } else {
                    callback1(null, ptdtAccountData);
                }
            });
        },
        // function call to get user details
        function (callback2) {
            var userData = {
                body: {
                    userId: userId,
                    isMine: "noti"
                },
                authenticationId: ptdtId
            };
            userService.getUserInfo(userData, function (userAccountData) {
                console.log("userAccountData", userAccountData);
                if (userAccountData == 'err') {
                    callback2(err, null);
                } else {
                    callback2(null, userAccountData);
                }
            });
        }
    ], function (err, resultSet) {
        console.log("error", err);
        console.log("resultSet", resultSet);
        if (err) {
            callback('err', null)
        } else {
            var data = {};
            data.ptdtInfo = resultSet[0];
            data.userInfo = resultSet[1];
            callback(null, data);
        }
    });
}
exports.getNotificaitonsDetails = getNotificaitonsDetails;

exports.signup = function (req, callback) {
    var profilePic = "";
    var certificates = [];
    if (req.files.profilePic !== undefined) {
        profilePic = constants.BASE_URL + constants.PROFILE_PIC + req.files['profilePic'][0].filename;
    }
    if (req.files.certificate !== undefined) {
        for (var i = 0; i < req.files['certificate'].length; i++) {
            certificates.push(constants.BASE_URL + constants.CERTIFICATE + req.files['certificate'][i].filename);
        }
    }
    async.parallel([
        function (hashCallback) {
            userService.genPassHashs(req.body.password, function (arrData) {
                hashCallback(null, arrData);
            });
        },
        function (randomCallback) {
            userService.genRandomStrings(5, function (arrData) {
                randomCallback(null, arrData);
            });
        }
    ], function (err, results) {
        password = results[0];
        verificationToken = results[1];
        var newUser = {
            nickname: req.body.nickname,
            email: req.body.email.toLowerCase(),
            password: password,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            gender: req.body.gender,
            dob: moment(req.body.dob).valueOf(),
            height: "",
            weight: "",
            country: req.body.country,
            city: req.body.city,
            location: {"type": "Point", "coordinates": [0.000, 0.000]},
            verificationToken: verificationToken,
            emailVerified: true,
            profilePic: profilePic,
            certificate: certificates,
            type: req.body.type, // 1-dt 2-pt
            isCertifiedAndApproved: 0, // 0-inactive 1-active 2-declined
            activity: "",
            isBlocked: false,
            isDeleted: false,
            isFreezed: false,
            commission: 10
        };

        // Save user.
        user(newUser).save(function (err, result) {
            if (err || result == null) {
                console.log("service", err);
                callback("err");
            } else {
                callback(result);
            }
        });
    });
};

var updatePtdtDetails = function (req, callback) {
    // var certificates = [];
    var obj = {
        nickname: req.body.nicekname,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        gender: req.body.gender,
        dob: moment(req.body.dob).valueOf(),
        country: req.body.country,
        city: req.body.city,
    };
    if (req.files.profilePic !== undefined) {
        obj.profilePic = constants.BASE_URL + constants.PROFILE_PIC + req.files['profilePic'][0].filename;
    }

    // if (req.files.certificate !== undefined) {
    //     for (var i = 0; i < req.files['certificate'].length; i++) {
    //         certificates.push(req.files['certificate'][i].filename);
    //     }
    //     obj.certificate = certificates;
    // }

    user.findOneAndUpdate({'_id': req.authenticationId},
            {$set: obj}, {new : true, fields: {password: 0, verificationToken: 0}})
            .populate('city').populate('country').lean().exec(function (err, result) {
        if (err || result == null) {
            callback("err", null);
        } else {
            var data = result.accounts.filter((account) => {
                return account.accessToken == req.authToken;
            })
            delete result.accounts;
            result.accounts = data;
            if (req.files.profilePic !== undefined) {
                var saveImage = {
                    userId: req.authenticationId,
                    image: constants.BASE_URL + constants.PROFILE_PIC + req.files['profilePic'][0].filename,
                };
                userService.saveUserImage(saveImage, function (data) {
                    if (data == "err") {
                        callback('err', null);
                    } else {
                        callback(null, result);
                    }
                });
            } else {
                callback(null, result);
            }
        }
    });
};
exports.updatePtdtDetails = updatePtdtDetails;

var addTrainingProgram = function (req, callback) {
    var newTrainingProgram = {
        createdBy: req.authenticationId,
        title: req.body.title,
        trainingType: 0,
        programType: req.body.programType,
        programGoal: req.body.goal,
        programMode: req.body.mode.toLowerCase(),
        address: req.body.address,
        duration: req.body.duration,
        price: req.body.price,
        coverPic: constants.BASE_URL + constants.COVER_PIC + req.file.filename,
    };
    if (req.body.city !== undefined && req.body.country !== undefined) {
        newTrainingProgram.city = req.body.city;
        newTrainingProgram.country = req.body.country;
    }
    trainingProgram(newTrainingProgram).save(function (err, trainingProgram) {
        if (err || trainingProgram == null) {
            callback('err', null);
        } else {
            callback(null, trainingProgram);
        }
    })
}
exports.addTrainingProgram = addTrainingProgram;

var updateTrainingProgram = function (req, callback) {
    var newTrainingProgram = {};
    userPlan.findOne({'planId': req.body.id}, function (err, plan) {
        if (err) {
            callback('err', null);
        } else {
            if (plan) {
                newTrainingProgram.title = req.body.title;
                if (req.file !== undefined) {
                    newTrainingProgram.coverPic = constants.BASE_URL + constants.COVER_PIC + req.file.filename;
                }
                trainingProgram.findOneAndUpdate({'_id': req.body.id}, {$set: newTrainingProgram}, {new : true}, function (err, result) {
                    if (err || result == null) {
                        callback('err', null);
                    } else {
                        result.msg = "Only Title and Cover Picture has been updated as this program is already purchased";
                        callback(null, result);
                    }
                })
            } else {
                newTrainingProgram.title = req.body.title;
                newTrainingProgram.duration = req.body.duration;
                newTrainingProgram.price = req.body.price;
                newTrainingProgram.programGoal = req.body.goal;
                newTrainingProgram.programType = req.body.programType;
                newTrainingProgram.programMode = req.body.mode.toLowerCase();
                if (req.body.mode == 'offline' && req.body.city !== undefined && req.body.country !== undefined && req.body.address !== undefined) {
                    newTrainingProgram.city = req.body.city;
                    newTrainingProgram.country = req.body.country;
                    newTrainingProgram.address = req.body.address;
                }
                if (req.file !== undefined) {
                    newTrainingProgram.coverPic = constants.BASE_URL + constants.COVER_PIC + req.file.filename;
                }
                var updateQuery = {
                    $set: newTrainingProgram,
                };
                if (req.body.mode.toLowerCase() == 'online') {
                    updateQuery.$unset = {city: 1, country: 1, address: 1};
                }
                trainingProgram.findOneAndUpdate({'_id': req.body.id}, updateQuery, {new : true}, function (err, result) {
                    if (err || result == null) {
                        callback('err', null);
                    } else {
                        if (req.body.sessions !== undefined) {
                            addTrainingSessions(req, result._id, function (err1, data) {
                                if (err1) {
                                    callback('err', null);
                                } else {
                                    result.msg = "Training Program updated successfully";
                                    callback(null, result);
                                }
                            })
                        } else {
                            result.msg = "Training Program updated successfully";
                            callback(null, result);
                        }
                    }
                })
            }
        }
    })
}
exports.updateTrainingProgram = updateTrainingProgram;

var getAllProgramsList = function (req, callback) {
    if (req.body.trainingType !== undefined) {
        var trainingType = req.body.trainingType;
    } else {
        var trainingType = 0;
    }
    var page = parseInt(req.body.skip * req.body.limit);
    var limit = parseInt(req.body.limit);

    trainingProgram.find({
        'createdBy': req.body.createdBy,
        'trainingType': trainingType,
        'isCompleted': 0,
        'isDeleted': 0
    }, {_id: 1, title: 1, coverPic: 1, trainingType: 1}).limit(limit).skip(page).sort({updated_at: 'desc'}).exec(function (err, result) {
        if (err) {
            callback('err', null);
        } else {
            trainingProgram.find({
                'createdBy': req.body.createdBy,
                'trainingType': trainingType,
                'isCompleted': 0,
                'isDeleted': 0
            }).exec(function (err1, result1) {
                if (err1) {
                    callback('err', null);
                } else {
                    var response = {
                        result: result,
                        total_count: result1.length
                    }
                    callback(null, response);
                }
            })
        }
    })
}
exports.getAllProgramsList = getAllProgramsList;

var getProgramDetails = function (req, callback) {
    trainingProgram.findOne({'_id': req.body.programId}).lean().exec(function (err, result) {
        if (err || result == null) {
            console.log('1', err);
            callback('err', null);
        } else {
            if (result.isDeleted == true) {
                callback('err1', null);
            } else {
                userPlan.find({'planId': result._id}, function (err1, data) {
                    if (err1) {
                        console.log('2', err1);
                        callback('err', null);
                    } else {
                        console.log(data)
                        result.isEditable = (data.length > 0) ? false : true;
                        result.purchaseCount = data.length;
                        result.isPurchased = false;
                        async.forEach(data, function (element, callback1) {
                            if (element.userId.equals(req.authenticationId) && element.isExpired == false) {
                                result.isPurchased = true;
                                result.userPlanId = element._id;
                                callback1();
                            } else {
                                callback1();
                            }
                        }, function (err4) {
                            if (err4) {
                                console.log('3', err4);
                                callback('err', null);
                            } else {
                                if (result.trainingType == 0 || result.trainingType == 3) {
                                    trainingSessions.find({'programId': result._id, 'isDeleted': false}, function (err2, data1) {
                                        if (err2 || data1.length == 0) {
                                            console.log('4', err2, data1);
                                            callback('err', null);
                                        } else {
                                            var populateObj = {select: 'name firstName lastName _id'};
                                            result.numberOfSessions = data1.length;
                                            if (result.programMode == 'online') {
                                                if (result.trainingType == 3) {
                                                    populateObj.path = 'programType programGoal userAssigned';
                                                } else {
                                                    populateObj.path = 'programType programGoal';
                                                }
                                            } else {
                                                if (result.trainingType == 3) {
                                                    populateObj.path = 'programType programGoal userAssigned city country';
                                                } else {
                                                    populateObj.path = 'programType programGoal city country';
                                                }
                                            }
                                            trainingProgram.populate(result, populateObj, function (err3, data2) {
                                                if (err3 || data2 == null) {
                                                    console.log('5', err3);
                                                    callback('err', null);
                                                } else {
                                                    callback(null, data2);
                                                }
                                            })
                                        }
                                    })
                                } else {
                                    trainingProgram.populate(result, {path: 'city country', select: 'name'}, function (err2, data1) {
                                        if (err2 || data1 == null) {
                                            console.log('6', err2);
                                            callback('err', null);
                                        } else {
                                            callback(null, data1);
                                        }
                                    })
                                }
                            }
                        });
                    }
                })
            }
        }
    })
}
exports.getProgramDetails = getProgramDetails;

var addDietPlan = function (req, callback) {
    var dietPlanObj = {
        createdBy: req.authenticationId,
        title: req.body.title,
        goal: req.body.goal,
        numberOfDays: req.body.numberOfDays,
        dietType: 4,
        price: req.body.price,
        coverPic: constants.BASE_URL + constants.COVER_PIC + req.file.filename
                // mealsPerDay: req.body.mealsPerDay
    }
    dietProgram(dietPlanObj).save(function (err, result) {
        if (err || result == null) {
            callback("err");
        } else {
            addDietMeals(result._id, req, function (mealPlanResult) {
                if (mealPlanResult == 'err') {
                    callback('err');
                } else {
                    callback(result);
                }
            })
        }
    })
}

exports.addDietPlan = addDietPlan;

var addDietMeals = function (dietPlanID, req, callback) {
    var mealss = JSON.parse(req.body.meals);
    mealss.forEach(meal => {
        meal.dietPlanID = dietPlanID;
    });
    meals.insertMany(mealss, function (err, data) {
        if (err || data == null) {
            callback('err', null);
        } else {
            callback(null, data);
        }
    })
}
exports.addDietMeals = addDietMeals;

var editDietPlanMeals = function (req, callback) {
    userPlan.findOne({dietPlanId: req.body.id}, function (err, result) {
        if (err) {
            callback('err');
        } else {
            if (result) {
                callback('err1');
            } else {
                meals.findOneAndUpdate({_id: req.body.mealId}, {
                    $set: {
                        title: req.body.title,
                        description: req.body.description,
                        calories: req.body.calories,
                        time: req.body.time,
                        numberOfDay: req.body.numberOfDay
                    }
                }, {new : true}, function (err, result) {
                    if (err || result == null) {
                        callback('err');
                    } else {
                        callback(result);
                    }
                })
            }
        }
    });
}
exports.editDietPlanMeals = editDietPlanMeals;

var deleteDietPlanMeals = function (req, callback) {
    userPlan.findOne({dietPlanId: req.body.id}, function (err, updatePlan) {
        if (err) {
            callback('err');
        } else {
            if (updatePlan) {
                callback('err1');
            } else {
                meals.find({dietPlanID: req.body.id, isDeleted: false}, function (err2, result1) {

                    if (err2 || result1.length == 0) {
                        callback('err');
                    } else {
                        if (result1.length <= 1) {
                            callback('err2');
                        } else {
                            meals.findOneAndUpdate({_id: req.body.mealId}, {$set: {isDeleted: true}}, function (err1, result) {
                                if (err1 || result == null) {
                                    callback('err');
                                } else {
                                    callback(result);
                                }
                            })
                        }
                    }
                })
            }
        }
    })
}
exports.deleteDietPlanMeals = deleteDietPlanMeals;

var dietPlanMealsListing = function (req, callback) {
    if (parseInt(req.body.isPurchased)) {
        userMeals.find({'userId': req.authenticationId, 'userPlanId': req.body.userPlanId}).populate('mealId').lean().exec(function (err, result) {
            if (err) {
                callback('err');
            } else {
                var data = [];
                result.forEach(function (mealResult) {
                    var obj = mealResult.mealId;
                    obj.userPlanId = mealResult.userPlanId;
                    data.push(obj);
                })
                callback(data);
            }
        })
    } else {
        meals.find({dietPlanID: req.body.dietID, isDeleted: false}, function (err, result) {
            if (err) {
                callback('err');
            } else {
                callback(result);
            }
        })
    }
}
exports.dietPlanMealsListing = dietPlanMealsListing;

var dietPlanMealDetails = function (req, callback) {
    userMeals.findOne({'mealId': req.body.mealId, 'userPlanId': req.body.userPlanId}).populate('mealId').lean.exec(function (err, result) {
        if (err || result == null) {
            callback('err');
        } else {
            callback(result);
        }
    })
}
exports.dietPlanMealDetails = dietPlanMealDetails;

var updateDietPlan = function (req, callback) {
    userPlan.findOne({dietPlanId: req.body.id}, function (err, updatePlan) {
        if (updatePlan) {
            var newDietPlan = {
                title: req.body.title,
            };
            if (req.file !== undefined) {
                newDietPlan.coverPic = constants.BASE_URL + constants.COVER_PIC + req.file.filename;
            }
            dietProgram.findOneAndUpdate({_id: req.body.id}, {$set: newDietPlan}, {new : true}, function (err, result) {
                if (err || result == null) {
                    callback('err');
                } else {
                    result.msg = "Only Title and Cover Picture has been updated as this diet plan is already purchased";
                    callback(result);
                }
            });
        } else {
            var newDietPlan = {
                title: req.body.title,
                goal: req.body.goal,
                numberOfDays: req.body.numberOfDays,
                price: req.body.price,
            };
            if (req.file !== undefined) {
                newDietPlan.coverPic = constants.BASE_URL + constants.COVER_PIC + req.file.filename;
            }
            dietProgram.findOneAndUpdate({_id: req.body.id}, {$set: newDietPlan}, {new : true}, function (err, result) {
                if (err || result == null) {
                    callback('err')
                } else {
                    if (req.body.meals != 'undefined') {
                        addDietMeals(result._id, req, function (mealPlanResult) {
                            if (mealPlanResult == 'err') {
                                callback('err');
                            } else {
                                result.msg = "Diet plan updated successfully";
                                callback(result);
                            }
                        });
                    } else {
                        result.msg = "Diet plan updated successfully";
                        callback(result);
                    }
                }
            });
        }
    })
}
exports.updateDietPlan = updateDietPlan;

var dietPlanDetail = function (req, callback) {
    dietProgram.findOne({_id: req.body.dietID}).lean().exec(function (err, result) {
        if (err || result == null) {
            console.log(err);
            callback('err');
        } else {
            if (result.isDeleted == true) {
                callback('err1');
            } else {
                userPlan.find({'dietPlanId': result._id}, function (err1, data) {//createdby ID
                    if (err1) {
                        console.log(err1);
                        callback('err')
                    } else {
                        result.isEditable = (data.length > 0) ? false : true;
                        result.isPurchased = false;
                        result.purhchaseCount = data.length;
                        async.forEach(data, function (element, callback1) {
                            if (element.userId.equals(req.authenticationId) && element.isExpired == false) {
                                result.isPurchased = true;
                                result.userPlanId = element._id;
                                callback1();
                            } else {
                                callback1();
                            }
                        }, function (err4) {
                            if (err4) {
                                callback('err');
                            } else {
                                var populateObj = {
                                    select: 'name firstName lastName _id'
                                };
                                if (result.dietType == 4) {
                                    populateObj.path = 'goal';
                                } else {
                                    populateObj.path = 'goal userAssigned';
                                }
                                dietProgram.populate(result, populateObj, function (err2, result1) {
                                    if (err2 || result1 == null) {
                                        console.log(err2);
                                        callback('err')
                                    } else {
                                        callback(result1);
                                    }
                                })
                            }
                        })
                    }
                })
            }
        }
    })
}
exports.dietPlanDetail = dietPlanDetail;

var allDietPlanDetails = function (req, callback) {
    var page = parseInt(req.body.skip * req.body.limit);
    var limit = parseInt(req.body.limit);
    if (req.body.dietType != undefined) {
        var dietType = req.body.dietType
    } else {
        var dietType = 4;
    }
    dietProgram.find({'createdBy': req.body.createdBy, 'dietType': dietType, isDeleted: false}, {_id: 1, title: 1, dietType: 1, coverPic: 1}).skip(page).limit(limit).sort({created_at: 'desc'}).exec(function (err, result) {
        if (err) {
            callback('err');
        } else {
            dietProgram.find({'createdBy': req.body.createdBy, 'dietType': dietType, isDeleted: false}).exec(function (err1, result1) {
                if (err1) {
                    callback('err');
                } else {
                    var responseObj = {
                        result: result,
                        total_count: result1.length
                    };
                    callback(responseObj);
                }
            })
        }
    })
}
exports.allDietPlanDetails = allDietPlanDetails;

var deleteProgram = function (req, callback) {
    userPlan.findOne({'planId': req.body.programId}, function (err, result) {
        if (err) {
            callback('err', null);
        } else {
            if (result) {
                callback('err1', null);
            } else {
                trainingProgram.findOneAndUpdate({'_id': req.body.programId}, {$set: {isDeleted: true}}, {new : true}, function (err, result) {
                    if (err || result == null) {
                        callback('err', null);
                    } else {
                        trainingSessions.updateMany({'programId': req.body.programId}, {$set: {isDeleted: true}}, function (err1, data) {
                            if (err1 || data == null) {
                                callback('err', null);
                            } else {
                                callback(null, result);
                            }
                        })
                    }
                })
            }
        }
    })
}
exports.deleteProgram = deleteProgram;

var deleteDietPlan = function (req, callback) {
    userPlan.findOne({dietPlanId: req.body.dietPlanId}, function (err, resultPlan) {
        if (resultPlan) {
            callback('err', null);
        } else {
            dietProgram.findOneAndUpdate({_id: req.body.dietPlanId}, {$set: {isDeleted: true}}, {new : true}, function (err, result) {
                if (err || result == null) {
                    callback('err', null);
                } else {
                    var query = {dietPlanID: req.body.dietPlanId};
                    meals.updateMany(query, {$set: {isDeleted: true}}, function (err, result) {
                        console.log(result);
                        if (err) {
                            callback('err', null);
                        } else {
                            callback(null, result);
                        }
                    })
                }
            })
        }
    })
}
exports.deleteDietPlan = deleteDietPlan;

var addTrainingSessions = function (req, programId, callback) {
    var sessions = JSON.parse(req.body.sessions);
    sessions.forEach(session => {
        session.programId = programId;
    });
    trainingSessions.insertMany(sessions, function (err, data) {
        if (err || data == null) {
            callback('err', null);
        } else {
            callback(null, data);
        }
    })
}
exports.addTrainingSessions = addTrainingSessions;

var editSession = function (req, callback) {
    var session = {
        title: req.body.title,
        description: req.body.description,
    }
    userPlan.findOne({planId: req.body.programId}, function (err, result) {
        if (err) {
            callback('err', null);
        } else {
            if (result) {
                callback('err1', null);
            } else {
                trainingSessions.findOneAndUpdate({'_id': req.body.sessionId, isDeleted: false}, {$set: session}, {new : true}, function (err, result) {
                    console.log(err)
                    if (err || result == null) {
                        callback('err', null);
                    } else {
                        callback(null, result);
                    }
                })
            }
        }
    });
}
exports.editSession = editSession;

var deleteSession = function (req, callback) {
    userPlan.findOne({planId: req.body.programId}, function (err, result) {
        if (err) {
            callback('err', null);
        } else {
            if (result) {
                callback('err1', null);
            } else {
                trainingSessions.find({programId: req.body.programId, isDeleted: false}, function (err2, result2) {
                    if (err2 || result2.length == 0) {
                        callback('err', null);
                    } else {
                        if (result2.length <= 1) {
                            callback('err2', null);
                        } else {
                            trainingSessions.updateOne({'_id': req.body.sessionId}, {$set: {isDeleted: true}}, function (err1, result1) {
                                if (err1 || result1 == null) {
                                    callback('err', null);
                                } else {
                                    callback(null, result1);
                                }
                            })
                        }
                    }
                })
            }
        }
    })
}
exports.deleteSession = deleteSession;

var getSessions = function (req, callback) {
    if (parseInt(req.body.isPurchased)) {
        var status = parseInt(req.body.status);
        if (status != 3) {
            userSession.find({'userId': req.authenticationId, userPlanId: req.body.userPlanId, 'status': {$ne: 3}}, {ptdtId: 0}).lean().populate("sessionId", "title description").exec(function (err, result) {
                if (err) {
                    callback('err', null);
                } else {
                    var data = [];
                    result.forEach(function (sessionResult) {
                        var obj = {};
                        obj._id = sessionResult.sessionId._id;
                        obj.title = sessionResult.sessionId.title;
                        obj.description = sessionResult.sessionId.description;
                        obj.userPlanId = sessionResult.userPlanId;
                        data.push(obj)
                    })
                    callback(null, data);
                }
            })
        } else {
            userSession.find({'userId': req.authenticationId, userPlanId: req.body.userPlanId, 'status': 3}, {ptdtId: 0}).lean().populate("sessionId", "title description").exec(function (err, result) {
                if (err) {
                    callback('err', null);
                } else {
                    var data = [];
                    result.forEach(function (sessionResult) {
                        var obj = {};
                        obj._id = sessionResult.sessionId._id;
                        obj.title = sessionResult.sessionId.title;
                        obj.description = sessionResult.sessionId.description;
                        obj.userPlanId = sessionResult.userPlanId;
                        data.push(obj)
                    })
                    callback(null, data);
                }
            })

        }
    } else {
        trainingSessions.find({'programId': req.body.programId, isDeleted: false}, {_id: 1, title: 1, description: 1}, function (err, result) {
            if (err) {
                callback('err', null);
            } else {
                callback(null, result);
            }
        })
    }
}
exports.getSessions = getSessions;

var addTrainingClasses = function (req, callback) {
    var date = new Date(parseInt(req.body.startDateTime));
    date.setHours(0, 0, 0, 0);

    var saveTrainingClass = {
        createdBy: req.authenticationId,
        title: req.body.title,
        description: req.body.description,
        trainingType: 1,
        coverPic: constants.BASE_URL + constants.COVER_PIC + req.file.filename,
        startDateTime: parseInt(req.body.startDateTime),
        endDateTime: parseInt(req.body.endDateTime),
        duration: req.body.duration,
        classNumberOfParticipants: req.body.numberOfParticipants,
        address: req.body.address,
        country: req.body.country,
        city: req.body.city,
        price: req.body.price,
        calendarEventDate: moment(date).valueOf()
    }
    trainingProgram(saveTrainingClass).save(function (err, result) {
        if (err || result == null) {
            console.log(err);
            callback('err');
        } else {
            callback(result);
        }
    })
}
exports.addTrainingClasses = addTrainingClasses;

var editTrainingClasses = function (req, callback) {
    userPlan.findOne({planId: req.body.id}, function (err, result) {
        if (result) {
            var newTrainingClass = {
                title: req.body.title,
                description: req.body.description,
                duration: req.body.duration,
            };
            if (req.file !== undefined) {
                newTrainingClass.coverPic = constants.BASE_URL + constants.COVER_PIC + req.file.filename;
            }
            trainingProgram.findOneAndUpdate({_id: req.body.id}, {$set: newTrainingClass}, function (err, result) {
                if (err || result == null) {
                    callback('err');
                } else {
                    result.msg = "Only Title , description , duration and Cover Picture has been updated as this training class is already purchased";
                    callback(result);
                }
            })
        } else {
            var date = new Date(parseInt(req.body.startDateTime));
            date.setHours(0, 0, 0, 0);
            var startDate = new Date(req.body.startDateTime * 1000);
            var currentDate = new Date();
            if (startDate >= currentDate) {
                var newTrainingClass = {
                    title: req.body.title,
                    description: req.body.description,
                    startDateTime: parseInt(req.body.startDateTime),
                    endDateTime: parseInt(req.body.endDateTime),
                    classNumberOfParticipants: req.body.numberOfParticipants,
                    duration: req.body.duration,
                    address: req.body.address,
                    country: req.body.country,
                    city: req.body.city,
                    price: req.body.price,
                    calendarEventDate: moment(date).valueOf()
                };
                if (req.file !== undefined) {
                    newTrainingClass.coverPic = constants.BASE_URL + constants.COVER_PIC + req.file.filename;
                }
                trainingProgram.findOneAndUpdate({_id: req.body.id}, {$set: newTrainingClass}, function (err, result) {
                    if (err || result == null) {
                        callback('err');
                    } else {
                        result.msg = "Class edited successfully";
                        callback(result);
                    }
                })
            } else {
                var newTrainingClass = {
                    title: req.body.title,
                    description: req.body.description,
                    duration: req.body.duration,
                };
                if (req.file !== undefined) {
                    newTrainingClass.coverPic = constants.BASE_URL + constants.COVER_PIC + req.file.filename;
                }
                trainingProgram.findOneAndUpdate({_id: req.body.id}, {$set: newTrainingClass}, function (err, result) {
                    if (err || result == null) {
                        callback('err');
                    } else {
                        result.msg = "Only Title , description , duration and Cover Picture has been updated as this training class is already purchased";
                        callback(result);
                    }
                })
            }
        }
    })
}
exports.editTrainingClasses = editTrainingClasses;

var deleteTrainingClasses = function (req, callback) {
    userPlan.findOne({planId: req.body.classId}, function (err, result) {
        console.log("1", result);
        if (result) {
            callback('err')
        } else {
            trainingProgram.findOneAndUpdate({_id: req.body.classId}, {$set: {isDeleted: true}}, function (err, result) {
                if (err || result == null) {
                    callback('err');
                } else {
                    callback(result);
                }
            })
        }
    })
}
exports.deleteTrainingClasses = deleteTrainingClasses;

var addTrainingWorkshop = function (req, callback) {
    var date = new Date(parseInt(req.body.startDateTime));
    date.setHours(0, 0, 0, 0);

    var workshopObject = {
        createdBy: req.authenticationId,
        title: req.body.title,
        startDateTime: parseInt(req.body.startDateTime),
        endDateTime: parseInt(req.body.endDateTime),
        city: req.body.city,
        country: req.body.country,
        address: req.body.address,
        price: req.body.price,
        description: req.body.description,
        coverPic: constants.BASE_URL + constants.COVER_PIC + req.file.filename,
        trainingType: 2,
        calendarEventDate: moment(date).valueOf()
    }
    trainingProgram(workshopObject).save(function (err, result) {
        if (err || result == null) {
            callback('err', null);
        } else {
            callback(null, result);
        }
    })
}
exports.addTrainingWorkshop = addTrainingWorkshop;

var editTrainingWorkshop = function (req, callback) {
    var newWorkshopObject = {};
    var msg = 1;
    userPlan.findOne({'planId': req.body.id}, function (err, plan) {
        if (err) {
            callback('err', null);
        } else {
            if (plan) {
                newWorkshopObject.title = req.body.title;
                newWorkshopObject.description = req.body.description;
            } else {
                var start = new Date(req.body.startDateTime * 1000);
                var current = new Date();
                if (start <= current) {
                    newWorkshopObject.title = req.body.title;
                    newWorkshopObject.description = req.body.description;
                } else {
                    msg = 2;
                    var date = new Date(parseInt(req.body.startDateTime));
                    date.setHours(0, 0, 0, 0);
                    newWorkshopObject.title = req.body.title;
                    newWorkshopObject.description = req.body.description;
                    newWorkshopObject.startDateTime = parseInt(req.body.startDateTime);
                    newWorkshopObject.endDateTime = parseInt(req.body.endDateTime);
                    newWorkshopObject.city = req.body.city;
                    newWorkshopObject.country = req.body.country;
                    newWorkshopObject.address = req.body.address;
                    newWorkshopObject.price = req.body.price;
                    newWorkshopObject.calendarEventDate = moment(date).valueOf();
                }
            }
            if (req.file !== undefined) {
                newWorkshopObject.coverPic = constants.BASE_URL + constants.COVER_PIC + req.file.filename;
            }
            trainingProgram.findOneAndUpdate({'_id': req.body.id}, {$set: newWorkshopObject}, {new : true}, function (err1, result) {
                if (err1 || result == null) {
                    callback('err', null);
                } else {
                    if (msg == 1) {
                        result.msg = "Only Title , description and Cover Picture has been updated as this training workshop is already purchased";
                    } else {
                        result.msg = "Workshop Modified Successfully";
                    }
                    callback(null, result);
                }
            });
        }
    })
}
exports.editTrainingWorkshop = editTrainingWorkshop;

var addCustomizeProgram = function (req, callback) {
    var newTrainingProgram = {
        createdBy: req.authenticationId,
        title: req.body.title,
        trainingType: 3,
        programType: req.body.programType,
        programGoal: req.body.goal,
        programMode: req.body.mode.toLowerCase(),
        userAssigned: req.body.userAssigned,
        duration: req.body.duration,
        price: req.body.price,
        coverPic: constants.BASE_URL + constants.COVER_PIC + req.file.filename,
    };
    if (req.body.address !== undefined && req.body.city !== undefined && req.body.country !== undefined) {
        newTrainingProgram.city = req.body.city;
        newTrainingProgram.country = req.body.country;
        newTrainingProgram.address = req.body.address;
    }
    trainingProgram(newTrainingProgram).save(function (err, trainingProgramResult) {
        if (err || trainingProgramResult == null) {
            console.log(err);
            callback('err', null);
        } else {
            var followObj = {
                followedBy: req.body.userAssigned,
                followedTo: req.authenticationId,
                firstName: req.body.firstName,
                lastName: req.body.lastName
            };
            commonService.followUnfollow(followObj, 1, req.authenticationId, function (err1, followResult) {
                if (err1 || followResult == null) {
                    console.log(err1);
                    callback('err', null);
                } else {
                    getNotificaitonsDetails(req, req.body.userAssigned, req.authenticationId, function (err3, data) {
                        if (err3) {
                            callback('err', null);
                        } else {
                            var objNotification = {
                                for : req.body.userAssigned,
                                programId: trainingProgramResult._id,
                                notification: data.ptdtInfo.firstName + " " + data.ptdtInfo.lastName + " has assigned you" + " " + req.body.title + " " + "custom program.",
                                type: 'customize program',
                                planType: 3
                            };
                            commonService.saveNotification(objNotification, function (err2, result) {
                                if (err2 || result == null) {
                                    callback('err', null);
                                } else {
                                    /************* Send Notification *********************/
                                    noti.sendNotifications(data.userInfo.accounts, "customize program", data.ptdtInfo.firstName + " " + data.ptdtInfo.lastName + " has assigned you" + " " + req.body.title + " " + "custom program.", {"id": trainingProgramResult._id, 'planType': 3, 'userType': 0}, function (notification) {
                                        if (notification == 'err') {
                                            callback('err', null);
                                        } else {
                                            callback(null, trainingProgramResult);
                                        }
                                    });
                                    /************* Send Notification *********************/
                                }
                            })
                        }
                    })
                }
            });
        }
    })
}
exports.addCustomizeProgram = addCustomizeProgram;

var editCustomizeProgram = function (req, callback) {
    var newTrainingProgram = {};
    userPlan.findOne({'planId': req.body.id}, function (err, plan) {
        if (err) {
            callback('err', null);
        } else {
            if (plan) {
                newTrainingProgram.title = req.body.title;
                if (req.file !== undefined) {
                    newTrainingProgram.coverPic = constants.BASE_URL + constants.COVER_PIC + req.file.filename;
                }
                trainingProgram.findOneAndUpdate({'_id': req.body.id}, {$set: newTrainingProgram}, {new : true}, function (err, trainingProgram) {
                    if (err || trainingProgram == null) {
                        callback('err', null);
                    } else {
                        trainingProgram.msg = "Only Title and Cover Picture has been updated as this program is already purchased";
                        callback(null, trainingProgram);
                        // var objNotification = {
                        //     for: req.body.userAssigned,
                        //     to: trainingProgram._id,
                        //     notification: 'Your Customized Program Has Been Modified',
                        //     type: 'customize program',
                        // };
                        // commonService.saveNotification(objNotification, function (err2, result) {
                        //     if (err2 || result == null) {
                        //         callback('err', null);
                        //     } else {
                        //         callback(null, trainingProgram);
                        //     }
                        // })
                    }
                })
            } else {
                var userChanged = false;
                trainingProgram.findOne({'_id': req.body.id}, {userAssigned: 1}, function (err5, result) {
                    if (err5 || result == null) {
                        callback('err', null);
                    } else {
                        if (result.userAssigned.equals(req.body.id)) {
                            userChanged = false;
                        } else {
                            userChanged = true;
                        }
                        newTrainingProgram.title = req.body.title;
                        newTrainingProgram.duration = req.body.duration;
                        newTrainingProgram.price = req.body.price;
                        newTrainingProgram.programGoal = req.body.goal;
                        newTrainingProgram.programType = req.body.programType;
                        newTrainingProgram.userAssigned = req.body.userAssigned;
                        newTrainingProgram.programMode = req.body.mode.toLowerCase();
                        if (req.body.city !== undefined && req.body.country !== undefined && req.body.address !== undefined) {
                            newTrainingProgram.city = req.body.city;
                            newTrainingProgram.country = req.body.country;
                            newTrainingProgram.address = req.body.address;
                        }
                        if (req.file !== undefined) {
                            newTrainingProgram.coverPic = constants.BASE_URL + constants.COVER_PIC + req.file.filename;
                        }
                        var updateQuery = {
                            $set: newTrainingProgram,
                        };
                        if (req.body.mode.toLowerCase() == 'online') {
                            updateQuery.$unset = {city: 1, country: 1, address: 1};
                        }
                        trainingProgram.findOneAndUpdate({'_id': req.body.id}, updateQuery, {new : true}, function (err4, trainingProgram) {
                            if (err4 || trainingProgram == null) {
                                callback('err', null);
                            } else {
                                if (req.body.sessions !== undefined) {
                                    addTrainingSessions(req, trainingProgram._id, function (err1, data) {
                                        if (err1 || data == null) {
                                            callback('err', null);
                                        } else {
                                            if (userChanged) {
                                                var followObj = {
                                                    followedBy: req.body.userAssigned,
                                                    followedTo: req.authenticationId,
                                                    firstName: req.body.firstName,
                                                    lastName: req.body.lastName
                                                };
                                                commonService.followUnfollow(followObj, 1, req.authenticationId, function (err3, followResult) {
                                                    if (err3 || followResult == null) {
                                                        callback('err', null);
                                                    } else {
                                                        getNotificaitonsDetails(req, req.body.userAssigned, req.authenticationId, function (err4, result1) {
                                                            if (err4) {
                                                                callback('err', null);
                                                            } else {
                                                                var objNotification = {
                                                                    for : req.body.userAssigned,
                                                                    programId: trainingProgram._id,
                                                                    notification: result1.ptdtInfo.firstName + " " + result1.ptdtInfo.lastName + " has assigned you " + req.body.title + " program.",
                                                                    type: 'customize program',
                                                                    planType: 3
                                                                };
                                                                commonService.saveNotification(objNotification, function (err2, result) {
                                                                    if (err2 || result == null) {
                                                                        callback('err', null);
                                                                    } else {
                                                                        noti.sendNotifications(result1.userInfo.accounts, "customize program", result1.ptdtInfo.firstName + " " + result1.ptdtInfo.lastName + " has assigned you " + req.body.title + " program.", {"id": trainingProgram._id, 'planType': 3, userType: 0}, function (notification) {
                                                                            if (notification == 'err') {
                                                                                callback('err', null);
                                                                            } else {
                                                                                trainingProgram.msg = "Training Program updated successfully";
                                                                                callback(null, trainingProgram);
                                                                            }
                                                                        });
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            } else {
                                                trainingProgram.msg = "Training Program updated successfully";
                                                callback(null, trainingProgram);
                                            }
                                        }
                                    }
                                    )
                                } else {
                                    if (userChanged) {
                                        var followObj = {
                                            followedBy: req.body.userAssigned,
                                            followedTo: req.authenticationId,
                                            firstName: req.body.firstName,
                                            lastName: req.body.lastName
                                        };
                                        commonService.followUnfollow(followObj, 1, req.authenticationId, function (err1, followResult) {
                                            if (err1 || followResult == null) {
                                                callback('err', null);
                                            } else {
                                                getNotificaitonsDetails(req, req.body.userAssigned, req.authenticationId, function (err3, result1) {
                                                    if (err3) {
                                                        callback('err', null);
                                                    } else {
                                                        var objNotification = {
                                                            for : req.body.userAssigned,
                                                            programId: trainingProgram._id,
                                                            notification: result1.ptdtInfo.firstName + " " + result1.ptdtInfo.lastName + " has assigned you " + req.body.title + " program.",
                                                            type: 'customize program',
                                                            planType: 3
                                                        };
                                                        commonService.saveNotification(objNotification, function (err2, result) {
                                                            if (err2 || result == null) {
                                                                callback('err', null);
                                                            } else {
                                                                noti.sendNotifications(result1.userInfo.accounts, "customize program", result1.ptdtInfo.firstName + " " + result1.ptdtInfo.lastName + " has assigned you " + req.body.title + " program.", {"id": trainingProgram._id, 'planType': 3, userType: 0}, function (notification) {
                                                                    if (notification == 'err') {
                                                                        callback('err', null);
                                                                    } else {
                                                                        trainingProgram.msg = "Training Program updated successfully";
                                                                        callback(null, trainingProgram);
                                                                    }
                                                                });
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        });
                                    } else {
                                        trainingProgram.msg = "Training Program updated successfully";
                                        callback(null, trainingProgram);
                                    }
                                }
                            }
                        })
                    }
                })
            }
        }
    })
}
exports.editCustomizeProgram = editCustomizeProgram;

var deleteTrainingWorkshop = function (req, callback) {
    userPlan.findOne({'planId': req.body.workshopId}, function (err, result) {
        if (err) {
            callback(err, null);
        } else {
            if (result) {
                callback('err', null);
            } else {
                trainingProgram.findOneAndUpdate({'_id': req.body.workshopId}, {$set: {isDeleted: true}}, {new : true}, function (err, result) {
                    if (err || result == null) {
                        callback('err', null);
                    } else {
                        callback(null, result);
                    }
                })
            }
        }
    })
}
exports.deleteTrainingWorkshop = deleteTrainingWorkshop;

var addCustomizeDietPlan = function (req, callback) {
    var dietPlanObj = {
        createdBy: req.authenticationId,
        title: req.body.title,
        goal: req.body.goal,
        numberOfDays: req.body.numberOfDays,
        dietType: 5,
        price: req.body.price,
        coverPic: constants.BASE_URL + constants.COVER_PIC + req.file.filename,
        // mealsPerDay: req.body.mealsPerDay,
        userAssigned: req.body.userAssigned
    }
    dietProgram(dietPlanObj).save(function (err, dietProgramResult) {
        if (err || dietProgramResult == null) {
            callback("err", null);
        } else {
            var followObj = {
                followedBy: req.body.userAssigned,
                followedTo: req.authenticationId,
                firstName: req.body.firstName,
                lastName: req.body.lastName
            };
            commonService.followUnfollow(followObj, 1, req.authenticationId, function (err1, followResult) {
                if (err1 || followResult == null) {
                    callback('err', null);
                } else {
                    getNotificaitonsDetails(req, req.body.userAssigned, req.authenticationId, function (err3, data) {
                        if (err3) {
                            callback('err', null);
                        } else {
                            var objNotification = {
                                for : req.body.userAssigned,
                                dietProgramId: dietProgramResult._id,
                                notification: data.ptdtInfo.firstName + " " + data.ptdtInfo.lastName + " has assigned you " + req.body.title + " diet plan.",
                                type: 'customize diet plan',
                                planType: 5
                            };
                            commonService.saveNotification(objNotification, function (err2, result) {
                                if (err2 || result == null) {
                                    callback('err', null);
                                } else {
                                    /************* Send Notification *********************/
                                    noti.sendNotifications(data.userInfo.accounts, "customize diet plan", data.ptdtInfo.firstName + " " + data.ptdtInfo.lastName + " has assigned you " + req.body.title + " diet plan.", {"id": dietProgramResult._id, "planType": 5, userType: 0}, function (notification) {
                                        if (notification == 'err') {
                                            callback('err', null);
                                        } else {
                                            callback(null, dietProgramResult);
                                        }
                                    })
                                    /************* Send Notification *********************/
                                }
                            })
                        }
                    })
                }
            });
        }
    })
}
exports.addCustomizeDietPlan = addCustomizeDietPlan;

var editCustomizeDietPlan = function (req, callback) {
    var newDietPlan = {};
    userPlan.findOne({'dietPlanId': req.body.id}, function (err, dietPlan) {
        if (err) {
            callback('err', null);
        } else {
            if (dietPlan) {
                newDietPlan.title = req.body.title;
                if (req.file !== undefined) {
                    newDietPlan.coverPic = constants.BASE_URL + constants.COVER_PIC + req.file.filename;
                }
                dietProgram.findOneAndUpdate({'_id': req.body.id}, {$set: newDietPlan}, {new : true}, function (err1, dietProgram) {
                    if (err1 || dietProgram == null) {
                        callback('err', null);
                    } else {
                        dietProgram.msg = "Only Title and Cover Picture has been updated as this diet plan is already purchased";
                        callback(null, dietProgram);
                        // var objNotification = {
                        //     for: req.body.userAssigned,
                        //     to: dietProgram._id,
                        //     notification: 'Your Customized Diet Plan Has Been Modified',
                        //     type: 'customize diet plan',
                        // };
                        // commonService.saveNotification(objNotification, function (err2, result) {
                        //     if (err2 || result == null) {
                        //         callback('err', null);
                        //     } else {
                        //         callback(null, dietProgram);
                        //     }
                        // })
                    }
                });
            } else {
                dietProgram.findOne({'_id': req.body.id}, {userAssigned: 1}, function (err5, result) {
                    if (err5 || result == null) {
                        callback('err', null);
                    } else {
                        var userChanged = false;
                        if (result.userAssigned.equals(req.body.userAssigned)) {
                            userChanged = false;
                        } else {
                            userChanged = true;
                        }
                        newDietPlan.title = req.body.title;
                        newDietPlan.goal = req.body.goal;
                        newDietPlan.numberOfDays = req.body.numberOfDays;
                        newDietPlan.price = req.body.price;
                        // newDietPlan.mealsPerDay = req.body.mealsPerDay;
                        newDietPlan.userAssigned = req.body.userAssigned;
                        if (req.file !== undefined) {
                            newDietPlan.coverPic = constants.BASE_URL + constants.COVER_PIC + req.file.filename;
                        }
                        dietProgram.findOneAndUpdate({'_id': req.body.id}, {$set: newDietPlan}, {new : true}, function (err1, dietProgram) {
                            if (err1 || dietProgram == null) {
                                callback('err', null);
                            } else {
                                if (req.body.meals !== undefined) {
                                    addDietMeals(dietProgram._id, req, function (err2, data) {
                                        if (err2 || data == null) {
                                            callback('err', null);
                                        } else {
                                            if (userChanged) {
                                                var followObj = {
                                                    followedBy: req.body.userAssigned,
                                                    followedTo: req.authenticationId,
                                                    firstName: req.body.firstName,
                                                    lastName: req.body.lastName
                                                };
                                                commonService.followUnfollow(followObj, 1, req.authenticationId, function (err3, followResult) {
                                                    if (err3 || followResult == null) {
                                                        callback('err', null);
                                                    } else {
                                                        getNotificaitonsDetails(req, req.body.userAssigned, req.authenticationId, function (err6, result1) {
                                                            if (err6) {
                                                                callback('err', null);
                                                            } else {
                                                                var objNotification = {
                                                                    for : req.body.userAssigned,
                                                                    dietProgramId: dietProgram._id,
                                                                    notification: result1.ptdtInfo.firstName + " " + result1.ptdtInfo.lastName + " has assigned you " + req.body.title + " diet plan.",
                                                                    type: 'customize diet plan',
                                                                    planType: 5
                                                                };
                                                                commonService.saveNotification(objNotification, function (err4, result) {
                                                                    if (err4 || result == null) {
                                                                        callback('err', null);
                                                                    } else {
                                                                        noti.sendNotifications(result1.userInfo.accounts, "customize diet plan", result1.ptdtInfo.firstName + " " + result1.ptdtInfo.lastName + " has assigned you " + req.body.title + " diet plan.", {"id": dietProgram._id, 'planType': 5, userType: 0}, function (notification) {
                                                                            if (notification == 'err') {
                                                                                callback('err', null);
                                                                            } else {
                                                                                dietProgram.msg = "Diet plan updated successfully";
                                                                                callback(null, dietProgram);
                                                                            }
                                                                        });
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                });
                                            } else {
                                                dietProgram.msg = "Diet plan updated successfully";
                                                callback(null, dietProgram);
                                            }
                                        }
                                    })
                                } else {
                                    if (userChanged) {
                                        var followObj = {
                                            followedBy: req.body.userAssigned,
                                            followedTo: req.authenticationId,
                                            firstName: req.body.firstName,
                                            lastName: req.body.lastName
                                        };
                                        commonService.followUnfollow(followObj, 1, req.authenticationId, function (err2, followResult) {
                                            if (err2 || followResult == null) {
                                                callback('err', null);
                                            } else {
                                                getNotificaitonsDetails(req, req.body.userAssigned, req.authenticationId, function (err4, result1) {
                                                    if (err4) {
                                                        callback('err', null);
                                                    } else {
                                                        var objNotification = {
                                                            for : req.body.userAssigned,
                                                            dietProgramId: dietProgram._id,
                                                            notification: result1.ptdtInfo.firstName + " " + result1.ptdtInfo.lastName + " has assigned you " + req.body.title + " diet plan.",
                                                            type: 'customize diet plan',
                                                            planType: 5
                                                        };
                                                        commonService.saveNotification(objNotification, function (err3, result) {
                                                            if (err3 || result == null) {
                                                                callback('err', null);
                                                            } else {
                                                                noti.sendNotifications(result1.userInfo.accounts, "customize program", result1.ptdtInfo.firstName + " " + result1.ptdtInfo.firstName + " " + result1.ptdtInfo.lastName + " has assigned you " + req.body.title + " diet plan.", {"id": dietProgram._id, 'planType': 5, userType: 0}, function (notification) {
                                                                    if (notification == 'err') {
                                                                        callback('err', null);
                                                                    } else {
                                                                        dietProgram.msg = "Diet plan updated successfully";
                                                                        callback(null, dietProgram);
                                                                    }
                                                                });
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        });
                                    } else {
                                        dietProgram.msg = "Diet plan updated successfully";
                                        callback(null, dietProgram);
                                    }
                                }
                            }
                        });
                    }
                })
            }
        }
    })
}
exports.editCustomizeDietPlan = editCustomizeDietPlan;

var getUserPurchasePrograms = function (req, callback) {
    var page = parseInt(req.body.skip * req.body.limit);
    var limit = parseInt(req.body.limit);
    userPlan.find({userId: req.authenticationId, planType: {$lte: 3}, isExpired: false}).skip(page).limit(limit).populate({path: 'planId', select: '_id title coverPic trainingType'}).lean().exec(function (err, result) {
        if (err) {
            callback('err');
        } else {
            if (result.length > 0) {
                var programArr = [];
                result.forEach(function (data) {
                    var obj = {};
                    obj.userPlanId = data._id;
                    obj._id = data.planId._id;
                    obj.title = data.planId.title;
                    obj.trainingType = data.planId.trainingType;
                    obj.coverPic = data.planId.coverPic;
                    programArr.push(obj);
                })
                userPlan.find({userId: req.authenticationId, planType: {$lte: 3}, isExpired: false}).exec(function (err1, result1) {
                    if (err1) {
                        callback('err');
                    } else {
                        var responseObj = {
                            result: programArr,
                            total_count: result1.length
                        };
                        callback(responseObj);
                    }
                })
            } else {
                var responseObj = {
                    result: [],
                    total_count: 0
                };
                callback(responseObj);
            }
        }
    })
}
exports.getUserPurchasePrograms = getUserPurchasePrograms;

var getUserPurchaseDietPlan = function (req, callback) {
    var page = parseInt(req.body.skip * req.body.limit);
    var limit = parseInt(req.body.limit);
    userPlan.find({userId: req.authenticationId, planType: {$gte: 4, $lte: 5}, isExpired: false}).skip(page).limit(limit).populate({path: 'dietPlanId', select: '_id title coverPic dietType'}).lean().exec(function (err, results) {
        if (err) {
            callback('err');
        } else {
            if (results.length > 0) {
                var dietArr = [];
                results.forEach(function (data) {
                    var dietObj = {}
                    dietObj.userPlanId = data._id;
                    dietObj._id = data.dietPlanId._id;
                    dietObj.title = data.dietPlanId.title;
                    dietObj.dietType = data.dietPlanId.dietType;
                    dietObj.coverPic = data.dietPlanId.coverPic;
                    dietArr.push(dietObj)
                })
                userPlan.find({userId: req.authenticationId, planType: {$gte: 4, $lte: 5}, isExpired: false}).lean().exec(function (err1, results1) {
                    if (err1) {
                        callback('err');
                    } else {
                        var responseObj = {
                            result: dietArr,
                            total_count: results1.length
                        };
                        callback(responseObj);
                    }
                })
            } else {
                var responseObj = {
                    result: [],
                    total_count: 0
                };
                callback(responseObj);
            }
        }
    })
}
exports.getUserPurchaseDietPlan = getUserPurchaseDietPlan;

var downloadPersonalInformation = function (req, callback) {
    var planIds = [];
    var arr = [];

    async.parallel([
        function (userCallback) {
            user.findOne({_id: req.authenticationId}).lean().exec(function (err, result) {
                if (err) {
                    userCallback('err', null);
                } else {
                    var invoiceDate = new Date(result.dob).toISOString().slice(0, 10);
                    result.dob = invoiceDate;
                    userCallback(null, result);
                }
            })
        }, function (programCallabck) {
            var arr = [];
            userPlan.find({userId: req.authenticationId, planType: 0}).populate('planId', "_id title").populate("userPlanId").lean().exec(function (err, programResult) {
                if (err) {
                    programCallabck('err', null);
                } else {
                    if (programResult.length > 0) {
                        programResult.forEach(function (result) {
                            var tempObj = {
                                title: result.planId.title,
                                inCompleteCount: 0,
                                completeCount: 0,
                                totalProgram: 0,
                                planId: result.planId._id,
                                userPlanId: result._id
                            };
                            planIds.push(result.planId._id)
                            arr.push(tempObj);
                        });
                        userSession.find({planId: {$in: planIds}}).populate("userPlanId").lean().exec(function (err1, result) {
                            if (err1) {
                                programCallabck('err', null);
                            } else {
                                result.forEach(function (data) {
                                    arr.forEach(function (arrData) {
                                        if (arrData.planId.equals(data.userPlanId.planId) && arrData.userPlanId.equals(data.userPlanId._id)) {
                                            arrData.totalProgram++;
                                            if (data.status == 0) {
                                                arrData.inCompleteCount++;
                                            } else if (data.status == 3) {
                                                arrData.completeCount++;
                                            }
                                        }
                                    })
                                })
                                programCallabck(null, arr);
                            }
                        })
                    } else {
                        programCallabck(null, arr);
                    }
                }
            })
        }, function (customProgram) {
            var arrd = []
            var planIds = []
            userPlan.find({userId: req.authenticationId, planType: 3}).populate('planId', "_id title").populate("userPlanId").lean().exec(function (err, programResult) {
                if (err) {
                    customProgram('err', null);
                } else {
                    if (programResult.length > 0) {
                        programResult.forEach(function (result) {
                            var tempObj = {
                                title: result.planId.title,
                                inCompleteCount: 0,
                                completeCount: 0,
                                totalProgram: 0,
                                planId: result.planId._id,
                                userPlanId: result._id
                            };
                            planIds.push(result.planId._id)
                            arrd.push(tempObj);
                        });

                        userSession.find({planId: {$in: planIds}}).populate("userPlanId").lean().exec(function (err1, result) {
                            if (err1) {
                                customProgram('err', null);
                            } else {
                                result.forEach(function (data) {
                                    arrd.forEach(function (arrData) {
                                        if (arrData.planId.equals(data.userPlanId.planId) && arrData.userPlanId.equals(data.userPlanId._id)) {
                                            arrData.totalProgram++;
                                            if (data.status == 0) {
                                                arrData.inCompleteCount++;
                                            } else if (data.status == 3) {
                                                arrData.completeCount++;
                                            }
                                        }
                                    })
                                })
                                customProgram(null, arrd);
                            }
                        })
                    } else {
                        customProgram(null, arrd);
                    }
                }
            })
        }, function (dietPlan) {
            var dietArr = [];
            var dataIdArray = [];
            userPlan.find({userId: req.authenticationId, planType: 4}).populate('dietPlanId', "_id title").populate("userPlanId").lean().exec(function (err, dietPlanResult) {
                if (err) {
                    callback('err', null);
                } else {
                    if (dietPlanResult.length > 0) {
                        dietPlanResult.forEach(function (data) {
                            var tempObj = {
                                title: data.dietPlanId.title,
                                completeCount: 0,
                                inCompleteCount: 0,
                                totalProgram: 0,
                                userPlanId: data._id,
                                dietPlanId: data.dietPlanId._id,
                            };
                            dietArr.push(tempObj);
                            dataIdArray.push(data._id);
                        })
                        userMeals.find({userPlanId: {$in: dataIdArray}}, function (err, result) {
                            if (err) {
                                callback('err')
                            } else {
                                result.forEach(function (mealsResult) {
                                    dietArr.forEach(function (mealsId) {
                                        if (mealsId.userPlanId.equals(mealsResult.userPlanId) && mealsId.dietPlanId.equals(mealsResult.dietPlanId)) {
                                            mealsId.totalProgram++
                                            if (mealsResult.isCompleted) {
                                                mealsId.completeCount++;
                                            } else {
                                                mealsId.inCompleteCount++;
                                            }
                                        }
                                    })
                                })
                                dietPlan(null, dietArr);
                            }
                        })
                    } else {
                        dietPlan(null, dietArr);
                    }

                }
            })
        },
        function (customeDietPlan) {
            var dietArr1 = [];
            var dietIdArr = []
            userPlan.find({userId: req.authenticationId, planType: 5}).populate('dietPlanId', "_id title").populate("userPlanId").lean().exec(function (err, dietPlanResult) {
                if (err) {
                    customeDietPlan('err', null);
                } else {
                    if (dietPlanResult.length > 0) {
                        dietPlanResult.forEach(function (data) {
                            var tempObj = {
                                title: data.dietPlanId.title,
                                inCompleteCount: 0,
                                completeCount: 0,
                                totalProgram: 0,
                                userPlanId: data._id,
                                dietPlanId: data.dietPlanId._id
                            };
                            // userPlanIdArr.push(data._id);
                            dietIdArr.push(data._id)
                            dietArr1.push(tempObj);
                        })
                        userMeals.find({userPlanId: {$in: dietIdArr}}, function (err, result) {
                            if (err) {
                                customeDietPlan('err', null)
                            } else {
                                result.forEach(function (mealsResult) {
                                    dietArr1.forEach(function (mealsId) {
                                        if (mealsId.userPlanId.equals(mealsResult.userPlanId) && mealsId.dietPlanId.equals(mealsResult.dietPlanId)) {
                                            mealsId.totalProgram++
                                            if (mealsResult.isCompleted) {
                                                mealsId.completeCount++;
                                            } else {
                                                mealsId.inCompleteCount++;
                                            }
                                        }
                                    })
                                })
                                customeDietPlan(null, dietArr1);
                            }
                        })
                        // userMeals.find({ userPlanId: { $in: { dietIdArr } } }).populate("mealId").exec(function (err, data) {
                        //     if (err) {
                        //         customeDietPlan("err", null);
                        //     }
                        //     else {
                        //         data.forEach(function (mealsData) {
                        //             dietArr1.forEach(function (dietArrResult) {
                        //                 if (dietArrResult.userPlanId.equals(mealsData.userPlanId) && dietArrResult.dietPlanId.equals(mealsData.dietPlanId)) {
                        //                     dietArrResult.totalProgram++
                        //                     if (mealsData.isCompleted) {
                        //                         dietArrResult.completeCount++;
                        //                     } else {
                        //                         dietArrResult.inCompleteCount++;
                        //                     }

                        //                     // if (mealsResult.isCompleted) {
                        //                     //     mealsId.completeCount++;
                        //                     // } else {
                        //                     //     mealsId.inCompleteCount++;
                        //                     // }
                        //                 }
                        //             })


                        //         })
                        //         customeDietPlan(null, dietArr1);
                        //     }

                        // })
                    } else {
                        customeDietPlan(null, dietArr1);
                    }
                }
            })
        },
        function (classResult) {
            var classArr = [];
            userPlan.find({userId: req.authenticationId, planType: 1}).populate('planId').lean().exec(function (err, classData) {
                if (err) {
                    classResult('err', null);
                } else {
                    if (classData.length > 0) {
                        classData.forEach(function (result) {
                            var startdDateTime = new Date(result.planId.startDateTime).toISOString().slice(0, 10);

                            var endDateTime = new Date(result.planId.endDateTime).toISOString().slice(0, 10);
                            var tempObj = {
                                title: result.planId.title,
                                startDate: startdDateTime,
                                endDate: endDateTime
                            };
                            if (result.planId.isCompleted) {
                                tempObj.isActive = "Active";
                            } else {
                                tempObj.isActive = "In Active";
                            }
                            classArr.push(tempObj)
                        })
                        classResult(null, classArr);
                    } else {
                        classResult(null, classArr);
                    }
                }
            })
        },
        function (workshopResult) {
            var workShopArr = [];
            userPlan.find({userId: req.authenticationId, planType: 2}).populate('planId').lean().exec(function (err, workshopData) {
                if (err) {
                    workshopResult('err', null);
                } else {
                    if (workshopData > 0) {
                        workshopData.forEach(function (result) {
                            var startdDateTime = new Date(result.planId.startDateTime).toISOString().slice(0, 10);
                            var endDateTime = new Date(result.planId.endDateTime).toISOString().slice(0, 10);
                            var tempObj = {
                                title: result.planId.title,
                                startDate: startdDateTime,
                                endDate: endDateTime
                            };
                            if (result.planId.isCompleted) {
                                tempObj.isActive = "Active";
                            } else {
                                tempObj.isActive = "In Active";
                            }
                            workShopArr.push(tempObj)
                        })
                        workshopResult(null, workShopArr);
                    } else {
                        workshopResult(null, workShopArr);
                    }
                }
            })
        }
    ], function (err, results) {
        if (err) {
            callback('err');
        } else {
            console.log("results", results);
            var searchResult = results.filter(function (element) {
                return element != null;
            });
            callback(searchResult);
        }
    })
}
exports.downloadPersonalInformation = downloadPersonalInformation;

var getSubscriptionPlan = function (req, callback) {
    subscriptionPlan.find({userType: req.body.type}).lean().exec(function (err, result) {
        if (err || result.length == 0) {
            callback('err');
        } else {
            ptdtSubscriptionPlan.findOne({'ptdtId': req.authenticationId, isActive: true, isDeleted: false}).lean().exec(function (err1, data) {
                if (err1) {
                    callback('err');
                } else {
                    var resultArray = result.map(function (element) {
                        if (data && data.subscriptionPlanId.equals(element._id)) {
                            element.isPurchased = true;
                        } else {
                            element.isPurchased = false;
                        }
                        return element;
                    })
                    callback(resultArray);
                }
            })
        }
    })
}
exports.getSubscriptionPlan = getSubscriptionPlan;

var addSubscriptionPlan = function (req, callback) {
    subscriptionPlan.find({userType: req.body.userType, type: req.body.type}, function (err, result) {
        if (result) {
            callback('err');
        } else {
            var addSubscriptionPlanObj = {
                title: req.body.title,
                description: req.body.description,
                userType: req.body.userType,
                type: req.body.type,
                duration: req.body.duration,
                amount: req.body.amount
            }
            subscriptionPlan(addSubscriptionPlanObj).save(function (err, result) {
                if (err || result == null) {
                    callback('err');
                } else {
                    callback(result);
                }
            })
        }
    })
}
exports.addSubscriptionPlan = addSubscriptionPlan;

var getProgramsDietsGoalsTypes = function (req, callback) {
    programsDietGoals.find({'planType': req.body.planType}, function (err, result) {
        if (err || result.length == 0) {
            callback('err', null);
        } else {
            programsType.find({'planType': req.body.planType}, function (err1, result1) {
                if (err1) {
                    callback('err', null);
                } else {
                    var response = {
                        goals: result,
                        types: result1
                    };
                    callback(null, response);
                }
            })
        }
    })
}
exports.getProgramsDietsGoalsTypes = getProgramsDietsGoalsTypes;

var chkPtDtSubscriptionExpiry = function (userId, callback) {
    try {
        ptdtSubscriptionPlan.findOne({'ptdtId': userId, isActive: true}).exec(function (err, data) {
            if (err) {
                callback(false);
            } else {
                if (data !== null) {
                    callback(true);
                } else {
                    callback(false);
                }
            }
        });
    } catch (exception) {
        throw Error("Error while getting data");
    }
}
exports.chkPtDtSubscriptionExpiry = chkPtDtSubscriptionExpiry;

var chkPtDtSubscriptionExpiryAsync = async function (userId) {
    try {
        var result = await ptdtSubscriptionPlan.findOne({'ptdtId': userId, isActive: true});
        if (result !== null) {
            return true;
        } else {
            return false;
        }
    } catch (exception) {
        throw Error("Error while getting data");
    }
}
exports.chkPtDtSubscriptionExpiryAsync = chkPtDtSubscriptionExpiryAsync;

var generatePtdtInvoice = function (req, res, invoiceId, callback) {
    invoice.findOne({_id: invoiceId}).populate({path: 'subscriptionPlanId userId', populate: {path: 'city country'}}).exec(function (err, result) {
        var invoiceDate = new Date(result.created_at).toISOString().slice(0, 10);
        if (err || result == null) {
            callback('err');
        } else {
            var invoice = fs.readFileSync('views/invoice.hbs', 'utf8');
            var options = {
                format: "A2",
                orientation: "portrait",
                border: "10mm"
            };
            var document = {
                type: 'file', // 'file' or 'buffer'
                template: invoice,
                context: {
                    userData: result.userId,
                    invoiceData: result,
                    subscriptionData: result.subscriptionPlanId,
                    invoiceDate: invoiceDate,
                    BASE_URL: constants.BASE_URL,
                },
                path: "./public/pdf/userInvoice/" + req.authenticationId + ".pdf"
            };
            pdf.create(document, options)
                    .then(data => {
                        /******* mail code **/
                        var templateVariable = {
                            templateURL: 'mailtemplate/invoice',
                            fullName: `${result.userId.firstName} ${result.userId.lastName}`,
                            planName: `${result.subscriptionPlanId.title}`,
                            BASE_URL: constants.BASE_URL,
                            API_VERSION: constants.API_VERSION,
                            layout: false
                        };
                        var mailParamsObject = {
                            templateVariable: templateVariable,
                            to: result.userId.email,
                            subject: "Invoice",
                            attachments: [
                                {
                                    filename: 'Invoice.pdf',
                                    filePath: "./public/pdf/userInvoice/" + req.authenticationId + ".pdf" // stream this file
                                }
                            ]
                        };
                        mailer.sendMail(req, res, mailParamsObject, function (err1) {
                            if (err1) {
                                console.log('Error..................', err1);
                                callback('err');
                            } else {
                                callback('Success');
                            }
                        });
                    })
                    .catch(error => {
                        console.log('Promise Error....................', error);
                        callback('err');
                    })
        }
    })
}
exports.generatePtdtInvoice = generatePtdtInvoice;

var getAllSubscriptionPlan = function (type, callback) {
    subscriptionPlan.find({userType: type}).lean().exec(function (err, result) {
        if (err || result.length == 0) {
            callback('err');
        } else {
            callback(result);
        }
    })
}
exports.getAllSubscriptionPlan = getAllSubscriptionPlan;
