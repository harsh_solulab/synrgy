const userPlan = require('../../models/user_plans.model');
const userMeals = require("../../models/user_meals.model");
const meals = require('../../models/meals.model')
const dietPlan = require('../../models/diet_programs.model')
const user = require('../../models/user.model')
const programs = require('../../models/program.model');
const _ = require('lodash')
const ptdtSubscriptionPlan = require('../../models/ptdt_subscription.model')
const userSession = require('../../models/user_session.model')
const moment = require('moment');
const commonService = require("../../services/v1/common.service");
const userService = require("../../services/v1/user.service");
const noti = require('../../modules/push_notifications');
var async = require('async');
var checkDietPlanExpiry = function (req, callback) {
    userPlan.updateMany({ dietExpiredDate: { $lte: Date.now() }, isExpired: false, planType: { $gte: 4 } }, { $set: { isExpired: true } }, function (err, result) {
        if (err) {
            callback('err');
        } else {
            callback(result);
        }
    })
}
exports.checkDietPlanExpiry = checkDietPlanExpiry;

var dietMealsExpiry = function (req, callback) {
    // userPlan.update({}, { $set: { 'meals.$[element].isCompleted': true } },{ multi: true,
    //     arrayFilters: [ {'element.planDate': { $lt: Date.now() } } ] }, function (err, result) {
    //     console.log(err)
    //     if (err) {
    //         callback('err');
    //     }
    //     else {
    //         callback(result);
    //     }
    // })
    userMeals.updateMany({ planDate: { $lte: Date.now() } }, { $set: { isCompleted: true } }, function (err, result) {
        if (err) {
            callback('err');
        } else {
            callback(result);
        }
    })

}
exports.dietMealsExpiry = dietMealsExpiry;


var workShopClassExpiry = function (req, callback) {
    programs.updateMany({ endDateTime: { $lte: Date.now() }, isCompleted: false }, { $set: { isCompleted: true } }, function (err, result) {
        if (err) {
            callback('err')
        } else {
            console.log(result);
            callback(result)
        }
    })
}
exports.workShopClassExpiry = workShopClassExpiry;

var ptdtSubscriptionExpiry = function (req, callback) {
    ptdtSubscriptionPlan.updateMany({ 'renewalDate': { $lte: Date.now() }, isDeleted: false, isActive: true }, { $set: { 'isActive': false } }, function (err, result) {
        if (err) {
            callback('err');
        } else {
            callback(result);
        }
    })
}
exports.ptdtSubscriptionExpiry = ptdtSubscriptionExpiry;

var updateUserSessionExpiry = function (req, callback) {
    userSession.updateMany({ endTime: { $lte: Date.now() }, status: 1 }, { $set: { status: 3, remainingDuration: 0 } }, function (err, result) {
        if (err) {
            callback('err')
        } else {
            callback(result)
        }
    })
}
exports.updateUserSessionExpiry = updateUserSessionExpiry;

var sendClassNotification = function (req, callback) {
    userPlan.find({
        $and: [{ $or: [{ planType: 1 }, { planType: 2 }], sendNotificationTime: { $lte: Date.now() }, notiFlag: false, isExpired: false }]
    }).populate("planId").exec(function (err, data) {
        console.log("data.............=>", data)
        console.log("data.length.............=>", data.length)
        if (err) {
            callback('err');
        } else {
            if (data.length > 0) {
                data.forEach(function (userPlanResult) {
                    if (userPlanResult.planType == 1) {
                        var objNotification = {
                            for: userPlanResult.userId,
                            programId: userPlanResult.planId._id,
                            notification: userPlanResult.planId.title + " class will start after 15 mins",
                            type: 'class reminder',
                            planType: 1
                        };
                    } else {
                        var objNotification = {
                            for: userPlanResult.userId,
                            programId: userPlanResult.planId._id,
                            notification: userPlanResult.planId.title + " workshop will start after 15 mins",
                            type: 'workshop reminder',
                            planType: 2
                        };
                    }
                    commonService.saveNotification(objNotification, function (err2, result) {
                        if (err2) {
                            callback('err')
                        } else {
                            /************* Send Notification *********************/
                            var userData = {
                                body: {
                                    userId: userPlanResult.userId,
                                    isMine: "noti"
                                },
                                authenticationId: userPlanResult.ptdtId
                            }
                            userService.getUserInfo(userData, function (user) {
                                if (user == 'err') {
                                    callback('err');
                                } else {
                                    noti.sendNotifications(user.accounts, objNotification.type, objNotification.notification, { "id": userPlanResult.planId._id, planType: userPlanResult.planType, userType: 0 }, function (notification) {

                                        if (notification == 'err') {
                                            callback('err');
                                        } else {
                                            userPlan.update({ _id: userPlanResult._id, notiFlag: false }, { $set: { notiFlag: true } }, function (err, data) {
                                                if (err) {
                                                    callback('err');
                                                } else {
                                                    callback(data);
                                                }
                                            })

                                        }
                                    });
                                }
                            });
                        }
                    })
                })
            }
        }
    })
}
exports.sendClassNotification = sendClassNotification;

var planExpirationNotification = function (req, callback) {
    ptdtSubscriptionPlan.find({ $and: [{ $or: [{ userType: 1 }, { userType: 2 }], sendNotificationTime: { $lte: Date.now() }, notiFlag: false, isActive: true, isDeleted: false }] }, function (err, result) {
        console.log(result)
        if (err) {
            callback('err');
        }
        else {
            if (result.length > 0) {
                result.forEach(function (data) {
                    if (data.userType == 1) {
                        var objNotification = {
                            for: data.ptdtId,
                            to: data._id,
                            notification: "Your subscription plan will be expired after 3 days. Upgrade or renew before it ends",
                            type: 'Plan Expiration',
                            planType: 6,
                            userType: 1
                        };
                    } else {
                        var objNotification = {
                            for: data.ptdtId,
                            to: data._id,
                            notification: "Your subscription plan will be expired after 3 days. Upgrade or renew before it ends",
                            type: 'Plan Expiration',
                            planType: 7,
                            userType: 2
                        };
                    }
                    commonService.saveNotification({ for: objNotification.for, to: objNotification.to, notification: objNotification.notification, type: objNotification.type, planType: objNotification.planType }, function (err2, result) {
                        if (err2) {
                            callback('err')
                        } else {
                            /************* Send Notification *********************/
                            var userData = {
                                body: {
                                    userId: data.ptdtId,
                                    isMine: "noti"
                                },
                                authenticationId: data.ptdtId
                            }
                            userService.getUserInfo(userData, function (user) {
                                if (user == 'err') {
                                    callback('err');
                                } else {
                                    noti.sendNotifications(user.accounts, objNotification.type, objNotification.notification, { planType: 6, userType: objNotification.userType }, function (notification) {
                                        if (notification == 'err') {
                                            callback('err');
                                        } else {
                                            ptdtSubscriptionPlan.update({ _id: data._id, notiFlag: false }, { $set: { notiFlag: true } }, function (err, data) {
                                                if (err) {
                                                    callback('err');
                                                } else {
                                                    callback(data);
                                                }
                                            })
                                        }
                                    });
                                }
                            });
                        }
                    })
                })
            } else {
                callback(result)

            }
        }

    })
}
exports.planExpirationNotification = planExpirationNotification;

var deleteFreezedAccount = function (req, callback) {
    var deletedDate = new Date();
    deletedDate.setDate(deletedDate.getDate() + 180);
    user.updateMany({ accountFreezedDate: { $lte: Date.now() }, isDeleted: false, isFreezed: true }, { $set: { isDeleted: true, accountDeletedDate: moment(deletedDate).valueOf() } }, function (err, data) {
        if (err) {
            callback('err')
        } else {
            callback(data);
        }
    })
}
exports.deleteFreezedAccount = deleteFreezedAccount;

var permanentDeleteAccount = function (req, callback) {
    user.deleteMany({ 'accountDeletedDate': { $lte: Date.now() }, isDeleted: true }, function (err, result) {
        if (err) {
            callback('err');
        } else {
            callback(result);
        }
    })
}
exports.permanentDeleteAccount = permanentDeleteAccount

var userSessionCompleted = function (req, callback) {
    var userPlanId = []
    var flag = true;
    var completedId = []
    userPlan.distinct('_id', {
        $and: [{
            $or: [{ planType: 0, planType: 3 }],
            isExpired: false
        },
        ]
    }, function (err, userPlanResult) {
        if (err) {
            callback('err');
        } else {
            userSession.find({ status: { $ne: 3 } }, function (err, data1) {
                if (err) {
                    callback('err');
                }
                else {
                    userPlanResult.forEach(function (userPlaFor) {
                        data1.forEach(function (dataResult) {
                            completedId.push(dataResult.userPlanId);
                        })
                        var flag = _.find(completedId, userPlaFor);

                        if (flag == undefined) {
                            userPlanId.push(userPlaFor);
                        }
                    })
                    userPlan.updateMany({ "_id": userPlanId }, { $set: { isExpired: true } }, function (err, data) {
                        if (err) {
                            callback('err');
                        } else {
                            callback(data);
                        }
                    })
                }
            })
        }
    })
}
exports.userSessionCompleted = userSessionCompleted


