const user = require('../../models/user.model');
const notification = require('../../models/notification.model');
const avatars = require('../../models/avatar.model');
const countries = require('../../models/countries.model');
const diets = require('../../models/diet_programs.model');
const userSession = require('../../models/user_session.model');
const programs = require('../../models/program.model');
const cities = require('../../models/cities.model');
const states = require('../../models/states.model');
const meals = require('../../models/meals.model');
const session = require('../../models/session.model');
const userPictures = require('../../models/user_pictures.model');
const follow = require('../../models/follow.model');
const invoice = require('../../models/invoice.model');
const userMeals = require("../../models/user_meals.model")
const userPlan = require('../../models/user_plans.model');
const mailer = require('../../modules/mailer.helper');
const ptdtSubscriptionPlan = require('../../models/ptdt_subscription.model');
const constants = require('../../config/constants');
const _ = require('lodash');
const bcrypt = require('bcryptjs');
const async = require('async');
const userService = require('../../services/v1/user.service');
const ptdtService = require('../../services/v1/ptdt.service');
const moment = require('moment');
const noti = require('../../modules/push_notifications');
const conversation = require('../../models/conversation.model');
const feeds = require('../../models/feeds.model');
const comments = require('../../models/feeds_comments.model');
const firebaseFunction = require('../../modules/firebase_functions');


// function call to get price bifurcation

var getChargesBifurcation = function (amount, commision, callback) {

    var objReturn = {
        ptdtPrice: 0.00,
        transactionCharge: 0.00,
        synrgyAmount: 0.00,
        amountPaidByUser: 0.00
    };

    var paytabsChargesPercentage = constants.PAYTABS_CHARGES_PERCENTAGE;
    //var synergyComission = constants.SYNERGY_COMISSION;
    var ptdtCommision = (commision == undefined) ? 0.00 : commision;
    var amountPaidByUser = (parseFloat(amount)).toFixed(2);
    var paytabsCharges = (parseFloat(parseFloat(amount * paytabsChargesPercentage) / 100)).toFixed(2);
    var amountAfterTransectionCharge = (parseFloat(amountPaidByUser - paytabsCharges)).toFixed(2);
    var synrgyAmount = (parseFloat(parseFloat(amountAfterTransectionCharge * ptdtCommision) / 100)).toFixed(2);
    var ptdtPrice = (parseFloat(amountPaidByUser - synrgyAmount - paytabsCharges)).toFixed(2);

    var objReturn = {
        ptdtPrice: ptdtPrice,
        transactionCharge: paytabsCharges,
        synrgyAmount: synrgyAmount,
        amountPaidByUser: amountPaidByUser
    };

    console.log(objReturn);
    console.log(amountPaidByUser);
    console.log(paytabsCharges);
    console.log(amountAfterTransectionCharge);
    console.log(synrgyAmount);
    console.log(ptdtPrice);

    callback(objReturn);

}

exports.getChargesBifurcation = getChargesBifurcation;


exports.follow = function (req, callback) {
    var followObj = {
        followedBy: req.authenticationId,
        followedTo: req.body.id,
        firstName: req.body.firstName,
        lastName: req.body.lastName
    };
    followUnfollow(followObj, parseInt(req.body.followStatus), req.authenticationId, function (err, result) {
        if (err || result == null) {
            callback('err', null);
        } else {
            callback(null, result);
        }
    })
}

var followUnfollow = function (followObj, followStatus, authenticationId, callback) {
    if (followStatus == 1) {
        follow.findOne({ followedBy: followObj.followedBy, followedTo: followObj.followedTo }, function (err, data) {
            if (err) {
                callback('err', null);
            } else {
                if (data) {
                    callback(null, 'User already followed!');
                } else {
                    follow({ followedBy: followObj.followedBy, followedTo: followObj.followedTo }).save(function (err, result) {
                        if (err || result == null) {
                            callback('err', null);
                        } else {

                            var objNotification = {
                                for: followObj.followedTo,
                                to: followObj.followedBy,
                                notification: followObj.firstName + ' ' + followObj.lastName + ' has started following you',
                                type: 'follow',
                                planType: 8
                            };
                            saveNotification(objNotification, function (err1, data) {
                                if (err1 || data == null) {
                                    callback('err', null);
                                } else {
                                    /************* Send Notification *********************/
                                    var userData = {
                                        body: {
                                            userId: followObj.followedTo,
                                            isMine: "noti"
                                        },
                                        authenticationId: authenticationId
                                    };
                                    userService.getUserInfo(userData, function (user) {
                                        if (user == 'err') {
                                            callback('err', null);
                                        } else {
                                            var userData1 = {
                                                body: {
                                                    userId: followObj.followedBy,
                                                    isMine: "noti"
                                                },
                                                authenticationId: authenticationId
                                            };
                                            userService.getUserInfo(userData1, function (userData2) {
                                                if (userData2 == 'err') {
                                                    callback('err', null);
                                                } else {
                                                    noti.sendNotifications(user.accounts, "Started Following", followObj.firstName + " " + followObj.lastName + " has strarted following you", { "id": followObj.followedBy, planType: 8, userType: userData2.type }, function (notification) {
                                                        if (notification == 'err') {
                                                            callback('err', null);
                                                        } else {
                                                            feeds.updateMany({ 'userId': followObj.followedTo }, { $push: { followers: followObj.followedBy } }, function (err2, feed) {
                                                                if (err2) {
                                                                    console.log(err2);
                                                                    callback('err', null);
                                                                } else {
                                                                    console.log("User Followed Successfully");
                                                                    callback(null, "User Followed Successfully");
                                                                }
                                                            })
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                    /************* Send Notification *********************/
                                }
                            });
                        }
                    });
                }
            }
        });
    } else {
        follow.deleteOne({ 'followedBy': followObj.followedBy, 'followedTo': followObj.followedTo }, function (err, result) {
            if (err || result == null) {
                callback('err', null);
            } else {
                feeds.updateMany({ 'userId': followObj.followedTo }, { $pull: { followers: followObj.followedBy } }, function (err2, feed) {
                    if (err2) {
                        console.log(err2);
                        callback('err', null);
                    } else {
                        console.log("User unfollowed Successfully");
                        callback(null, "User unfollowed Successfully");
                    }
                })
            }
        })
    }
}
exports.followUnfollow = followUnfollow;

// Update Password
exports.changePwd = function (req, callback) {
    user.findOne({ _id: req.authenticationId }, 'password', function (err, authUser) {
        if (err || authUser == null) {
            callback('err', null);
        } else {
            if (bcrypt.compareSync(req.body.old_password, authUser.password) === true) {
                userService.genPassHashs(req.body.new_password, function (hashedPassword) {
                    user.updateOne({ _id: authUser._id },
                        { $set: { password: hashedPassword }, $pull: { accounts: { accessToken: { $ne: req.authToken } } } },
                        function (err, newUser) {
                            callback(null, newUser);
                        });
                });
            } else {
                callback('Invalid Old Password', null);
            }
        }
    })
}

exports.getCountries = function (req, callback) {
    var countryList = ["17", "104", "117", "165", "178", "191", "229"];
    countries.find({ id: { $in: countryList } }, function (err, result) {
        if (err || result.length == 0) {
            callback('err');
        } else {
            callback(result);
        }
    })
}

exports.getCities = function (req, callback) {
    countries.findOne({ '_id': req.body.country }, function (err, countryResult) {
        if (err || countryResult == null) {
            callback('err', null);
        } else {
            states.distinct('id', { 'country_id': countryResult.id }, function (err, statesList) {
                if (err || statesList.length == 0) {
                    callback('err', null);
                } else {
                    cities.find({ 'state_id': { $in: statesList } }, function (err1, cityList) {
                        if (err1 || cityList.length == 0) {
                            callback('err', null);
                        } else {
                            callback(null, cityList);
                        }
                    })
                }
            });
        }
    });
}

var getFollowers = function (req, callback) {
    var page = parseInt(req.body.skip * req.body.limit);
    var limit = parseInt(req.body.limit);
    follow.find({ 'followedTo': req.body.userId, 'isDeleted': false, 'isBlocked': false, 'isFreezed': false }, { followedBy: 1 }).limit(limit).skip(page).lean().exec(function (err, result) {
        console.log('Result......................', result);
        if (err) {
            callback('err');
        } else {
            if (result.length > 0) {
                var result1 = result.map(function (index) {
                    console.log('Index................', index);
                    return index.followedBy;
                })
                follow.distinct('followedTo', { 'followedTo': { $in: result1 }, 'followedBy': req.authenticationId }, function (err1, filteredResult) {
                    if (err1) {
                        callback('err');
                    } else {

                        var resultArray = result1.map(function (element) {
                            console.log("resultArray", element);
                            var flag = _.find(filteredResult, element);
                            console.log("flag...", flag)
                            if (flag !== undefined) {
                                return obj = {
                                    userData: element,
                                    isFollow: true
                                }
                            } else {
                                return obj = {
                                    userData: element,
                                    isFollow: false
                                }
                            }
                        })
                        console.log("resultArray", resultArray);
                        user.populate(resultArray, { path: 'userData', select: '_id firstName lastName profilePic type' }, function (err2, data) {

                            if (err2) {
                                callback('err');
                            } else {
                                follow.distinct('followedBy', { 'followedTo': req.body.userId, 'isDeleted': false, 'isBlocked': false, 'isFreezed': false }).lean().exec(function (err3, followArray) {
                                    if (err3) {
                                        callback('err');
                                    } else {
                                        var response = {
                                            result: data,
                                            total_count: followArray.length
                                        };
                                        callback(response);
                                    }
                                })
                            }
                        })
                    }
                })
            } else {
                callback({ result: [], total_count: 0 });
            }
        }
    })
};
exports.getFollowers = getFollowers;

var getFollowings = function (req, callback) {
    var page = parseInt(req.body.skip * req.body.limit);
    var limit = parseInt(req.body.limit);
    follow.find({ 'followedBy': req.body.userId, 'isDeleted': false, 'isBlocked': false, 'isFreezed': false }, { followedTo: 1 }).limit(limit).skip(page).lean().exec(function (err, result) {
        console.log("Result..................", result)
        if (err) {
            callback('err');
        } else {
            if (result.length > 0) {
                var result1 = result.map(function (index) {
                    return index.followedTo;
                })
                follow.distinct('followedTo', { 'followedTo': { $in: result1 }, 'followedBy': req.authenticationId }, function (err1, filteredResult) {
                    if (err1) {
                        callback('err');
                    } else {
                        var resultArray = result1.map(function (element) {
                            console.log("resultArray", element);
                            var flag = _.find(filteredResult, element);
                            console.log("flag", flag);
                            if (flag !== undefined) {
                                return obj = {
                                    userData: element,
                                    isFollow: true
                                }
                            } else {
                                return obj = {
                                    userData: element,
                                    isFollow: false
                                }
                            }
                        })
                        user.populate(resultArray, { path: 'userData', select: '_id firstName lastName profilePic type' }, function (err2, data) {
                            console.log("data", data)
                            if (err2) {
                                callback('err');
                            } else {
                                follow.distinct('followedTo', { 'followedBy': req.body.userId, isDeleted: false, isBlocked: false, isFreezed: false }).lean().exec(function (err3, followArray) {
                                    console.log(followArray)
                                    if (err3) {
                                        callback('err');
                                    } else {
                                        var response = {
                                            result: data,
                                            total_count: followArray.length
                                        };
                                        callback(response);
                                    }
                                })
                            }
                        })
                    }
                })
            } else {
                callback({ result: [], total_count: 0 });
            }
        }
    })
};
exports.getFollowings = getFollowings;

var getAvatars = function (req, callback) {
    avatars.find(function (err, result) {
        console.log(result)
        if (err) {
            callback('err', null)
        } else {
            callback(null, result);
        }
    })
};
exports.getAvatars = getAvatars;

var getUserPictures = function (req, callback) {
    userPictures.find({ 'userId': req.authenticationId }, function (err, userPictures) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, userPictures);
        }
    })
}
exports.getUserPictures = getUserPictures;

var getNotifications = function (req, callback) {
    var page = parseInt(req.body.skip * req.body.limit);
    var limit = parseInt(req.body.limit);
    notification.find({ for: req.authenticationId },
        { _id: 1, feedId: 1, planType: 1, type: 1, notification: 1, created_at: 1, updated_at: 1, to: 1, programId: 1, dietProgramId: 1 })
        .limit(limit).skip(page).lean().populate('to', '_id firstName lastName type profilePic')
        .populate('programId', '_id title coverPic description trainingType')
        .populate('dietProgramId', '_id title coverPic dietType').sort({ created_at: 'desc' }).exec(function (err, result) {

            if (err) {
                console.log(err);
                callback("err", null);
            } else {
                result.forEach(function (data) {
                    data.created_at = moment(data.created_at).valueOf();
                    data.updated_at = moment(data.updated_at).valueOf();
                });
                notification.find({ for: req.authenticationId }).lean().exec(function (err1, result1) {
                    if (err1) {
                        callback('err', null);
                    } else {
                        var obj = {
                            result: result,
                            total_count: result1.length
                        };
                        callback(null, obj);
                    }
                })
            }
        });
}
exports.getNotifications = getNotifications;

var saveNotification = function (data, callback) {
    notification(data).save(function (err, result) {
        if (err || result == null) {
            callback('err', null);
        } else {
            callback(null, result);
        }
    })
}
exports.saveNotification = saveNotification;

var getSearchResult = function (req, callback) {
    var keyword = req.body.keyword;
    var userQuery = {
        $and: [{
            $or: [
                { 'nickname': { "$regex": keyword, "$options": "i" } },
                { 'firstName': { "$regex": keyword, "$options": "i" } },
                { 'lastName': { "$regex": keyword, "$options": "i" } }]
        },
        { 'isCertifiedAndApproved': 1 },
        { 'isFreezed': 0 },
        { 'isBlocked': 0 },
        { 'isDeleted': 0 },
        { type: 0 },
        { _id: { $ne: req.authenticationId } }
        ]
    };
    async.parallel([
        function (userCallback) {
            user.find(userQuery, function (err, customer) {
                if (err || customer.length == 0) {
                    userCallback(err, null);
                } else {
                    var result = {
                        label: "In Users",
                        keyword: keyword
                    }
                    userCallback(null, result);
                }
            })
        },
        function (ptCallback) {
            userQuery.$and[5].type = 2;
            user.find(userQuery, function (err, pt) {
                if (err || pt.length == 0) {
                    ptCallback(err, null);
                } else {
                    var result = {
                        label: "In Personal Trainers",
                        keyword: keyword
                    }
                    ptCallback(null, result);
                }
            })
        },
        function (dtCallback) {
            userQuery.$and[5].type = 1;
            user.find(userQuery, function (err, dt) {
                if (err || dt.length == 0) {
                    dtCallback(err, null);
                } else {
                    var result = {
                        label: "In Dietitians",
                        keyword: keyword
                    }
                    dtCallback(null, result);
                }
            })
        },
        function (dietCallback) {
            diets.find({ 'title': { "$regex": keyword, "$options": "i" }, 'isDeleted': 0, 'dietType': { $ne: 5 } }, function (err, diets) {
                if (err || diets.length == 0) {
                    dietCallback(err, null);
                } else {
                    var result = {
                        label: "In Diet Plans",
                        keyword: keyword
                    }
                    dietCallback(null, result);
                }
            })
        },
        function (programCallback) {
            programs.find({ 'title': { "$regex": keyword, "$options": "i" }, 'isCompleted': 0, 'isDeleted': 0, 'trainingType': { $ne: 3 } }, function (err, program) {
                if (err || program.length == 0) {
                    programCallback(err, null);
                } else {
                    var result = {
                        label: "In Programs",
                        keyword: keyword
                    }
                    programCallback(null, result);
                }
            })
        }
    ], function (err, resultArray) {
        if (err) {
            callback('err', null);
        } else {
            var searchResult = resultArray.filter(function (element) {
                return element != null;
            })
            callback(null, searchResult);
        }
    })
}
exports.getSearchResult = getSearchResult;

var getPersonalizedSearchResults = function (req, callback) {

    var keyword = req.body.keyword;
    var label = req.body.label;
    var page = parseInt(req.body.skip * req.body.limit);
    var limit = parseInt(req.body.limit);
    var userQuery = {
        $and: [{
            $or: [
                { 'nickname': { "$regex": keyword, "$options": "i" } },
                { 'firstName': { "$regex": keyword, "$options": "i" } },
                { 'lastName': { "$regex": keyword, "$options": "i" } }]
        },
        { 'isCertifiedAndApproved': 1 },
        { 'isFreezed': 0 },
        { 'isBlocked': 0 },
        { 'isDeleted': 0 },
        { 'emailVerified': 1 },
        { _id: { $ne: req.authenticationId } }
        ]
    };
    var programQuery = {
        'title': { "$regex": keyword, "$options": "i" },
        'isDeleted': 0
    }
    if (label == "In Users") {
        userQuery.$and.push({ type: 0 });
        if (req.body.gender !== undefined) {
            userQuery.$and.push({ gender: req.body.gender });
        }
        if (req.body.lowerAge !== undefined && req.body.upperAge !== undefined) {
            var upperDate = new Date(Date.now())
            upperDate.setFullYear(upperDate.getFullYear() - parseInt(req.body.upperAge));
            console.log("upperDate:", upperDate);
            var lowerDate = new Date(Date.now());
            lowerDate.setFullYear(lowerDate.getFullYear() - parseInt(req.body.lowerAge));
            console.log("lowerDate:", lowerDate);
            console.log(moment(upperDate).valueOf())
            userQuery.$and.push({
                dob: {
                    $gte: moment(upperDate).valueOf(),
                    $lte: moment(lowerDate).valueOf()
                }
            });
        }
        user.find(userQuery).limit(limit).skip(page).lean().exec(function (err, users) {
            if (err) {
                callback('err', null);
            } else {
                var user1 = users.map(function (index) {
                    return index._id;
                })
                follow.distinct('followedTo', { 'followedTo': { $in: user1 }, 'followedBy': req.authenticationId }, function (err1, filteredResult) {
                    if (err1) {
                        callback('err', null);
                    } else {
                        var resultArray = user1.map(function (element) {
                            var flag = _.find(filteredResult, element);
                            if (flag !== undefined) {
                                return obj = {
                                    userData: element,
                                    isFollow: true
                                };
                            } else {
                                return obj = {
                                    userData: element,
                                    isFollow: false
                                };
                            }
                        });
                        user.populate(resultArray, { path: 'userData', select: '_id firstName lastName profilePic type' }, function (err2, data) {
                            if (err2) {
                                callback('err');
                            } else {
                                user.find(userQuery).lean().exec(function (err3, users1) {
                                    if (err3) {
                                        callback('err', null);
                                    } else {
                                        var responseObj = {
                                            result: data,
                                            total_count: users1.length
                                        };
                                        callback(null, responseObj);
                                    }
                                })
                            }
                        });
                    }
                })
            }
        })
    } else if (label == "In Personal Trainers") {
        userQuery.$and.push({ type: 2 });
        if (req.body.gender !== undefined) {
            userQuery.$and.push({ gender: req.body.gender });
        }
        if (req.body.lowerAge !== undefined && req.body.upperAge !== undefined) {
            var upperDate = new Date(Date.now())
            upperDate.setFullYear(upperDate.getFullYear() - parseInt(req.body.upperAge));
            console.log("upperDate:", upperDate);
            var lowerDate = new Date(Date.now());
            lowerDate.setFullYear(lowerDate.getFullYear() - parseInt(req.body.lowerAge));
            console.log("lowerDate:", lowerDate);
            console.log(moment(upperDate).valueOf())
            userQuery.$and.push({
                dob: {
                    $gte: moment(upperDate).valueOf(),
                    $lte: moment(lowerDate).valueOf()
                }
            });
        }
        user.find(userQuery).limit(limit).skip(page).lean().exec(function (err, pt) {
            if (err) {
                callback(err, null);
            } else {
                var pt1 = pt.map(function (index) {
                    return index._id;
                })
                follow.distinct('followedTo', { 'followedTo': { $in: pt1 }, followedBy: req.authenticationId }, function (err1, filteredResult) {
                    if (err1) {
                        callback(err1, null);
                    } else {
                        var resultArray = pt1.map(function (element) {
                            var flag = _.find(filteredResult, element);
                            if (flag !== undefined) {
                                return obj = {
                                    userData: element,
                                    isFollow: true
                                };
                            } else {
                                return obj = {
                                    userData: element,
                                    isFollow: false
                                };
                            }
                        });
                        user.populate(resultArray, { path: 'userData', select: '_id firstName lastName profilePic type' }, function (err2, data) {
                            if (err2) {
                                callback(err2, null);
                            } else {
                                user.find(userQuery).lean().exec(function (err3, data1) {
                                    if (err3) {
                                        callback(err3, null);
                                    } else {
                                        var responseObj = {
                                            result: data,
                                            total_count: data1.length
                                        };
                                        callback(null, responseObj);
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    } else if (label == "In Dietitians") {
        userQuery.$and.push({ type: 1 });
        if (req.body.gender !== undefined) {
            userQuery.$and.push({ gender: req.body.gender });
        }
        if (req.body.lowerAge !== undefined && req.body.upperAge !== undefined) {
            var upperDate = new Date(Date.now())
            upperDate.setFullYear(upperDate.getFullYear() - parseInt(req.body.upperAge));
            console.log("upperDate:", upperDate);
            var lowerDate = new Date(Date.now());
            lowerDate.setFullYear(lowerDate.getFullYear() - parseInt(req.body.lowerAge));
            console.log("lowerDate:", lowerDate);
            console.log(moment(upperDate).valueOf())
            userQuery.$and.push({
                dob: {
                    $gte: moment(upperDate).valueOf(),
                    $lte: moment(lowerDate).valueOf()
                }
            });
        }
        user.find(userQuery).limit(limit).skip(page).lean().exec(function (err, dt) {
            console.log(dt)
            if (err) {
                callback('err', null);
            } else {
                var dt1 = dt.map(function (index) {
                    return index._id;
                })
                follow.distinct('followedTo', { 'followedTo': { $in: dt1 }, followedBy: req.authenticationId }, function (err1, filteredResult) {
                    if (err1) {
                        callback('err', null);
                    } else {
                        var resultArray = dt1.map(function (element) {
                            var flag = _.find(filteredResult, element);
                            if (flag !== undefined) {
                                return obj = {
                                    userData: element,
                                    isFollow: true
                                };
                            } else {
                                return obj = {
                                    userData: element,
                                    isFollow: false
                                };
                            }
                        });
                        user.populate(resultArray, { path: 'userData', select: '_id firstName lastName profilePic type' }, function (err2, data) {
                            if (err2) {
                                callback('err', null);
                            } else {
                                user.find(userQuery).lean().exec(function (err3, data1) {
                                    if (err3) {
                                        callback('err', null);
                                    } else {
                                        var responseObj = {
                                            result: data,
                                            total_count: data1.length
                                        };
                                        callback(null, responseObj);
                                    }
                                })
                            }
                        })

                    }
                })
            }
        })
    } else if (label == "Customize") {
        user.find(userQuery, { firstName: 1, lastName: 1, profilePic: 1, type: 1 }).limit(limit).skip(page).lean().exec(function (err, customize) {
            if (err) {
                callback('err', null)
            } else {
                user.find(userQuery).lean().exec(function (err1, customize1) {
                    if (err1) {
                        callback('err', null);
                    } else {
                        var responseObj = {
                            result: customize,
                            total_count: customize1.length
                        };
                        callback(null, responseObj);
                    }
                })
            }
        })
    } else if (label == "In Diet Plans") {
        // programQuery.isExpired = 0;
        programQuery.dietType = { $ne: 5 };
        if (req.body.goal !== undefined) {
            programQuery.goal = req.body.goal;
        }
        if (req.body.lowerPrice !== undefined && req.body.upperPrice !== undefined) {
            programQuery.price = { $gte: parseInt(req.body.lowerPrice), $lte: parseInt(req.body.upperPrice) };
        }
        // if (req.body.lowerMealCount !== undefined && req.body.upperMealCount !== undefined) {
        //     programQuery.mealsPerDay = {$gte: parseInt(req.body.lowerMealCount), $lte: parseInt(req.body.upperMealCount)};
        // }
        diets.find(programQuery, { title: 1, coverPic: 1, dietType: 1 }).limit(limit).skip(page).lean().exec(function (err, diet) {
            if (err) {
                callback('err', null);
            } else {
                diets.find(programQuery).lean().exec(function (err1, diet1) {
                    if (err1) {
                        callback('err', null);
                    } else {
                        var responseObj = {
                            result: diet,
                            total_count: diet1.length
                        };
                        callback(null, responseObj);
                    }
                })
            }
        })
    } else {
        programQuery.isCompleted = 0;
        programQuery.trainingType = { $ne: 3 };
        if (req.body.trainingFilterType == 'programs') {
            programQuery.trainingType = 0;
            if (req.body.goal !== undefined) {
                programQuery.programGoal = req.body.goal;
            }
            if (req.body.programType !== undefined) {
                programQuery.programType = req.body.programType;
            }
            if (req.body.lowerPrice !== undefined && req.body.upperPrice !== undefined) {
                programQuery.price = { $gte: parseInt(req.body.lowerPrice), $lte: parseInt(req.body.upperPrice) };
            }
            if (req.body.lowerDuration !== undefined && req.body.upperDuration !== undefined) {
                programQuery.duration = { $gte: parseInt(req.body.lowerDuration), $lte: parseInt(req.body.upperDuration) };
            }
        } else if (req.body.trainingFilterType == 'classes') {
            programQuery.trainingType = 1;
            if (req.body.startDateTime !== undefined) {
                var startDateTime = parseInt(req.body.startDateTime);
                programQuery.startDateTime = { $gte: startDateTime };
            }
            // if (req.body.startTime !== undefined) {
            //     var startTime = new Date(req.body.startTime * 1000).getTime();
            //     programQuery.filterStartTime = startTime;
            // }
            if (req.body.endDateTime !== undefined) {
                var endDateTime = parseInt(req.body.endDateTime);
                programQuery.endDateTime = { $lte: endDateTime };
            }
            if (req.body.lowerPrice !== undefined && req.body.upperPrice !== undefined) {
                programQuery.price = { $gte: parseInt(req.body.lowerPrice), $lte: parseInt(req.body.upperPrice) };
            }
            if (req.body.lowerDuration !== undefined && req.body.upperDuration !== undefined) {
                programQuery.duration = { $gte: parseInt(req.body.lowerDuration), $lte: parseInt(req.body.upperDuration) };
            }
        } else if (req.body.trainingFilterType == 'workshops') {
            programQuery.trainingType = 2;
            if (req.body.startDateTime !== undefined) {
                var startDateTime = parseInt(req.body.startDateTime);
                programQuery.startDateTime = { $gte: startDateTime };
            }
            if (req.body.endDateTime !== undefined) {
                var endDateTime = parseInt(req.body.endDateTime);
                programQuery.endDateTime = { $lte: endDateTime };
            }
            // if (req.body.startDateTime !== undefined) {
            //     var dateA=new Date(req.body.startTime*1000*60*60);
            //     console.log("dateA",dateA)
            //     programQuery.filterStartTime = { $gte: moment(dateA).valueOf() };
            // }
            // if (req.body.endTime !== undefined) {
            //     var endTime = new Date(req.body.endTime * 1000).getTime();
            //     programQuery.filterEndTime = endTime;
            // }
            if (req.body.lowerPrice !== undefined && req.body.upperPrice !== undefined) {
                programQuery.price = { $gte: parseInt(req.body.lowerPrice), $lte: parseInt(req.body.upperPrice) };
            }
        } else {
            if (req.body.trainingType !== undefined) {
                var trainingType = parseInt(req.body.trainingType);
            } else {
                var trainingType = 0;
            }
            programQuery.trainingType = trainingType;
        }
        programs.find(programQuery, { title: 1, coverPic: 1, trainingType: 1 }).limit(limit).skip(page).lean().exec(function (err, program) {
            if (err) {
                callback('err', null);
            } else {
                programs.find(programQuery).lean().exec(function (err1, program1) {
                    if (err1) {
                        callback('err', null);
                    } else {
                        var responseObj = {
                            result: program,
                            total_count: program1.length
                        };
                        callback(null, responseObj);
                    }
                })
            }
        })
    }
}
exports.getPersonalizedSearchResults = getPersonalizedSearchResults;

var resendVerificationMail = function (req, res, callback) {
    user.findOne({ '_id': req.body.userId }, function (err, data) {
        if (err || data == null) {
            callback('err', null);
        } else {
            var token = data.verificationToken;
            if (data.emailVerified == false) {

                /******* mail code *******/
                var templateVariable = {
                    templateURL: "mailtemplate/registration",
                    fullName: `${data.firstName} ${data.lastName}`,
                    token: token,
                    BASE_URL: constants.BASE_URL,
                    API_VERSION: constants.API_VERSION,
                    layout: false
                };

                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: data.email,
                    subject: "Registration Successful."
                };
                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log('error', err);
                        callback('err', null);
                    } else {
                        console.log('Success sending email');
                        callback(null, 'Success');
                    }
                });
            } else {
                callback('err1', null);
            }
        }
    })
}
exports.resendVerificationMail = resendVerificationMail;

var clearAllNotifications = function (req, res, callback) {
    notification.deleteMany({ 'for': req.authenticationId }, function (err, result) {
        if (err || result == null) {
            callback("err");
        } else {
            callback(result);
        }
    })
}
exports.clearAllNotifications = clearAllNotifications;

var getMeasurements = function (req, callback) {
    user.findOne({ '_id': req.authenticationId }, 'measurements', function (err, result) {
        if (err || result == null) {
            callback('err', null);
        } else {
            callback(null, result);
        }
    })
}
exports.getMeasurements = getMeasurements;

var purchasePlan = function (req, res, callback) {
    var addInvoice = {};
    var userplan = {};
    var userSessionArr = [];
    var userMealsArr = [];
    var notiMsg = "";
    var notiTitle = "";
    var notiData = {};
    var notiTo;
    var ptdtSubscription = {};
    var planType = req.body.planType;
    var dietExpireDate = new Date(Date.now());
    dietExpireDate.setDate(dietExpireDate.getDate() + parseInt(req.body.numberOfDays));
    /************************* Get details *********************************/

    async.parallel([
        // function call to get charge bifurcation
        function (callback) {
            var ptdtData = {
                body: {
                    userId: req.body.ptdtId,
                    isMine: "noti"
                },
                authenticationId: req.authenticationId
            };
            userService.getUserInfo(ptdtData, function (ptdbAccountData) {
                getChargesBifurcation(req.body.amountPaidByUser, ptdbAccountData.commission, function (objPriceData) {
                    callback(null, objPriceData);
                });
            });
        },
        // function call to get program details
        function (callback) {
            ptdtService.getProgramDetails({ body: { programId: req.body.planId } }, function (err, programData) {
                callback(null, programData);
            });
        },
        // function call to get deit details
        function (callback) {
            ptdtService.dietPlanDetail({ body: { dietID: req.body.planId } }, function (dietData) {
                callback(null, dietData);
            });
        },
        // function call to get ptdt details
        function (callback) {
            var ptdtData = {
                body: {
                    userId: req.body.ptdtId,
                    isMine: "noti"
                },
                authenticationId: req.authenticationId
            };
            userService.getUserInfo(ptdtData, function (ptdbAccountData) {
                callback(null, ptdbAccountData);
            });
        },
        // function call to get user details
        function (callback) {
            var userData = {
                body: {
                    userId: req.authenticationId,
                    isMine: "noti"
                },
                authenticationId: req.authenticationId
            };
            userService.getUserInfo(userData, function (userAccountData) {
                callback(null, userAccountData);
            });
        }
    ], function (error, resultSet) {
        var objPriceData = resultSet[0];
        var programData = resultSet[1];
        var dietData = resultSet[2];
        var ptdbAccountData = resultSet[3];
        var userAccountData = resultSet[4];

        /************************* Code execution *********************************/
        async.waterfall([
            function (callback) {
                switch (planType) {
                    case '0'://for program                        
                        session.find({ programId: req.body.planId }).populate("programId").exec(function (err, sessionResult) {
                            if (err || session.length == 0) {
                                callback('err', null);
                            } else {
                                console.log("sessionResult=================>", sessionResult);
                                sessionResult.forEach(result => {
                                    console.log("result===============>", result);
                                    var userSessionObj = {};
                                    userSessionObj.sessionId = result._id;
                                    userSessionObj.remainingDuration = (result.programId.duration * 60000);
                                    userSessionObj.totalDuration = (result.programId.duration * 60000);
                                    userSessionObj.planId = req.body.planId;
                                    userSessionObj.userId = req.authenticationId;
                                    userSessionObj.ptdtId = req.body.ptdtId;
                                    userSessionArr.push(userSessionObj);
                                });
                                addInvoice.planType = req.body.planType;
                                addInvoice.planId = req.body.planId;
                                addInvoice.userId = req.authenticationId;
                                addInvoice.paymentType = req.body.paymentType;
                                addInvoice.orderId = req.body.orderId;

                                userplan.planType = req.body.planType;
                                userplan.planId = req.body.planId;
                                userplan.userId = req.authenticationId;
                                userplan.ptdtId = req.body.ptdtId;
                                var date = new Date(Date.now());
                                date.setHours(0, 0, 0, 0);
                                userplan.calendarEventDate = moment(date).valueOf();
                                if (req.body.paymentType == '0') {
                                    addInvoice.transactionId = "Offline";
                                } else {
                                    addInvoice.transactionId = req.body.transactionId;
                                    addInvoice.transactionCharge = objPriceData.transactionCharge;
                                    addInvoice.amountPaidByUser = objPriceData.amountPaidByUser;
                                    addInvoice.synrgyAmount = objPriceData.synrgyAmount;
                                    addInvoice.ptdtPrice = objPriceData.ptdtPrice;
                                }

                                notiTitle = "Program Purchased";
                                notiMsg = userAccountData.firstName + " " + userAccountData.lastName + " has purchased your " + programData.title + " program";
                                notiData = { id: req.body.planId, planType: 0, userType: 2 };
                                notiTo = ptdbAccountData.accounts;

                                callback(null, userSessionArr);
                            }
                        });
                        break;

                    case '1'://for class
                        addInvoice.planType = req.body.planType;
                        addInvoice.planId = req.body.planId;
                        addInvoice.userId = req.authenticationId;
                        addInvoice.paymentType = req.body.paymentType;
                        addInvoice.orderId = req.body.orderId;

                        userplan.planType = req.body.planType;
                        userplan.planId = req.body.planId;
                        userplan.userId = req.authenticationId;
                        userplan.ptdtId = req.body.ptdtId;
                        var date = new Date(parseInt(req.body.startDateTime));
                        var notiDate = new Date(parseInt(req.body.startDateTime));
                        ;
                        notiDate.setMilliseconds(notiDate.getMilliseconds() - 900000);
                        date.setHours(0, 0, 0, 0);
                        userplan.calendarEventDate = moment(date).valueOf();
                        userplan.sendNotificationTime = moment(notiDate).valueOf();

                        if (req.body.paymentType == '0') {
                            addInvoice.transactionId = "Offline";
                        } else {
                            addInvoice.transactionId = req.body.transactionId;
                            addInvoice.transactionCharge = objPriceData.transactionCharge;
                            addInvoice.amountPaidByUser = objPriceData.amountPaidByUser;
                            addInvoice.synrgyAmount = objPriceData.synrgyAmount;
                            addInvoice.ptdtPrice = objPriceData.ptdtPrice;
                        }

                        notiTitle = "Class Purchased";
                        notiMsg = userAccountData.firstName + " " + userAccountData.lastName + " has purchased your " + programData.title + " class";
                        notiData = { id: req.body.planId, planType: 1, userType: 2 };
                        notiTo = ptdbAccountData.accounts;

                        callback(null, 'success');
                        break;

                    case '2'://for workshops
                        addInvoice.planType = req.body.planType;
                        addInvoice.planId = req.body.planId;
                        addInvoice.userId = req.authenticationId;
                        addInvoice.paymentType = req.body.paymentType;
                        addInvoice.orderId = req.body.orderId;

                        userplan.planType = req.body.planType;
                        userplan.planId = req.body.planId;
                        userplan.userId = req.authenticationId;
                        userplan.ptdtId = req.body.ptdtId;
                        var date = new Date(parseInt(req.body.startDateTime));
                        var notiDate = new Date(parseInt(req.body.startDateTime));
                        ;
                        date.setHours(0, 0, 0, 0);
                        notiDate.setMilliseconds(notiDate.getMilliseconds() - 900000);
                        userplan.calendarEventDate = moment(date).valueOf();
                        userplan.sendNotificationTime = moment(notiDate).valueOf();
                        if (req.body.paymentType == '0') {
                            addInvoice.transactionId = "Offline";
                        } else {
                            addInvoice.transactionId = req.body.transactionId;
                            addInvoice.transactionCharge = objPriceData.transactionCharge;
                            addInvoice.amountPaidByUser = objPriceData.amountPaidByUser;
                            addInvoice.synrgyAmount = objPriceData.synrgyAmount;
                            addInvoice.ptdtPrice = objPriceData.ptdtPrice;
                        }


                        notiTitle = "Workshop Purchased";
                        notiMsg = userAccountData.firstName + " " + userAccountData.lastName + " has purchased your " + programData.title + " workshop";
                        notiData = { id: req.body.planId, planType: 2, userType: 2 };
                        notiTo = ptdbAccountData.accounts;

                        callback(null, 'success');
                        break;

                    case '3'://for customize
                        programs.findOne({ _id: req.body.planId }, function (err, result) {
                            if (err || result == null) {
                                callback('err', null);
                            } else {
                                if (result.userAssigned != undefined) {
                                    if (result.userAssigned.equals(req.authenticationId)) {
                                        session.find({ programId: req.body.planId }).populate('programId').exec(function (err, sessionResult) {
                                            if (err || sessionResult.length == 0) {
                                                callback('err', null)
                                            } else {
                                                sessionResult.forEach(result => {
                                                    var userSessionObj = {};
                                                    userSessionObj.sessionId = result._id;
                                                    userSessionObj.remainingDuration = (result.programId.duration * 60000);
                                                    userSessionObj.totalDuration = (result.programId.duration * 60000);
                                                    userSessionObj.planId = req.body.planId;
                                                    userSessionObj.userId = req.authenticationId;
                                                    userSessionObj.ptdtId = req.body.ptdtId;
                                                    userSessionArr.push(userSessionObj);
                                                });
                                            }
                                        });
                                        addInvoice.planType = req.body.planType;
                                        addInvoice.planId = req.body.planId;
                                        addInvoice.userId = req.authenticationId;
                                        addInvoice.paymentType = req.body.paymentType;
                                        addInvoice.orderId = req.body.orderId;

                                        userplan.planType = req.body.planType;
                                        userplan.planId = req.body.planId;
                                        userplan.userId = req.authenticationId;
                                        userplan.ptdtId = req.body.ptdtId;
                                        var date = new Date(Date.now());
                                        date.setHours(0, 0, 0, 0);
                                        userplan.calendarEventDate = moment(date).valueOf();

                                        if (req.body.paymentType == 0) {
                                            addInvoice.transactionId = "Offline";
                                        } else {
                                            addInvoice.transactionId = req.body.transactionId;
                                            addInvoice.transactionCharge = objPriceData.transactionCharge;
                                            addInvoice.amountPaidByUser = objPriceData.amountPaidByUser;
                                            addInvoice.synrgyAmount = objPriceData.synrgyAmount;
                                            addInvoice.ptdtPrice = objPriceData.ptdtPrice;
                                        }

                                        notiTitle = "Program Purchased";
                                        notiMsg = userAccountData.firstName + " " + userAccountData.lastName + " has purchased your " + programData.title + " program assignd by you";
                                        notiData = { id: req.body.planId, planType: 3, userType: 2 };
                                        notiTo = ptdbAccountData.accounts;

                                        callback(null, userSessionArr);
                                    } else {
                                        callback('err1', null);
                                    }
                                } else {
                                    callback('err', null);
                                }
                            }
                        })
                        break;

                    case '4': // for Regular dietplan 
                        if (req.body.paymentType == '1') {
                            meals.find({ dietPlanID: req.body.planId }, function (err, mealsResult) {
                                if (err || mealsResult.length == 0) {
                                    callback('err', null);
                                } else {

                                    mealsResult.forEach(function (result) {
                                        var userMealsObj = {}
                                        var CurrentDate = new Date(Date.now())
                                        CurrentDate.setDate(CurrentDate.getDate() + parseInt(result.numberOfDay));
                                        userMealsObj.dietPlanId = req.body.planId;
                                        userMealsObj.userId = req.authenticationId;
                                        userMealsObj.mealId = result._id;
                                        userMealsObj.planDate = moment(CurrentDate).valueOf();
                                        userMealsObj.ptdtId = req.body.ptdtId;

                                        userMealsArr.push(userMealsObj);
                                    })
                                    addInvoice.planType = req.body.planType;
                                    addInvoice.dietPlanId = req.body.planId;
                                    addInvoice.userId = req.authenticationId;
                                    addInvoice.transactionId = req.body.transactionId;
                                    addInvoice.paymentType = req.body.paymentType;
                                    addInvoice.transactionCharge = objPriceData.transactionCharge;
                                    addInvoice.amountPaidByUser = objPriceData.amountPaidByUser;
                                    addInvoice.synrgyAmount = objPriceData.synrgyAmount;
                                    addInvoice.ptdtPrice = objPriceData.ptdtPrice;
                                    addInvoice.orderId = req.body.orderId;

                                    userplan.planType = req.body.planType;
                                    userplan.dietPlanId = req.body.planId;
                                    userplan.userId = req.authenticationId;
                                    userplan.ptdtId = req.body.ptdtId;
                                    userplan.dietExpiredDate = moment(dietExpireDate).valueOf();
                                    var date = new Date(Date.now());
                                    date.setHours(0, 0, 0, 0);
                                    userplan.calendarEventDate = moment(date).valueOf();

                                    notiTitle = "Diet Plan Purchased";
                                    notiMsg = userAccountData.firstName + " " + userAccountData.lastName + " has purchased your " + dietData.title + " diet plan";
                                    notiData = { id: req.body.planId, planType: 4, userType: 1 };
                                    notiTo = ptdbAccountData.accounts;

                                    callback(null, userMealsArr);
                                }
                            })
                        }
                        break;
                    case '5': // for Customize dietplan 
                        if (req.body.paymentType == '1') {
                            diets.findOne({ _id: req.body.planId }, function (err, result) {
                                console.log("5====================>", err);
                                console.log("5====================>", result);
                                if (err || result == null) {
                                    callback('err', null);
                                } else {
                                    if (result.userAssigned != undefined) {
                                        console.log("5====================>1");
                                        if (result.userAssigned.equals(req.authenticationId)) {
                                            console.log("5====================>2");
                                            meals.find({ dietPlanID: req.body.planId }, function (err, mealsResult) {
                                                console.log("5====================>3", err);
                                                console.log("5====================>4", result);
                                                if (err || mealsResult.length == 0) {
                                                    callback('err', null)
                                                } else {
                                                    mealsResult.forEach(function (result) {
                                                        var userMealsObj = {}
                                                        var CurrentDate = new Date(Date.now())
                                                        CurrentDate.setDate(CurrentDate.getDate() + parseInt(result.numberOfDay));
                                                        userMealsObj.dietPlanId = req.body.planId;
                                                        userMealsObj.userId = req.authenticationId;
                                                        userMealsObj.mealId = result._id;
                                                        userMealsObj.planDate = moment(CurrentDate).valueOf();
                                                        userMealsObj.ptdtId = req.body.ptdtId;
                                                        userMealsArr.push(userMealsObj);
                                                    })
                                                }
                                            })
                                            addInvoice.planType = req.body.planType;
                                            addInvoice.dietPlanId = req.body.planId;
                                            addInvoice.userId = req.authenticationId;
                                            addInvoice.transactionId = req.body.transactionId;
                                            addInvoice.paymentType = req.body.paymentType;
                                            addInvoice.transactionCharge = objPriceData.transactionCharge;
                                            addInvoice.amountPaidByUser = objPriceData.amountPaidByUser;
                                            addInvoice.synrgyAmount = objPriceData.synrgyAmount;
                                            addInvoice.ptdtPrice = objPriceData.ptdtPrice;
                                            addInvoice.orderId = req.body.orderId;

                                            userplan.planType = req.body.planType;
                                            userplan.dietPlanId = req.body.planId;
                                            userplan.userId = req.authenticationId;
                                            userplan.ptdtId = req.body.ptdtId;
                                            userplan.dietExpiredDate = moment(dietExpireDate).valueOf();
                                            var date = new Date(Date.now());
                                            date.setHours(0, 0, 0, 0);
                                            userplan.calendarEventDate = moment(date).valueOf();

                                            notiTitle = "Diet Plan Purchased";
                                            notiMsg = userAccountData.firstName + " " + userAccountData.lastName + " has purchased your " + dietData.title + " diet plan assignd by you";
                                            notiData = { id: req.body.planId, planType: 5, userType: 1 };
                                            notiTo = ptdbAccountData.accounts;

                                            callback(null, userMealsArr);
                                        } else {
                                            callback('err', null)
                                        }
                                    } else {
                                        callback('err1', null)
                                    }
                                }
                            })
                        }
                        break;

                    case '6':// for subscription dietitians 
                        var date = new Date(Date.now());
                        date.setDate(date.getDate() + parseInt(req.body.duration));
                        addInvoice.planType = req.body.planType;
                        addInvoice.userId = req.authenticationId;
                        addInvoice.paymentType = req.body.paymentType;
                        addInvoice.subscriptionPlanId = req.body.planId;
                        addInvoice.transactionId = req.body.transactionId;
                        addInvoice.transactionCharge = objPriceData.transactionCharge;
                        addInvoice.amountPaidByUser = objPriceData.amountPaidByUser;
                        addInvoice.synrgyAmount = objPriceData.synrgyAmount;
                        addInvoice.ptdtPrice = objPriceData.ptdtPrice;
                        addInvoice.orderId = req.body.orderId;

                        ptdtSubscription.ptdtId = req.authenticationId;
                        ptdtSubscription.subscriptionPlanId = req.body.planId;
                        ptdtSubscription.renewalDate = moment(date).valueOf();
                        ptdtSubscription.subscriptionDays = req.body.duration;
                        ptdtSubscription.userType = 1;
                        ptdtSubscription.sendNotificationTime = moment(date).valueOf() - 259200000;
                        callback(null, 'success');
                        break;

                    case '7':// for subscription pt
                        var date = new Date(Date.now());
                        date.setDate(date.getDate() + parseInt(req.body.duration));
                        addInvoice.planType = req.body.planType;
                        addInvoice.userId = req.authenticationId;
                        addInvoice.paymentType = req.body.paymentType;
                        addInvoice.subscriptionPlanId = req.body.planId;
                        addInvoice.transactionId = req.body.transactionId;
                        addInvoice.transactionCharge = objPriceData.transactionCharge;
                        addInvoice.amountPaidByUser = objPriceData.amountPaidByUser;
                        addInvoice.synrgyAmount = objPriceData.synrgyAmount;
                        addInvoice.ptdtPrice = objPriceData.ptdtPrice;
                        addInvoice.orderId = req.body.orderId;

                        ptdtSubscription.ptdtId = req.authenticationId;
                        ptdtSubscription.subscriptionPlanId = req.body.planId;
                        ptdtSubscription.renewalDate = moment(date).valueOf();
                        ptdtSubscription.subscriptionDays = req.body.duration;
                        ptdtSubscription.userType = 2;
                        ptdtSubscription.sendNotificationTime = moment(date).valueOf() - 259200000;
                        callback(null, 'success');
                        break;

                    default:
                        callback('err', null);
                }
            }
        ], function (error, data) {
            console.log("error");
            console.log(error);
            if (error == 'err') {
                callback('err')
            } else if (error == 'err1') {
                callback('err1')
            } else {
                invoice(addInvoice).save(function (err, invoice) {
                    if (err || invoice == null) {
                        callback('err');
                    } else {
                        var planTypeVar = parseInt(planType);
                        if (planTypeVar <= 5) {
                            console.log("here==================>");
                            userPlan(userplan).save(function (err1, userPlan) {
                                console.log("err1==================>", err1);
                                console.log("userPlan==================>", userPlan);
                                if (err1 || userPlan == null) {
                                    callback('err');
                                } else {
                                    if (planTypeVar < 4) {
                                        if (planTypeVar == 3 || planTypeVar == 0) {
                                            console.log("data==================>", data);
                                            console.log("userPlan==================>", data);
                                            data.forEach(function (result) {
                                                result.userPlanId = userPlan._id;
                                            })
                                            console.log("userSession==================>", data);
                                            userSession.insertMany(data, function (err2, result) {
                                                if (err2 || result == null) {
                                                    console.log(err2);
                                                    callback('err');
                                                } else {
                                                    programs.populate(userPlan, { path: 'planId' }, function (err3, result) {
                                                        if (err3 || result.length == 0) {
                                                            console.log(err3);
                                                            callback('err');
                                                        } else {
                                                            var objNotification = {
                                                                for: ptdbAccountData._id,
                                                                programId: notiData.id,
                                                                notification: notiMsg,
                                                                type: notiTitle,
                                                                planType: notiData.planType
                                                            };
                                                            saveNotification(objNotification, function (err4, data1) {
                                                                if (err4) {
                                                                    callback('err');
                                                                } else {
                                                                    noti.sendNotifications(notiTo, notiTitle, notiMsg, notiData, function (err5) {
                                                                        if (err5 == 'err') {
                                                                            callback('err');
                                                                        } else {
                                                                            callback(result);
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        } else {
                                            programs.populate(userPlan, { path: 'planId' }, function (err2, result) {
                                                if (err2 || result.length == 0) {
                                                    console.log(err2);
                                                    callback('err');
                                                } else {
                                                    // callback(result);
                                                    var objNotification = {
                                                        for: ptdbAccountData._id,
                                                        programId: notiData.id,
                                                        notification: notiMsg,
                                                        type: notiTitle,
                                                        planType: notiData.planType
                                                    };
                                                    saveNotification(objNotification, function (err3) {
                                                        if (err3) {
                                                            callback('err');
                                                        } else {
                                                            noti.sendNotifications(notiTo, notiTitle, notiMsg, notiData, function (err4) {
                                                                if (err4 == 'err') {
                                                                    callback('err');
                                                                } else {
                                                                    callback(result);
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    } else {
                                        data.forEach(function (result) {
                                            result.userPlanId = userPlan._id;
                                        })
                                        userMeals.insertMany(userMealsArr, function (err, data) {
                                            if (err) {
                                                callback('err', null);
                                            } else {
                                                console.log("userPlan", userPlan)
                                                diets.populate(userPlan, { path: 'dietPlanId' }, function (err2, result) {
                                                    if (err2 || result.length == 0) {
                                                        callback('err');
                                                    } else {
                                                        var objNotification = {
                                                            for: ptdbAccountData._id,
                                                            dietProgramId: notiData.id,
                                                            notification: notiMsg,
                                                            type: notiTitle,
                                                            planType: notiData.planType
                                                        };
                                                        saveNotification(objNotification, function (err3, data1) {
                                                            if (err3) {
                                                                callback('err');
                                                            } else {
                                                                noti.sendNotifications(notiTo, notiTitle, notiMsg, notiData, function (err4) {
                                                                    if (err4 == 'err') {
                                                                        callback('err');
                                                                    } else {
                                                                        callback(result);
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                }
                            })
                        } else {
                            ptdtSubscriptionPlan.findOne({ ptdtId: req.authenticationId, isActive: true }, function (err, ptdtResult) {
                                if (err) {
                                    callback('err');
                                } else {
                                    if (ptdtResult) {
                                        ptdtSubscriptionPlan.findOneAndUpdate({ ptdtId: req.authenticationId, isActive: true }, { $set: { isActive: false, renewalDate: _.now() } }, { new: true }, function (err2, ptdtUpdateResult) {
                                            if (err2 || ptdtUpdateResult == null) {
                                                callback('err');
                                            } else {
                                                ptdtSubscriptionPlan(ptdtSubscription).save(function (err1, ptdtPlan) {
                                                    if (err1 || ptdtPlan == null) {
                                                        callback('err');
                                                    } else {
                                                        ptdtSubscriptionPlan.populate(ptdtPlan, { path: 'subscriptionPlanId', select: 'title description duration amount' }, function (err2, result) {
                                                            if (err2 || result == null) {
                                                                callback('err');
                                                            } else {
                                                                ptdtService.generatePtdtInvoice(req, res, invoice._id, function (data) {
                                                                    callback(result);
                                                                });
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    } else {
                                        ptdtSubscriptionPlan(ptdtSubscription).save(function (err1, ptdtPlan) {
                                            if (err1 || ptdtPlan == null) {
                                                callback('err');
                                            } else {
                                                ptdtSubscriptionPlan.populate(ptdtPlan, { path: 'subscriptionPlanId', select: 'title description duration amount' }, function (err2, result) {
                                                    if (err2 || result == null) {
                                                        callback('err');
                                                    } else {
                                                        ptdtService.generatePtdtInvoice(req, res, invoice._id, function (data) {
                                                            callback(result);
                                                        });
                                                    }
                                                })
                                            }
                                        })
                                    }
                                }
                            })
                        }
                    }
                })
            }
        });

        /************************ Code execution **********************************/
    });




    /************************* Get details *********************************/
};
exports.purchasePlan = purchasePlan;

var addConversation = function (req, res, callback) {

    var reqBody = req.body;
    var newConversation = {
        conversationId: reqBody.conversationId,
        userId: reqBody.userId,
        chatWith: reqBody.chatWith,
        updatedDate: _.now()
    };

    conversation(newConversation).save(function (err, result) {
        if (err) {
            console.log(err);
            callback(req, res, "err");
        } else {
            callback(req, res, result);
        }
    });

};
exports.addConversation = addConversation;

var getConversation = function (req, res, callback) {
    var reqBody = req.body;
    conversation.findOne({ $or: [{ userId: reqBody.userId, chatWith: reqBody.chatWith }, { userId: reqBody.chatWith, chatWith: reqBody.userId }] }).lean().exec(function (err, convo) {
        if (err) {
            callback(req, res, "err");
        } else {
            callback(req, res, convo);
        }
    });

};
exports.getConversation = getConversation;

var getCalendarEvents = function (req, callback) {
    if (parseInt(req.body.purchased)) {
        userPlan.aggregate([
            { $match: { 'userId': req.authenticationId } },
            { $match: { 'isExpired': false } },
            { $match: { 'calendarEventDate': { $gte: parseInt(req.body.startDateTime), $lte: parseInt(req.body.endDateTime) } } },
            { $lookup: { from: "program", localField: "planId", foreignField: "_id", as: "program" } },
            { $lookup: { from: "dietprograms", localField: "dietPlanId", foreignField: "_id", as: "diet" } },
            { $project: { 'program._id': 1, 'program.title': 1, 'program.description': 1, 'calendarEventDate': 1, 'program.trainingType': 1, 'program.startDateTime': 1, 'diet._id': 1, 'diet.title': 1, 'diet.dietType': 1 } },
            { $unwind: { path: '$program', "preserveNullAndEmptyArrays": true } },
            { $unwind: { path: '$diet', "preserveNullAndEmptyArrays": true } },
            { "$group": { "_id": "$calendarEventDate", "program": { "$addToSet": "$program" }, "diet": { "$addToSet": "$diet" } } },
            { "$project": { "_id": "$_id", "data": { "$concatArrays": ["$program", "$diet"] }, "_id": 1, } },
        ], function (err, result) {
            if (err) {
                console.log('error.................', err);
                callback('err', null);
            } else {
                console.log(result);
                callback(null, result);
            }
        })
    } else {
        programs.aggregate([
            { $match: { 'createdBy': req.authenticationId } },
            { $match: { $or: [{ trainingType: 1 }, { trainingType: 2 }] } },
            { $match: { $and: [{ 'isDeleted': false }, { isCompleted: false }] } },
            { $match: { 'calendarEventDate': { $gte: parseInt(req.body.startDateTime), $lte: parseInt(req.body.endDateTime) } } },
            { $project: { 'title': 1, 'description': 1, 'startDateTime': 1, 'trainingType': 1, 'calendarEventDate': 1 } },
            { $group: { _id: "$calendarEventDate", data: { $push: '$$ROOT' } } },
        ], function (err, result) {
            if (err) {
                console.log('error.................', err);
                callback('err', null);
            } else {
                console.log(result);
                callback(null, result);
            }
        })
    }
}
exports.getCalendarEvents = getCalendarEvents;

var addFeed = function (req, callback) {

    var reqBody = req.body;
    var userId = reqBody.userId;
    var followers = [];

    follow.distinct('followedBy', { followedTo: userId, isDeleted: false }).lean().exec(function (err, followData) {
        if (err) {
            callback("err", null);
        } else {
            if (followData.length > 0) {
                followers = followData;
            } else {
                followers = [];
            }

            var newFeed = {
                userId: userId,
                followers: followers,
                caption: reqBody.caption,
                media: reqBody.media,
                type: reqBody.type,
                createdAt: _.now(),
                updatedAt: _.now()
            };

            feeds(newFeed).save(function (err, result) {
                if (err) {
                    console.log(err);
                    callback("err", null);
                } else {
                    callback(null, result);
                }
            });
        }
    });
};
exports.addFeed = addFeed;

var likePost = function (req, callback) {
    feeds.findOne({ '_id': req.body.postId }, function (err, result) {
        if (err) {
            callback("err", null);
        }
        else {
            if (!result) {
                callback('err2', null);
            }
            else {
                chkFollowStatus(req, req.body.createdBy, function (error, flag) {
                    if (error) {
                        callback('err', null);
                    } else {
                        if (flag) {
                            var likeObj = req.body.userId;
                            var likeStatus = parseInt(req.body.likeStatus);
                            var likeLength = 0;
                            if (likeStatus) {
                                feeds.findOne({ '_id': req.body.postId, "likes": req.body.userId }, function (err, result) {
                                    if (err) {
                                        callback("err", null);
                                    }
                                    else {
                                        if (!result) {
                                            feeds.findOneAndUpdate({ '_id': req.body.postId }, { $push: { likes: likeObj } }, { new: true }, function (err, result) {
                                                if (err) {
                                                    callback('err', null);
                                                } else {
                                                    likeLength = result.likes.length;
                                                    if (!result.userId.equals(req.authenticationId)) {
                                                        var userData = {
                                                            body: {
                                                                userId: result.userId,
                                                                isMine: 'noti'
                                                            },
                                                            authenticationId: req.authenticationId
                                                        };
                                                        userService.getUserInfo(userData, function (user) {
                                                            if (user == 'err') {
                                                                callback('err', null);
                                                            } else {
                                                                var objNotification = {
                                                                    for: result.userId,
                                                                    to: req.body.userId,
                                                                    feedId: result._id,
                                                                    notification: req.body.firstName + " " + req.body.lastName + " has liked your post",
                                                                    type: 'like',
                                                                    planType: 9
                                                                };
                                                                saveNotification(objNotification, function (err1, data) {
                                                                    if (err1 || data == null) {
                                                                        callback('err', null);
                                                                    } else {
                                                                        noti.sendNotifications(user.accounts, "Liked your Post", req.body.firstName + " " + req.body.lastName + " has liked your post", { "id": result._id, planType: 9, userType: req.body.userType }, function (notification) {
                                                                            if (notification == 'err') {
                                                                                callback('err', null);
                                                                            } else {
                                                                                callback(null, { likeCount: likeLength, arrayIndex: req.body.arrayIndex });
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    } else {
                                                        callback(null, { likeCount: likeLength, arrayIndex: req.body.arrayIndex });
                                                    }

                                                }
                                            });
                                        }
                                        else {
                                            console.log("likeLength", likeLength);
                                            callback(null, { likeCount: result.likes.length, arrayIndex: req.body.arrayIndex });
                                        }
                                    }

                                })

                            } else {
                                feeds.findOneAndUpdate({ '_id': req.body.postId }, { $pull: { likes: req.body.userId } }, { new: true }, function (err, result) {
                                    if (err) {
                                        callback('err', null);
                                    } else {
                                        var likeLength = result.likes.length;
                                        callback(null, { likeCount: likeLength, arrayIndex: req.body.arrayIndex });
                                    }
                                });
                            }
                        } else {
                            callback('err1', null);
                        }
                    }
                })
            }
        }
    })

}
exports.likePost = likePost;

var addComment = function (req, callback) {
    feeds.findOne({ '_id': req.body.postId }, function (err, result) {
        if (err) {
            callback("err", null);
        }
        else {
            if (!result) {
                callback("err2", null);
            } else {
                chkFollowStatus(req, req.body.createdBy, function (error, flag) {
                    if (error) {
                        callback('err', null);
                    } else {
                        if (flag) {
                            var parentId = (req.body.parentId != undefined) ? req.body.parentId : null;
                            var commentObj = {
                                postId: req.body.postId,
                                type: req.body.type, //0 - parent, 1 - reply
                                userId: req.body.userId,
                                commentString: req.body.commentString,
                                createdAt: _.now(),
                                updatedAt: _.now()
                            };
                            comments(commentObj).save(function (err, result) {
                                if (err) {
                                    callback("err", null);
                                } else {
                                    if (req.body.type == 1) {
                                        comments.findOneAndUpdate({ '_id': parentId }, { $push: { replies: result._id } }, { new: true }).populate({
                                            path: 'replies',
                                            model: 'feeds_comments',
                                            populate: {
                                                path: 'userId',
                                                model: 'user',
                                                select: '_id firstName lastName profilePic'
                                            }
                                        }).populate('userId', '_id firstName lastName profilePic').lean().exec(function (err1, result12) {

                                            if (err1) {
                                                callback('err', null);
                                            } else {
                                                commentObj.commentId = result._id;
                                                addCommentInFeed(req, result, commentObj, function (err, data) {
                                                    if (err) {
                                                        callback('err', null);
                                                    } else {
                                                        callback(null, result12);
                                                    }
                                                });
                                            }
                                        });
                                    } else {
                                        commentObj.commentId = result._id;
                                        addCommentInFeed(req, result, commentObj, function (err, data) {
                                            if (err) {
                                                callback('err', null);
                                            } else {
                                                user.populate(data, { path: "userId", select: '_id firstName lastName profilePic' }, function (err, result) {
                                                    if (err) {
                                                        callback('err', null);
                                                    } else {
                                                        callback(null, data);
                                                    }
                                                })

                                            }
                                        });
                                    }
                                }
                            });
                        } else {
                            callback('err1', null);
                        }
                    }
                })
            }
        }
    })

};
exports.addComment = addComment;

var addCommentInFeed = function (req, result, commentObj, callback) {
    if (req.body.type != 1) {
        var setObj = { $set: { latestComment: commentObj }, $inc: { commentsCount: 1 } };
    } else {
        var setObj = { $inc: { commentsCount: 1 } };
    }

    feeds.findOneAndUpdate({ '_id': req.body.postId }, setObj, { new: true }).populate('latestComment.postId'
    ).lean().exec(function (err1, result1) {

        if (err1) {
            callback('err', null);
        } else {
            if (!result1.latestComment.postId.userId.equals(req.body.userId)) {
                if (!result1.latestComment.postId.userId.equals(req.body.parentCommentUserId)) {
                    var userData = {
                        body: {
                            userId: result1.latestComment.postId.userId,
                            isMine: 'noti'
                        },
                        authenticationId: req.authenticationId
                    };
                    userService.getUserInfo(userData, function (user) {
                        if (user == 'err') {
                            callback('err', null);
                        } else {
                            var objNotification = {
                                for: result1.latestComment.postId.userId,
                                to: req.body.userId,
                                feedId: result1._id,
                                notification: req.body.firstName + " " + req.body.lastName + " has commented on your post",
                                type: 'comment',
                                planType: 10
                            };
                            saveNotification(objNotification, function (err1, data) {
                                if (err1 || data == null) {
                                    callback('err', null);
                                } else {
                                    noti.sendNotifications(user.accounts, "Commented your Post", req.body.firstName + " " + req.body.lastName + " has commented on your post", { "id": result1._id, planType: 10, userType: req.body.userType }, function (notification) {
                                        if (notification == 'err') {
                                            callback('err', null);
                                        } else {
                                            if (req.body.type == 1) {
                                                var userData = {
                                                    body: {
                                                        userId: req.body.parentCommentUserId,
                                                        isMine: 'noti'
                                                    },
                                                    authenticationId: req.authenticationId
                                                };
                                                userService.getUserInfo(userData, function (user) {
                                                    if (user == 'err') {
                                                        callback('err', null);
                                                    } else {
                                                        var objNotification = {
                                                            for: req.body.parentCommentUserId,
                                                            feedId: result1._id,
                                                            notification: req.body.firstName + " " + req.body.lastName + " has replied on your comment",
                                                            type: 'comment',
                                                            planType: 10
                                                        };
                                                        saveNotification(objNotification, function (err1, data) {
                                                            if (err1 || data == null) {
                                                                callback('err', null);
                                                            } else {
                                                                noti.sendNotifications(user.accounts, "Replied on your comment", req.body.firstName + " " + req.body.lastName + " has replied on your comment", { "id": result1._id, planType: 10, userType: req.body.userType }, function (notification) {
                                                                    if (notification == 'err') {
                                                                        callback('err', null);
                                                                    } else {
                                                                        callback(null, result);
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            } else {
                                                callback(null, result);
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    if (req.body.type == 1) {
                        var userData = {
                            body: {
                                userId: req.body.parentCommentUserId,
                                isMine: 'noti'
                            },
                            authenticationId: req.authenticationId
                        };
                        userService.getUserInfo(userData, function (user) {
                            if (user == 'err') {
                                callback('err', null);
                            } else {
                                var objNotification = {
                                    for: req.body.parentCommentUserId,
                                    feedId: result1._id,
                                    notification: req.body.firstName + " " + req.body.lastName + " has replied on your comment",
                                    type: 'comment',
                                    planType: 10
                                };
                                saveNotification(objNotification, function (err1, data) {
                                    if (err1 || data == null) {
                                        callback('err', null);
                                    } else {
                                        noti.sendNotifications(user.accounts, "Replied on your comment", req.body.firstName + " " + req.body.lastName + " has replied on your comment", { "id": result1._id, planType: 10, userType: req.body.userType }, function (notification) {
                                            if (notification == 'err') {
                                                callback('err', null);
                                            } else {
                                                callback(null, result);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                    // callback(null, result);
                }
            }
            else {
                if (req.body.type == 1 && req.body.userId != req.body.parentCommentUserId) {
                    var userData = {
                        body: {
                            userId: req.body.parentCommentUserId,
                            isMine: 'noti'
                        },
                        authenticationId: req.authenticationId
                    };
                    userService.getUserInfo(userData, function (user) {
                        if (user == 'err') {
                            callback('err', null);
                        } else {
                            var objNotification = {
                                for: req.body.parentCommentUserId,
                                feedId: result1._id,
                                notification: req.body.firstName + " " + req.body.lastName + " has replied on your comment",
                                type: 'comment',
                                planType: 10
                            };
                            saveNotification(objNotification, function (err1, data) {
                                if (err1 || data == null) {
                                    callback('err', null);
                                } else {
                                    noti.sendNotifications(user.accounts, "Replied on your comment", req.body.firstName + " " + req.body.lastName + " has replied on your comment", { "id": result1._id, planType: 10, userType: req.body.userType }, function (notification) {
                                        if (notification == 'err') {
                                            callback('err', null);
                                        } else {
                                            callback(null, result);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
                else {
                    callback(null, result);
                }
            }
        }
    });
};
exports.addCommentInFeed = addCommentInFeed;

var editFeed = function (req, callback) {

    var reqBody = req.body;
    var setObj = {
        // caption: reqBody.caption,
        // media: reqBody.media,
        type: req.body.type,
        updatedAt: _.now()
    };
    if (reqBody.caption != undefined) {
        setObj.caption = reqBody.caption;
    }
    if (reqBody.media != undefined) {
        setObj.media = reqBody.media;
    }
    feeds.findOne({ '_id': req.body.postId }, function (err, result) {
        if (err) {
            callback('err', null);
        } else {
            if (!result) {
                callback('err1', null);
            } else {
                feeds.findOneAndUpdate({ '_id': req.body.postId }, setObj, { new: true }, function (err, result) {
                    if (err) {
                        callback('err', null);
                    } else {
                        firebaseFunction.updateFeedData(result._id, function (data) {
                            callback(null, result);
                        });
                    }
                });
            }
        }
    })
};
exports.editFeed = editFeed;

var deleteFeed = function (req, callback) {
    feeds.findOne({ '_id': req.body.postId }, function (err, result) {
        if (err) {
            callback("err", null);
        }
        else {
            if (!result) {
                callback("err1", null);
            }
            else {
                var postId = req.body.postId;
                feeds.deleteOne({ _id: postId }).exec(function (err, result) {
                    if (err) {
                        callback('err', null);
                    } else {
                        comments.deleteMany({ postId: postId }).exec(function (err, result) {
                            if (err) {
                                callback('err', null);
                            } else {
                                callback(null, 'success');
                            }
                        });
                    }
                });
            }
        }
    })
};
exports.deleteFeed = deleteFeed;

var getUserFeeds = function (req, callback) {
    chkFollowStatus(req, req.body.userId, function (error, flag) {
        if (error) {
            callback('err', null);
        } else {
            if (flag) {
                var userId = req.body.userId;
                var page = parseInt(req.body.skip * req.body.limit);
                var limit = parseInt(req.body.limit);
                var finalData = {};
                feeds.find({ userId: userId }).limit(limit).skip(page).lean().populate('userId', '_id firstName lastName profilePic type').populate('latestComment.userId', '_id firstName lastName profilePic').sort({ createdAt: 'desc' }).exec(function (err, result) {
                    if (err) {
                        callback('err', null);
                    } else {
                        result.forEach(function (element) {
                            var like = _.find(element.likes, req.authenticationId);
                            if (like !== undefined) {
                                element.isLiked = true;
                            } else {
                                element.isLiked = false;
                            }
                            element.likeCount = element.likes.length;
                        })
                        feeds.find({ userId: userId }).lean().exec(function (err1, totalRecords) {
                            if (err1) {
                                callback('err', null);
                            } else {
                                finalData.result = result;
                                finalData.total_count = totalRecords.length;
                                callback(null, finalData);
                            }
                        });
                    }
                });
            } else {
                callback('err1', null);
            }
        }
    })
};
exports.getUserFeeds = getUserFeeds;

var getFeedDetails = function (req, callback) {
    var id = req.body.postId;
    feeds.findById(id).lean().populate('userId', '_id firstName lastName profilePic type').populate('latestComment.userId', '_id firstName lastName profilePic').exec(function (err, result) {
        if (err) {
            callback('err', null);
        } else {
            if (result) {
                var flag = _.find(result.likes, req.authenticationId);
                if (flag !== undefined) {
                    result.isLiked = true;
                    result.likeCount = result.likes.length;
                    callback(null, result);
                } else {
                    result.isLiked = false;
                    result.likeCount = result.likes.length;
                    callback(null, result);
                }
            } else {
                callback(null, "");
            }
        }
    });
};
exports.getFeedDetails = getFeedDetails;

var getFeedComments = function (req, callback) {

    var postId = req.body.postId;
    var page = parseInt(req.body.skip * req.body.limit);
    var limit = parseInt(req.body.limit);
    var finalData = {};

    comments.find({ postId: postId, type: 0 }).limit(limit).skip(page).lean().populate({
        path: 'replies',
        model: 'feeds_comments',
        populate: {
            path: 'userId',
            model: 'user',
            select: '_id firstName lastName profilePic'
        }
    }).populate('userId', '_id firstName lastName profilePic').sort({ updatedAt: -1 }).exec(function (err, result) {
        if (err || result == null) {
            callback('err', null);
        } else {
            comments.find({ postId: postId, type: 0 }).lean().exec(function (err1, totalRecords) {
                if (err1) {
                    callback('err', null);
                } else {
                    finalData.result = result;
                    finalData.total_count = totalRecords.length;
                    callback(null, finalData);
                }
            });
        }
    });
};
exports.getFeedComments = getFeedComments;

var getFeeds = function (req, callback) {
    var userId = req.body.userId;
    var page = parseInt(req.body.skip * req.body.limit);
    var limit = parseInt(req.body.limit);
    var finalData = {};
    feeds.find({ $and: [{ $or: [{ followers: { $in: userId } }, { userId: userId }] }] }).limit(limit).skip(page).lean().populate('userId', '_id firstName lastName profilePic type').populate('latestComment.userId', '_id firstName lastName profilePic').sort({ createdAt: 'desc' }).exec(function (err, result) {
        if (err) {
            callback('err', null);
        } else {
            result.forEach(function (element) {
                var like = _.find(element.likes, req.authenticationId);
                if (like !== undefined) {
                    element.isLiked = true;
                } else {
                    element.isLiked = false;
                }
                element.likeCount = element.likes.length;
            })
            feeds.find({ $and: [{ $or: [{ followers: { $in: userId } }, { userId: userId }] }] }).lean().exec(function (err, totalRecords) {
                if (err) {
                    callback('err', null);
                } else {
                    finalData.result = result;
                    finalData.total_count = totalRecords.length;
                    callback(null, finalData);
                }
            });

        }
    });
};
exports.getFeeds = getFeeds;

var chkFollowStatus = function (req, createdBy, callback) {
    if (req.authenticationId.equals(createdBy)) {
        callback(null, true);
    } else {
        follow.distinct('followedBy', { 'followedTo': createdBy, 'isDeleted': false, 'isBlocked': false, 'isFreezed': false }).lean().exec(function (err, result) {
            if (err) {
                callback('err', null);
            } else {
                var flag = _.find(result, req.authenticationId);
                if (flag !== undefined) {
                    callback(null, true);
                } else {
                    callback(null, false);
                }
            }
        })
    }
}
exports.chkFollowStatus = chkFollowStatus;

var getFollowersFollowings = function (req, callback) {
    follow.distinct(
        "followedBy", { followedTo: req.authenticationId }).lean().exec(function (err1, filteredResult) {
            if (err1) {
                callback("err", null);
            } else {
                follow.distinct(
                    "followedTo", { followedBy: req.authenticationId }).lean().exec(function (err2, followedResult) {
                        if (err2) {
                            callback("err", null);
                        } else {
                            console.log("filteredResult",filteredResult);
                            console.log("followedResult",followedResult);
                            var dif = _.union(filteredResult, followedResult);
                            console.log("dif",dif);
                            arr1 = [];
                            dif.forEach(function (result) {
                                var id = { userData: result }
                                arr1.push(id)
                            })
                            var arr = _.uniqWith(arr1, _.isEqual);
                            user.populate(arr, { path: "userData", select: '_id firstName lastName profilePic type ', options: { lean: true } }, function (err, result1) {
                                if (err) {
                                    callback("err", null);
                                } else {
                                    console.log("result1=============>", result1);
                                    conversation.find({ $and: [{ $or: [{ userId: req.authenticationId }, { chatWith: req.authenticationId }] }] }, function (err3, result2) {
                                        if (err3) {
                                            callback("err", null);
                                        } else {
                                            result2.forEach(function (data1) {
                                                result1.forEach(function (data) {
                                                    if (data.userData._id.toString() == data1.userId.toString() || data.userData._id.toString() == data1.chatWith.toString()) {
                                                        data.userData.conversationId = data1.conversationId
                                                    } else {
                                                        if (data.userData.conversationId == undefined) {
                                                            data.userData.conversationId = null
                                                        }
                                                    }

                                                })
                                            })
                                            var response = {
                                                result: arr,

                                            };
                                            callback(null, response);
                                        }
                                    })
                                }
                            })
                        }
                    })
            }
        })
}
exports.getFollowersFollowings = getFollowersFollowings;