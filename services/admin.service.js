const Admin = require('../models/admin.model')
const User = require('../models/user.model')


const follow = require("../models/follow.model")
const notification = require("../models/notification.model")
const Invoice = require('../models/invoice.model')
const Programs = require('../models/program.model')
const DietPlan = require('../models/diet_programs.model')
const Userplan = require('../models/user_plans.model')
const subscriptionPlan = require('../models/subscription_plan.model');
// const City = require('../models/cities.model')
// const Country = require('../models/countries.model')
const mailer = require('../modules/mailer.helper')
const CMSPages = require('../models/cmsPages.model')
const CONST = require('../config/constants')
const userService = require('../services/v1/user.service')
const bcrypt = require('bcryptjs')
var moment = require('moment')
const _ = require('lodash')

const firebaseFunction = require('../modules/firebase_functions');
const async = require('async');

const ptdtUpcomingPaymentList = async (callback) => {
    try {
        async.parallel([
            async function () {
                var invoiceResult = await Invoice.aggregate(
                    [
                        { $match: { planType: { $gte: 0, $lte: 5 } } },
                        { $match: { paidByAdmin: false } },
                        { $lookup: { from: "program", localField: "planId", foreignField: "_id", as: "program" } },
                        { $lookup: { from: "dietprograms", localField: "dietPlanId", foreignField: "_id", as: "dietPlan" } },
                        { $lookup: { from: "user", localField: "program.createdBy", foreignField: "_id", as: "ptuser" } },
                        { $lookup: { from: "user", localField: "dietPlan.createdBy", foreignField: "_id", as: "dtuser" } },
                        { $project: { 'ptuser._id': 1, 'ptuser.firstName': 1, 'ptuser.type': 1, 'dtuser._id': 1, "amountPaidByUser": 1, 'ptdtPrice': 1, 'dtuser.firstName': 1, 'dtuser.type': 1,'synrgyAmount':1 } },
                        { $unwind: { path: '$program', "preserveNullAndEmptyArrays": true } },
                        { $unwind: { path: '$dietPlan', "preserveNullAndEmptyArrays": true } },
                        { $unwind: { path: '$ptuser', "preserveNullAndEmptyArrays": true } },
                        { $unwind: { path: '$dtuser', "preserveNullAndEmptyArrays": true } },
                        { "$group": { "_id": { 'ptId': "$ptuser._id", 'dtId': "$dtuser._id", 'ptName': "$ptuser.firstName", 'ptType': "$ptuser.type", 'dtId': "$dtuser._id", 'dtName': "$dtuser.firstName", 'dtType': "$dtuser.type", 'totalPayment': "$completecount" }, "ptdtPrice": { "$sum": "$ptdtPrice" },"totalAmount": { "$sum": "$amountPaidByUser" },"commission": { "$sum": "$synrgyAmount" } } },
                        { "$project": { "_id": "$_id", "data": { "ptId": "$_id.ptId", "ptName": "$_id.ptName", "dtId": "$_id.dtId", "dtName": "$_id.dtName", "ptType": "$_id.ptType", "dtType": "$_id.dtType", "ptdtPrice": "$ptdtPrice","totalAmount":"$totalAmount","commission":"$commission" }, _id: 0 } },
                        { $unwind: { path: '$data', "preserveNullAndEmptyArrays": true } },
                        { "$group": { "_id": null, "data": { "$push": "$data" }, total: { $sum: "$data.totalAmount" } } },
                        { $unwind: { path: '$data', "preserveNullAndEmptyArrays": true } },
                        { "$project": { _id: 0 } },
                    ]
                )
                console.log("======>invoiceResult                  : ", invoiceResult);
                return invoiceResult;
            },
            async function (data) {
                var completeInvoiceResult = await Invoice.aggregate(
                    [
                        { $match: { planType: { $gte: 0, $lte: 5 } } },
                        { $match: { paidByAdmin: true } },
                        { $lookup: { from: "program", localField: "planId", foreignField: "_id", as: "program" } },
                        { $lookup: { from: "dietprograms", localField: "dietPlanId", foreignField: "_id", as: "dietPlan" } },
                        { $lookup: { from: "user", localField: "program.createdBy", foreignField: "_id", as: "ptuser" } },
                        { $lookup: { from: "user", localField: "dietPlan.createdBy", foreignField: "_id", as: "dtuser" } },
                        { $project: { 'ptuser._id': 1, 'ptuser.firstName': 1, 'ptuser.type': 1, 'dtuser._id': 1, "amountPaidByUser": 1, 'ptdtPrice': 1, 'dtuser.firstName': 1, 'dtuser.type': 1, } },
                        { $unwind: { path: '$program', "preserveNullAndEmptyArrays": true } },
                        { $unwind: { path: '$dietPlan', "preserveNullAndEmptyArrays": true } },
                        { $unwind: { path: '$ptuser', "preserveNullAndEmptyArrays": true } },
                        { $unwind: { path: '$dtuser', "preserveNullAndEmptyArrays": true } },
                        { "$group": { "_id": { 'ptId': "$ptuser._id", 'dtId': "$dtuser._id", 'ptName': "$ptuser.firstName", 'ptType': "$ptuser.type", 'dtId': "$dtuser._id", 'dtName': "$dtuser.firstName", 'dtType': "$dtuser.type", 'totalPayment': "$completecount" }, "total": { "$sum": "$ptdtPrice" } } },
                        { "$project": { "_id": "$_id", "data": { "ptId": "$_id.ptId", "ptName": "$_id.ptName", "dtId": "$_id.dtId", "dtName": "$_id.dtName", "ptType": "$_id.ptType", "dtType": "$_id.dtType", "total": "$total" }, _id: 0 } },
                        { $unwind: { path: '$data', "preserveNullAndEmptyArrays": true } },
                        { "$group": { "_id": null, "data": { "$push": "$data" }, total: { $sum: "$data.total" } } },
                        { $unwind: { path: '$data', "preserveNullAndEmptyArrays": true } },
                        { "$project": { _id: 0 } },
                    ]
                )
                console.log("======>completeInvoiceResult                  : ", completeInvoiceResult);
                return completeInvoiceResult;
            },
            async function (data) {
                var totalEarning = await Invoice.aggregate(
                    [
                        { $match: { planType: { $gte: 0, $lte: 5 } } },
                        { $match: { paidByAdmin: true } },
                        {
                            "$group": {
                                "_id": {
                                    month: { $month: "$created_at" },
                                    year: { $year: "$created_at" }
                                }, "total": { "$sum": "$ptdtPrice" }, "synrgyAmount": { "$sum": "$synrgyAmount" }
                            }
                        },
                        { "$project": { "_id": "$_id", "data": { "month": "$_id.month", "year": "$_id.year", "total": "$total", "synrgyAmount": "$synrgyAmount" } } },
                        { $unwind: { path: '$data', "preserveNullAndEmptyArrays": true } },
                        {
                            $addFields: {
                                months: {
                                    $let: {
                                        vars: {
                                            monthsInString: [" ",'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                                        },
                                        in: {
                                            $arrayElemAt: ['$$monthsInString', '$data.month']
                                        }
                                    }
                                }
                            }
                        },
                        { "$project": {"data": { "months": "$months", "year": "$_id.year", "total": "$data.total", "synrgyAmount": "$data.synrgyAmount" } } },
                        { "$group": { "_id": null, "data": { "$push": "$data" },totalAmountOfEarning: { $sum: "$data.synrgyAmount" } } },
                        { $unwind: { path: '$data', "preserveNullAndEmptyArrays": true } },
                        { "$project": { _id: 0 } },
                    ]
                )
                console.log("======>totalEarning                  : ", totalEarning);
                return totalEarning;
            }
        ], function (err, result) {
            // console.log("result",result);

            callback(result)
        });
    }
    catch (exception) {
        console.log(exception)
        var resObj = {
            statusCode: 400,
            status: 'error',
            message: 'Some error on server',
            data: exception.messagee
        }
        return resObj
    }
}
exports.ptdtUpcomingPaymentList = ptdtUpcomingPaymentList;

const dietitiansDietPlan = async (req) => {

    console.log(req._id)
    const [getdietitiansDietPlan, numberOfPurchas] = await Promise.all([
        DietPlan.find({ createdBy: req._id, dietType: req.dietType }),
        Invoice.aggregate([
            {
                $match: { '_id': { $ne: null }, 'planType': req.dietType }
            },
            {
                $group: {
                    _id: '$dietPlanId',
                    count: { $sum: 1 }
                }
            },

        ])
    ])

    console.log("1........", getdietitiansDietPlan)
    console.log('2........', numberOfPurchas)

    let arrVal = []
    let objVal = {}
    getdietitiansDietPlan.forEach(data1 => {
        let count = 0;
        numberOfPurchas.forEach(data2 => {
            if (data1._id.toString() === data2._id.toString()) {
                count++
                objVal = {
                    dietName: data1.title,
                    createdDate: moment(data1.created_at).format('YYYY-MM-DD'),
                    price: data1.price,
                    numberOfPurchase: data2.count,
                    total: (data1.price * data2.count).toFixed(2),
                }
                arrVal.push(objVal)
            }
        })
        if (count === 0) {
            objVal = {
                dietName: data1.title,
                createdDate: moment(data1.created_at).format('YYYY-MM-DD'),
                price: data1.price,
                numberOfPurchase: 0,
                total: 0
            }
            arrVal.push(objVal)
        }
    })
    console.log("arrVal", arrVal);
    return arrVal
}
exports.dietitiansDietPlan = dietitiansDietPlan;
const dietitiansCustomizeDietPlan = async (req) => {
    const [getdietitiansDietPlan, numberOfPurchas] = await Promise.all([
        DietPlan.find({ createdBy: req._id, dietType: req.dietType }).populate("userAssigned"),
        Invoice.aggregate([
            {
                $match: { '_id': { $ne: null }, 'planType': req.dietType }
            },
            {
                $group: {
                    _id: '$dietPlanId',
                    count: { $sum: 1 }
                }
            },

        ])
    ])
    let arrVal = []
    let objVal = {}
    getdietitiansDietPlan.forEach(data1 => {

        console.log("getdietitiansDietPlan", data1.userAssigned.firstName);
        numberOfPurchas.forEach(data2 => {
            if (data1._id.toString() === data2._id.toString()) {

                objVal = {
                    dietName: data1.title,
                    createdDate: moment(data1.created_at).format('YYYY-MM-DD'),
                    price: data1.price,
                    userAssigned: data1.userAssigned.firstName + " " + data1.userAssigned.lastName
                }
                arrVal.push(objVal)
            }
        })

    })
    console.log("Arrval", arrVal)
    return arrVal

}
exports.dietitiansCustomizeDietPlan = dietitiansCustomizeDietPlan;

const ptdtPaidByAdminFlag = async (req) => {
    console.log("invoiceUpdate");
    var ptdtPaidByAdminFlag = await Userplan.find({ ptdtId: req.id }, { planId: 1, dietPlanId: 1, _id: 0 })
    console.log("ptdtPaidByAdminFlag", ptdtPaidByAdminFlag);
    var id = [];
    ptdtPaidByAdminFlag.forEach(function (data) {
        if (data.planId != null) {
            id.push(data.planId);
        }
        else {
            id.push(data.dietPlanId);
        }
    })
    console.log("id", id);
    var invoiceUpdate = await Invoice.updateMany({ $or: [{ planId: { $in: id } }, { dietPlanId: { $in: id } }] }, { $set: { paidByAdmin: true } });

    return invoiceUpdate
}
exports.ptdtPaidByAdminFlag = ptdtPaidByAdminFlag;

const register = async (data) => {
    try {
        let uniqueEmail = await Admin.count({ username: data.username })
        if (uniqueEmail === 0) {
            var bcryptPassword = await bcrypt.hash(data.password, 10)
            let sendMail = true
            if (sendMail) {
                var objregitser = {
                    userName: data.username,
                    password: bcryptPassword
                }
                var saveData = await Admin(objregitser).save()
                var resObj = {
                    statusCode: 200,
                    status: 'success',
                    message: 'register successfully..',
                    data: saveData
                }
                return resObj
            } else {
                resObj = {
                    statusCode: 200,
                    status: 'success',
                    message: 'register successfully..',
                    data: saveData
                }
                return resObj
            }
        } else {
            resObj = {
                statusCode: 201,
                status: 'success',
                message: 'this email alredy registerd',
                data: []
            }
            return resObj
        }
    } catch (exception) {
        var resObj = {
            statusCode: 400,
            status: 'error',
            message: 'Some error on server',
            data: exception.message
        }
        return resObj
    }
}
exports.register = register

const getDashboard = async (res) => {
    try {
        const [allUsers, allPT, allDT, activeUser, activePT, activeDT, ApprovalPendingPT, ApprovalPendingDT,totalAmountOfEarning] = await Promise.all([
            User.count({ type: 0, emailVerified: true }),
            User.count({ type: 2, emailVerified: true }),
            User.count({ type: 1, emailVerified: true }),
            User.count({ type: 0, emailVerified: true, isBlocked: false, isDeleted: false }),
            User.count({ type: 2, emailVerified: true, isBlocked: false, isDeleted: false, isCertifiedAndApproved: true }),
            User.count({ type: 1, emailVerified: true, isBlocked: false, isDeleted: false, isCertifiedAndApproved: true }),
            User.count({ type: 2, emailVerified: true, isCertifiedAndApproved: false }),
            User.count({ type: 1, emailVerified: true, isCertifiedAndApproved: false }),
            Invoice.aggregate(
                [
                    { $match: { planType: { $gte: 0, $lte: 5 } } },
                    { $match: { paidByAdmin: true } },
                    {
                        "$group": {
                            "_id":null, "synrgyAmount": { "$sum": "$synrgyAmount" }
                        },
                        
                    },
                    { "$project": { _id: 0 } },
                ]
            )
        ])

        let resDashboard = {
            allUsers: allUsers,
            allPT: allPT,
            allDT: allDT,
            activeUser: activeUser,
            activePT: activePT,
            activeDT: activeDT,
            ApprovalPendingPT: ApprovalPendingPT,
            ApprovalPendingDT: ApprovalPendingDT,
            totalAmountOfEarning:totalAmountOfEarning
        }

        console.log('allUsers..', resDashboard)
        return resDashboard

    } catch (exception) {
        res.send(exception.message)
    }
}

exports.getDashboard = getDashboard

const getUserDetails = async (data, res) => {
    try {
        let userDetails = await User.findById(data)
        return userDetails
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.getUserDetails = getUserDetails

const adminLogin = async (data, res) => {
    try {
        var AdminDetails = await Admin.findOne({ userName: data.username })
        if (AdminDetails) {
            var userPassword = await bcrypt.compare(data.userpassword, AdminDetails.password)
            if (userPassword === true) {
                return true
            } else {
                return 'Password incorrect'
            }
        } else {
            return 'User not found'
        }
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.adminLogin = adminLogin

const changePassword = async (data, res) => {
    try {
        const adminPassword = await Admin.findOne({ userName: 'admin' })
        if (adminPassword) {
            var confirmAdmionPassword = await bcrypt.compare(data.currentPassword, adminPassword.password)
            if (confirmAdmionPassword === true) {
                var bcryptNewPassword = await bcrypt.hash(data.newPassword, 10)
                var updateAdminPassword = await Admin.update({ userName: 'admin' }, { password: bcryptNewPassword })
                return 'Password changed successfully'
            } else {
                return 'Current password is incorrect'
            }
        } else {
            return 'admin table not found'
        }
    } catch (exception) {
        console.log(exception.message)
        res.send(exception.message)
    }
}

exports.changePassword = changePassword

const userList = async (data, res) => {
    try {
        data.emailVerified = true
        const user = await User.find(data)

        let result = user.map(el => {
            var o = Object.assign(el)
            o.dateOfRegistration = moment(el.created_at).format('YYYY-MM-DD')
            console.log("o.dateOfRegistration", typeof o.dateOfRegistration)
            o.dateOfDeletion = moment(el.updated_at).format('YYYY-MM-DD')
            o.blockStatus = el.isBlocked === true ? 'Yes' : 'No'
            o.requestStatus = el.resonToBlocked === null ? 'Pending' : 'Decline'
            // o.checked = el.isBlocked === true ? '' : 'checked = true'
            return o
        })

        return result
    } catch (exception) {
        console.log(exception)
        res.send(exception.message)
    }
}

exports.userList = userList

const getFinanceStatistics = async (data, res) => {
    try {

        let getFinanceStatisticsData

        if (data !== null) {
            // var a = new Date(data.startDate);
            getFinanceStatisticsData = await Invoice.aggregate([
                {
                    $match:
                        { "created_at": { $gte: new Date(data.startDate), $lte: new Date(data.endDate) } }
                },
                {
                    $group: {
                        _id: {
                            year: { $year: "$created_at" },
                            month: { $month: "$created_at" },
                            day: { $dayOfMonth: "$created_at" }
                        },
                        totalSynrgyAmount: { $sum: "$amountPaidByUser" },
                    }
                }
            ])
        } else {
            var lastWeekDay = new Date();
            lastWeekDay.setDate(lastWeekDay.getDate() - 7);
            getFinanceStatisticsData = await Invoice.aggregate([
                {
                    $match:
                        { "created_at": { $gte: lastWeekDay } }
                },
                {
                    $group: {
                        _id: {
                            year: { $year: "$created_at" },
                            month: { $month: "$created_at" },
                            day: { $dayOfMonth: "$created_at" }
                        },
                        totalSynrgyAmount: { $sum: "$amountPaidByUser" },
                    }
                }
            ])
        }
        let financeStatisticsDataResult = getFinanceStatisticsData.map(el => {
            let o = Object.assign(el, {})
            o.date = el._id.year + '/' + el._id.month + '/' + el._id.day
            o.totalSynrgyAmount = el.totalSynrgyAmount
            return o
        })
        console.log("financeStatisticsDataResult", financeStatisticsDataResult);
        // console.log("financeStatisticsDate",financeStatisticsDate);
        return financeStatisticsDataResult
    } catch (exception) {
        console.log(exception)
        res.send(exception.message)
    }
}

exports.getFinanceStatistics = getFinanceStatistics

const personalDetails = async (data, res) => {
    try {
        let personalDetails = await User.findOne(data).populate('country').populate('city').lean()


        let currentDate = moment(new Date()).format('l')
        let mainDate = moment(personalDetails.created_at).format('l')

        console.log("PERSPNAL-DETAILS1::", mainDate)
        console.log("PERSPNAL-DETAILS2::", currentDate)

        // let diff = moment.duration(mainDate.diff(currentDate)) 
        // let days = diff.asMonths();
        let age = '';
        userService.calculateAge(personalDetails.dob, function (result2) {
            age = result2;

        })
        console.log('age:' + age);
        const monthsLeft = moment(currentDate).diff(moment(mainDate), 'months', 'dates')
        const Left = currentDate - mainDate

        console.log("PERSPNAL-DETAILS", monthsLeft)
        personalDetails.age = age;
        console.log('pic:' + personalDetails.profilePic);
        personalDetails.dateOfRegistration = moment(personalDetails.created_at).format('DD MMM YYYY')
        personalDetails.blockedStatusChange = personalDetails.isBlocked === true ? 'Unblock' : 'Block'
        personalDetails.freezedStatus = personalDetails.isFreezed === true ? 'Yes' : 'No'
        personalDetails.emailVerifiedStatus = personalDetails.emailVerified === true ? 'Yes' : 'No'
        personalDetails.profilePicUrl = personalDetails.profilePic == '' ? 'assets/images/use-img-details.png' : personalDetails.profilePic
        personalDetails.deletedDateDifference = monthsLeft
        console.log("personalDetails", personalDetails);
        return personalDetails


    } catch (exception) {
        res.send(exception.message)
    }
}

exports.personalDetails = personalDetails

const userStatusChange = async (req, data, res) => {
    try {
        var userNewStatus = data.userStatus === 'false' ? 'true' : 'false'
        let userUpdate = await User.findByIdAndUpdate(data.userId, { isBlocked: userNewStatus, resonToBlocked: data.blockReason, accounts: [] })
        if (userNewStatus === 'true') {
            let userDetails = await getUserDetails(data.userId)
            var templateVariable = {
                templateURL: 'mailtemplate/reasonToBlock',
                fullName: userDetails.firstName + ' ' + userDetails.lastName,
                BASE_URL: CONST.BASE_URL,
                reasonToBlock: data.blockReason
            }

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: userDetails.email,
                subject: 'Account Blocked'
            }
            // var blockMail = await mailer.sendMail(req, null,mailParamsObject)
            mailer.sendMail(req, null, mailParamsObject, function (err) {
                if (err) {
                    console.log('error', err)
                    res.send(err)
                } else {
                    console.log('Success sending email')
                    return userUpdate
                }
            })
        } else {
            return userUpdate
        }
    } catch (exception) {
        console.log(exception)
        res.send(exception.message)
    }
}

exports.userStatusChange = userStatusChange

const certificateApproveDecline = async (req, data, res) => {
    try {
        console.log('check1:' + data.userStatus);
        let updateCertificateApproveDecline = await User.findByIdAndUpdate(data.userId, { isCertifiedAndApproved: data.userStatus })

        let userDetails = await getUserDetails(data.userId)

        // if (data.userStatus == 'true') {
        if (data.userStatus == 1) {
            var templateVariable = {
                templateURL: 'mailtemplate/certificateApprove',
                fullName: userDetails.firstName + ' ' + userDetails.lastName,
                BASE_URL: CONST.BASE_URL,
                reasonToBlock: data.reason
            }

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: userDetails.email,
                subject: 'Registration Request Approved'
            }
        } else {
            var templateVariable = {
                templateURL: 'mailtemplate/certificateDecline',
                fullName: userDetails.firstName + ' ' + userDetails.lastName,
                BASE_URL: CONST.BASE_URL,
                reasonToBlock: data.reason
            }

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: userDetails.email,
                subject: 'Registration Request Declined'
            }
        }
        mailer.sendMail(req, null, mailParamsObject, function (err) {
            if (err) {
                console.log('error..........', err)
                res.send(err)
            } else {
                console.log('Success sending email')
                firebaseFunction.addUserInFirebase(data.userId, function (data) {
                    return updateCertificateApproveDecline
                });
            }
        })
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.certificateApproveDecline = certificateApproveDecline

const restoreAccount = async (data, res) => {
    try {
        let userRestore = await User.findByIdAndUpdate(data.userId, { isDeleted: false, accountDeletedDate: 0 });
        let followRestore = await follow.updateMany({ $or: [{ followedBy: data.userId }, { followedTo: data.userId }] }, { $set: { isDeleted: false } });
        let notifyUser = await notification.updateMany({ $or: [{ to: data.userId }, { for: data.userId }] }, { $set: { isDeleted: false } });
        return userRestore
    } catch (exception) {
        console.log('exception.message...', exception.message)
        res.send(exception.message)
    }
}

exports.restoreAccount = restoreAccount

const getCMSPages = async (data, res) => {
    try {
        let cmsDetails = await CMSPages.findOne(data)

        return cmsDetails
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.getCMSPages = getCMSPages

const saveCMSInformation = async (data, res) => {
    try {

        let pageType = data.pageType
        let details = data.details
        let saveCMS = await CMSPages.update({ pageType: pageType }, { details: details })
        return saveCMS
    } catch (exception) {
        console.log('exception.message...', exception.message)
        res.send(exception.message)
    }
}

exports.saveCMSInformation = saveCMSInformation

const getCMSPagesDetails = async (data, res) => {
    try {
        let cmsDetails = await CMSPages.findOne(data)

        return cmsDetails
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.getCMSPagesDetails = getCMSPagesDetails

const userPlans = async (data, res) => {
    try {
        //    populate('country').populate('city')
        let getUserPlans = await Invoice.find({ userId: data })
            .populate({
                path: 'planId',
                model: 'program',
                populate: {
                    path: 'createdBy',
                    model: 'user'
                }
            })
            .populate({
                path: 'dietPlanId',
                model: 'dietprograms',
                populate: {
                    path: 'createdBy',
                    model: 'user'
                }
            })
            .populate('subscriptionPlanId', 'title')

        // .populate('planId', 'title')
        // .populate('dietPlanId', 'title')
        // .populate('subscriptionPlanId', 'title')

        console.log("getUserPlans...", getUserPlans)
        let result = getUserPlans.map(el => {
            var o = Object.assign(el, {})
            o.programName = (el.planId === null && el.dietPlanId === null) ? el.subscriptionPlanId.title : ((el.planId === null) ? el.dietPlanId.title : el.planId.title)
            o.createdBy = (el.planId === null && el.dietPlanId === null) ? `Admin` : ((el.planId === null) ? el.dietPlanId.createdBy.firstName + ' ' + el.dietPlanId.createdBy.lastName : el.planId.createdBy.firstName + ' ' + el.planId.createdBy.lastName)
            o.price = el.amountPaidByUser
            o.dateOfPurchase = moment(el.created_at).format('YYYY-MM-DD')

            if (el.planType == 0) {
                o.planType1 = 'Programs'
            } else if (el.planType == 1) {
                o.planType1 = 'Classes'
            } else if (el.planType == 2) {
                o.planType1 = 'Workshops'
            } else if (el.planType == 3) {
                o.planType1 = 'Customize Programs'
            } else if (el.planType == 4) {
                o.planType1 = 'Regular Diet'
            } else if (el.planType == 5) {
                o.planType1 = 'Classcustomize Dietes'
            } else if (el.planType == 6) {
                o.planType1 = 'Dtsubscription Plan'
            } else if (el.planType == 7) {
                o.planType1 = 'Ptsubscription Plan'
            }

            return o
        })
        return result
    } catch (exception) {
        console.log('exception..............', exception)
        res.send(exception.message)
    }
}

exports.userPlans = userPlans

const personalTrainersPlans = async (data, res) => {
    try {
        console.log("data..........", data)
        //  let getPersonalTrainersPlans = await Programs.find({ createdBy : data })
        const [getPersonalTrainersPlans, numberOfPurchas] = await Promise.all([
            Programs.find(data).populate('userAssigned'),
            Invoice.aggregate([
                {
                    $match: { '_id': { $ne: null }, 'planType': data.trainingType }
                },
                {
                    $group: {
                        _id: '$planId',
                        count: { $sum: 1 }
                    }
                }
            ])
        ])

        console.log("1........", getPersonalTrainersPlans)
        console.log('2........', numberOfPurchas)

        let arrVal = []
        let objVal = {}
        getPersonalTrainersPlans.forEach(data1 => {
            let count = 0;
            numberOfPurchas.forEach(data2 => {
                if (data1._id.toString() === data2._id.toString()) {
                    count++
                    objVal = {
                        programName: data1.title,
                        createdDate: moment(data1.created_at).format('YYYY-MM-DD'),
                        price: data1.price,
                        numberOfPurchas: data2.count,
                        total: (data1.price * data2.count).toFixed(2),
                        assignedTo: (data1.userAssigned !== null) ? data1.userAssigned.firstName + ' ' + data1.userAssigned.lastName : null
                    }

                    arrVal.push(objVal)
                }
            })
            if (count === 0) {
                objVal = {
                    programName: data1.title,
                    createdDate: moment(data1.created_at).format('YYYY-MM-DD'),
                    price: data1.price,
                    numberOfPurchas: 0,
                    total: 0
                }

                arrVal.push(objVal)
            }

        })

        return arrVal
    } catch (exception) {
        console.log('exception..............', exception)
        res.send(exception.message)
    }
}

exports.personalTrainersPlans = personalTrainersPlans

var updateCommisionData = async (datas) => {
    try {

        const commisionFlag = await User.findOneAndUpdate({ _id: datas.id }, { $set: { commission: datas.commission } });
        console.log("commisionFlag", commisionFlag);
        return "Commission change successfully";
    }
    catch (exception) {
        console.log('exception..............', exception)
        res.send(exception.message)
    }

}
exports.updateCommisionData = updateCommisionData;

var updatePlanData = async (datas) => {
    console.log(datas)
    try {
        const commisionFlag1 = await subscriptionPlan.findOneAndUpdate({ _id: datas.id }, { $set: { amount: datas.value } });        
        console.log(commisionFlag1);
        return commisionFlag1;
    }
    catch (exception) {
        console.log('exception..............', exception)
        res.send(exception.message)
    }

}
exports.updatePlanData = updatePlanData;