var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;
var userSchema = new mongoose.Schema({
    nickname: String,
    email: String,
    password: { type: String, select: false },
    firstName: String,
    lastName: String,
    // phone: String,
    gender: String, // male , female
    dob: Number,
    // age: Number,
    country: { type: Schema.Types.ObjectId, ref: 'countries' },
    city: { type: Schema.Types.ObjectId, ref: 'cities' },
    verificationToken: String,
    emailVerified: { type: Boolean, default: false },
    accounts: [{ loginType: String, accessToken: String, fcmToken: String, deviceId: String, googleId: String, deviceType: String, location: { type: { type: String }, coordinates: [] } }], //deviceType 0-ios and 1-android    
    profilePic: String,
    certificate: [String],
    type: Number, // 0-user 1-dt 2-pt
    isCertifiedAndApproved: Number, // 0-inactive 1-active 2-declined
    measurements: { height: String, weight: String, activity: String },
    isMeasurements: { type: Boolean, default: false },
    isBlocked: Boolean,
    isDeleted: Boolean,
    isFreezed: Boolean,
    resonToBlocked: { type: String, default: null },
    accountFreezedDate: { type: Number, require: false },
    accountDeletedDate: { type: Number, require: false },
    commission: { type: Number,default:0}
    // following: [{type: Schema.ObjectId, ref: 'user'}],
    // followers: [{type: Schema.ObjectId, ref: 'user'}],
    // createdDate: {type: Number, default: _.now()},
    // updatedDate: {type: Number, default: _.now()}

}, {
        collection: 'user',
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
    });
userSchema.index({ "location": "2dsphere" });
module.exports = mongoose.model('user', userSchema);