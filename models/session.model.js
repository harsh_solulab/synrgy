var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;

const sessionSchema = new mongoose.Schema({
    programId: { type: Schema.Types.ObjectId, ref: 'program' },
    title: String,
    description: String,
    // isActive: { type: Boolean, default: true },
    // isCompleted: { type: Boolean, default: false },
    isDeleted: { type: Boolean, default: false },
    // createdDate: { type: Number, default: _.now() },
    // updatedDate: { type: Number, default: _.now() },
    
}, {
        collection: 'session',
        timestamps: { createdAt: 'created_at',updatedAt: 'updated_at' } 
    },
    );
module.exports = mongoose.model('session', sessionSchema);
