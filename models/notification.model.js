var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;

var notificationSchema = new mongoose.Schema({
    for: { type: Schema.Types.ObjectId, ref: 'user' },
    to: { type: Schema.Types.ObjectId, ref: 'user', default: null},
    notification: String,
    type: String,
    isDeleted: {type: Boolean, default: false},
    planType: { type: Number, default: null },
    programId: { type: Schema.Types.ObjectId, ref: 'program', default: null },
    dietProgramId: { type: Schema.Types.ObjectId, ref: 'dietprograms', default: null },
    feedId: { type: Schema.Types.ObjectId, ref: 'feeds', default: null }
    // createdDate: { type: Number, default: _.now() },
    // updatedDate: { type: Number, default: _.now() }
}, {
        collection: 'notification',
        timestamps: { createdAt: 'created_at',updatedAt: 'updated_at' }
    });
module.exports = mongoose.model('notification', notificationSchema);