var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;
var followSchema = new mongoose.Schema({
    followedBy: { type: Schema.ObjectId, ref: 'user' },
    followedTo: { type: Schema.ObjectId, ref: 'user' },
    // createdDate: { type: Number, default: _.now() },
    // updatedDate: { type: Number, default: _.now() },
    isBlocked: { type: Boolean, default: false },
    isDeleted: { type: Boolean, default: false },
    isFreezed: { type: Boolean, default: false }
}, {
        collection: 'follow',
        timestamps: { createdAt: 'created_at',updatedAt: 'updated_at' }
    });
module.exports = mongoose.model('follow', followSchema);