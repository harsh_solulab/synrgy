var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;

var userPlanSchema = new mongoose.Schema({
    planType: Number, //0-programs 1-classes 2-workshops 3-customizePrograms 4-regularDiet 5-customizeDiet
    planId: { type: Schema.Types.ObjectId, ref: 'program', default: null },
    dietPlanId: { type: Schema.Types.ObjectId, ref: 'dietprograms', default: null },
    // classBatchId: { type: Schema.Types.ObjectId, ref: 'program' },
    calendarEventDate: Number,
    // userSessionId:{type:Schema.Types.ObjectId, ref: 'user_session'},
    userId: { type: Schema.Types.ObjectId, ref: 'user' },
    ptdtId: { type: Schema.Types.ObjectId, ref: 'user' },
    dietExpiredDate: Number,
    isExpired: { type: Boolean, default: false },
    sendNotificationTime: { type: Number, default: null },
    notiFlag: { type: Boolean, default: false },
    paidByAdmin: {type: Boolean, default: false}
    // meals: [{
    //         id: {type: Schema.Types.ObjectId, ref: 'meals'},
    //         isCompleted: {type: Boolean, default: false},
    //         planDate: Number
    //     }],
    // createdDate: {type: Number, default: _.now()},
    // updatedDate: {type: Number, default: _.now()}
}, {
        collection: 'user_plans',
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
    });
module.exports = mongoose.model('user_plans', userPlanSchema);