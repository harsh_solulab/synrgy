var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;

var transectionSchema = new mongoose.Schema({    
    userId: {type: Schema.Types.ObjectId, ref: 'user'},
    programId: {type: Schema.Types.ObjectId, ref: 'program'},
    transectionType: String, //Auth , Capture , Refund
    transectionId: String,
    amount: Number,    
    // createdDate: {type: Number, default: _.now()},
    // updatedDate: {type: Number, default: _.now()}
}, {
    collection: 'transection',
    timestamps: { createdAt: 'created_at',updatedAt: 'updated_at' }
});
module.exports = mongoose.model('transection', transectionSchema);