var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;

var subscriptionPlan = new Schema({
    title: String,
    description: String,
    type: Number,//0-regular,1-platinum,2-gold
    duration:Number,//in Days
    amount: Number,
    userType:Number,//1-DT,2-PT
    // createdDate: { type: Number, default: _.now() },
    // updatedDate: { type: Number, default: _.now() },

},{
    collection:'subscription_plan',
    timestamps: { createdAt: 'created_at',updatedAt: 'updated_at' }
})
module.exports = mongoose.model('subscription_plan', subscriptionPlan);