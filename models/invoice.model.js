var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;

var invoiceSchema = new mongoose.Schema({
    userId: {type: Schema.Types.ObjectId, ref: 'user'},
    planType: Number, //0-programs 1-classes 2-workshops 3-customizePrograms 4-regularDiet 5-customizeDiet, 6-dtsubscriptionPlan,7-ptsubscriptionPlan,
    planId: {type: Schema.Types.ObjectId, ref: 'program', default: null},
    dietPlanId: {type: Schema.Types.ObjectId, ref: 'dietprograms', default: null},
    subscriptionPlanId: {type: Schema.Types.ObjectId, ref: 'subscription_plan', default: null},
    amountPaidByUser: {type: Number, default: 0},
    synrgyAmount: {type: Number, default: 0},
    ptdtPrice: {type: Number, default: 0},
    transactionCharge: {type: Number, default: 0},
    transactionId: String,
    orderId: String,
    isRefunded: {type: Boolean, default: false},
    paymentType: Number, // 0-offline,1-online
    paidByAdmin: {type: Boolean, default: false}
}, {
    collection: 'invoice',
    timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}
});
module.exports = mongoose.model('invoice', invoiceSchema);