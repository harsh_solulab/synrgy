var mongoose = require('mongoose');

var avatarSchema = new mongoose.Schema({
    avatar: String    
}, {
    collection: 'avatar'
});
module.exports = mongoose.model('avatar', avatarSchema);