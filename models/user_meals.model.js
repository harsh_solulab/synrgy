var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userMeals = Schema({
    dietPlanId: { type: Schema.Types.ObjectId, ref: 'dietprograms' },
    userPlanId: { type: Schema.Types.ObjectId, ref: 'user_plans' },
    userId: { type: Schema.Types.ObjectId, ref: 'user' },
    ptdtId: { type: Schema.Types.ObjectId, ref: 'user' },
    mealId: { type: Schema.Types.ObjectId, ref: 'meals' },
    isCompleted: { type: Boolean, default: false },
    planDate: Number,
}, {
        collection: 'user_meals',
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
    })
module.exports = mongoose.model('user_meals', userMeals);