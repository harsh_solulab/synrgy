var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var _ = require('lodash');


var adminSchema = new Schema({
    userName: { type: String },
    password: { type: String },
    createdDate: {type: Date, default: _.now()},
    updatedDate: {type: Date, default: _.now()}
});



module.exports = mongoose.model('admin', adminSchema);

