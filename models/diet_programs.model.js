var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var _ = require("lodash");

var dietProgram = new mongoose.Schema({
    createdBy: { type: Schema.Types.ObjectId, ref: 'user' },
    // isActive:{type: Boolean, default: true},
    // isExpired:{type: Boolean, default: false},
    isDeleted: { type: Boolean, default: false },
    title: String,
    goal: { type: Schema.Types.ObjectId, ref: 'programs_diets_goals' },   // weight loss, maintain, weight gain
    dietType: Number,// 4- regular,5-Customize
    numberOfDays: Number,
    userAssigned: { type: Schema.Types.ObjectId, ref: 'user' },
    // meals:[String],
    // mealsPerDay: Number,
    price: Number,
    coverPic: String
}, {
        collection: 'dietprograms',
        timestamps: { createdAt: 'created_at',updatedAt: 'updated_at' }
    })
module.exports = mongoose.model('dietprograms', dietProgram)