var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;

var blockedUserSchema = new mongoose.Schema({
    userId: String,
    blockedUserId: {type: Schema.Types.ObjectId, ref: 'user'},
    status: Number,
    reason: String,
    // createdDate: {type: Number, default: _.now()},
    // updatedDate: {type: Number, default: _.now()}
}, {
    collection: 'blocked_user',
    timestamps: { createdAt: 'created_at',updatedAt: 'updated_at' }
});
module.exports = mongoose.model('blocked_user', blockedUserSchema);