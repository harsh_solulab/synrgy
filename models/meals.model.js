var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;
var mealsSchema = new mongoose.Schema({
    dietPlanID: { type: Schema.ObjectId, ref: 'dietprograms' },
    title: String,
    description: String,
    calories: Number,
    time: String,
    // date:Number,
    numberOfDay: Number,
    isDeleted: { type: String, default: false },
    // createdDate: {type: Number, default: _.now()},
    // updatedDate: {type: Number, default: _.now()}
}, {
        collection: 'meals',
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
    });
module.exports = mongoose.model('meals', mealsSchema);