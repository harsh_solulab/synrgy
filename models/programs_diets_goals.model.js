var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;
var programsDietsGoalsSchema = new mongoose.Schema({
    name: String,
    planType: Number, // 0-programs, 4-diets
    // createdDate: { type: Number, default: _.now() },
    // updatedDate: { type: Number, default: _.now() }
}, {
        collection: 'programs_diets_goals',
        timestamps: { createdAt: 'created_at',updatedAt: 'updated_at' }
    });
module.exports = mongoose.model('programs_diets_goals', programsDietsGoalsSchema);