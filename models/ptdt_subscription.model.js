var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;

var ptdtSubscriptionSchema = new mongoose.Schema({
    ptdtId: { type: Schema.Types.ObjectId, ref: 'user' },
    subscriptionPlanId: { type: Schema.Types.ObjectId, ref: 'subscription_plan' },
    // amount: Number,
    startDate: { type: Number, default: _.now() },
    renewalDate: { type: Number },
    subscriptionDays: Number,
    isActive: { type: Boolean, default: true }, // 0-inactive 1-active    
    isDeleted: { type: Boolean, default: false },
    userType: Number,//1-dt,2-pt
    sendNotificationTime: { type: Number, default: null },
    notiFlag: { type: Boolean, default: false }
    // createdDate: { type: Number, default: _.now() },
    // updatedDate: { type: Number, default: _.now() }

}, {
        collection: 'ptdt_subscription',
        timestamps: { createdAt: 'created_at',updatedAt: 'updated_at' }
    });

module.exports = mongoose.model('ptdt_subscription', ptdtSubscriptionSchema);