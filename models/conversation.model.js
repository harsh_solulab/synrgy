var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;

var conversationSchema = new mongoose.Schema({
    conversationId: {type: String},
    userId: {type: Schema.Types.ObjectId, ref: 'user'},
    chatWith: {type: Schema.Types.ObjectId, ref: 'user'},
    status: {type: Number, default: 1},
    createdDate: {type: Number, default: _.now()},
    updatedDate: {type: Number, default: _.now()}
}, {
    collection: 'conversation'
});
module.exports = mongoose.model('conversation', conversationSchema);