var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;

var programSchema = new mongoose.Schema({
    createdBy: { type: Schema.Types.ObjectId, ref: 'user' },
    title: String,
    price: Number,
    coverPic: String,
    city: { type: Schema.Types.ObjectId, ref: 'cities' },
    country: { type: Schema.Types.ObjectId, ref: 'countries' },
    description: { type: String, default: "" },
    address: String,
    trainingType: Number, //0-programs,1-classes,2-workshop,3-customize
    duration: Number,
    startDateTime: Number,
    endDateTime: Number,
    calendarEventDate: { type: Number, default: 0 },
    // filterStartTime: Number,
    // filterEndDate: Number,
    // filterEndTime: Number,
    classNumberOfParticipants: { type: Number, default: 0 },
    programType: { type: Schema.Types.ObjectId, ref: 'programs_types', default: null },//without equipment, slimness, maintain
    programMode: { type: String, default: "" },//online,offline
    programGoal: { type: Schema.Types.ObjectId, ref: 'programs_diets_goals', default: null },//weight loss, maintain, gain
    userAssigned: { type: Schema.Types.ObjectId, ref: 'user', default: null },
    isCompleted: { type: Boolean, default: false },
    // isPurchased: { type: Boolean, default: false },
    // isActive: Boolean,
    // isExpired: { type: Boolean, default: false },
    isDeleted: { type: Boolean, default: false },
}, {
        collection: 'program', timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
    });

module.exports = mongoose.model('program', programSchema);