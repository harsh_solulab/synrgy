var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var _ = require('lodash');


var cmsPagesSchema = new Schema({
    details: { type: String },
    pageType: { type: String },
    createdDate: { type: Date, default: _.now() },
    updatedDate: { type: Date, default: _.now() }
}, {
        collection: 'cmspages'
        // timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
    }
);

module.exports = mongoose.model('cmsPages', cmsPagesSchema);

