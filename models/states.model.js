var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;

var stateSchema = new mongoose.Schema({
    id: Number,
    name: String,
    country_id: String,
}, {
        collection: 'states'
    });

module.exports = mongoose.model('states', stateSchema);