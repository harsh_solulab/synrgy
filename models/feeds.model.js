var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;

var feedsSchema = new mongoose.Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'user', default: null },
    followers: [{ type: Schema.Types.ObjectId, ref: 'user' }],
    caption: String,
    media: [],
    type: Number, //0 - video , 1 - photo , 2 - text
    likes: [{ type: Schema.Types.ObjectId, ref: 'user' }],
    commentsCount: { type: Number, default: 0 },
    latestComment: {
        postId: { type: Schema.Types.ObjectId, ref: 'feeds', default: null },
        type: { type: Number },  //0 - parent, 1 - reply
        userId: { type: Schema.Types.ObjectId, ref: 'user', default: null },
        commentString: String,
        createdAt: { type: String, default: _.now() },
        commentId: { type: Schema.Types.ObjectId, ref: 'feeds_comments', default: null }
    },
    createdAt: { type: Number, default: _.now() },
    updatedAt: { type: Number, default: _.now() }

}, {
        collection: 'feeds',
    });
module.exports = mongoose.model('feeds', feedsSchema);