var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;

var citySchema = new mongoose.Schema({
    id: Number,
    name: String,
    state_id: String,
}, {
        collection: 'cities'
    });

module.exports = mongoose.model('cities', citySchema);