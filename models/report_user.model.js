var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;

var reportUserSchema = new mongoose.Schema({
    reportedBy: {type: Schema.Types.ObjectId, ref: 'user'},
    reportedUser: {type: Schema.Types.ObjectId, ref: 'user'},
    status: Number,
    // createdDate: {type: Number, default: _.now()},
    // updatedDate: {type: Number, default: _.now()}
}, {
    collection: 'report_user',
    timestamps: { createdAt: 'created_at',updatedAt: 'updated_at' }
});
module.exports = mongoose.model('report_user', reportUserSchema);