var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;

var userPicturesSchema = new mongoose.Schema({
    userId: {type: Schema.Types.ObjectId, ref: 'user'},
    image: String,
    createdDate: {type: Number, default: _.now()},
    
}, {
    collection: 'user_pictures',
    timestamps: { createdAt: 'created_at',updatedAt: 'updated_at' }
});
module.exports = mongoose.model('user_pictures', userPicturesSchema);