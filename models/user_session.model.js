var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSessionSchema = new Schema({
    planId: { type: Schema.Types.ObjectId, ref: 'program' },
    userPlanId:{ type: Schema.Types.ObjectId, ref: 'user_plans' },
    userId: { type: Schema.Types.ObjectId, ref: 'user' },
    ptdtId: { type: Schema.Types.ObjectId, ref: 'user' },
    sessionId: { type: Schema.Types.ObjectId, ref: 'session' },
    status: { type: Number, default: 0 }, // 0-not_started,1-started,2-paused,3-completed
    startTime: { type: Number, default: 0 },
    endTime: { type: Number, default: 0 },
    remainingDuration: Number,  // in miliseconds
    totalDuration: Number  // in miliseconds
}, {
        collection: 'user_session',
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
    });
module.exports = mongoose.model('user_session', userSessionSchema);