var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;
var programsTypesSchema = new mongoose.Schema({
    name: String,
    planType: { type: Number, default: 0 }, // 0-programs, 4-diets
    // createdDate: { type: Number, default: _.now() },
    // updatedDate: { type: Number, default: _.now() }
}, {
        collection: 'programs_types',
        timestamps: { createdAt: 'created_at',updatedAt: 'updated_at' }
    });
module.exports = mongoose.model('programs_types', programsTypesSchema);