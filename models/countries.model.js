var mongoose=require("mongoose");
var _=require("lodash");
var Schema=mongoose.schema;

var countries= new mongoose.Schema({
    id: String,
    sortname: String,
    name:String
},
{
    collection:'countries'
});

module.exports = mongoose.model('countries', countries);

