var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;

var feedsCommentsSchema = new mongoose.Schema({
    postId: {type: Schema.Types.ObjectId, ref: 'feeds', default: null},
    userId: {type: Schema.Types.ObjectId, ref: 'user', default: null},
    type: Number, //0 - parent, 1 - reply
    replies: [{type: Schema.Types.ObjectId, ref: 'feeds_comments', default: null},],
    commentString: String,
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}

}, {
    collection: 'feeds_comments',
});
module.exports = mongoose.model('feeds_comments', feedsCommentsSchema);