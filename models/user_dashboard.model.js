var mongoose = require('mongoose');
var _ = require('lodash');
var Schema = mongoose.Schema;
var userDashboardSchema = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'user' },
    stepsWeekData: [],
    stepsMonthData: [],
    weightWeekData: [],
    weightMonthData: [],
    heartRateWeekData: [],
    heartRateMonthData: [],
    bodyFatWeekData: [],
    bodyFatMonthData: [],
    distanceWeekData: [],
    distanceMonthData: [],
    caloriesWeekData: [],
    caloriesMonthData: []   
}, {
        collection: 'user_dashboard',
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
    });
module.exports = mongoose.model('user_dashboard', userDashboardSchema);