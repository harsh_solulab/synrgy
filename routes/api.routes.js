const express = require('express');
const router = express.Router();

const usersV1 = require('./api/v1/users');
const ptdtV1 = require('./api/v1/ptdt');
const indexV1 = require('./api/v1/index');
const commonV1 = require('./api/v1/common');
router.use('/v1/users', usersV1);
router.use('/v1/index', indexV1);
router.use('/v1/common', commonV1);
router.use('/v1/ptdt', ptdtV1);


module.exports = router;