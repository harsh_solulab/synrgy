var express = require('express');
var router = express.Router();
const multer = require('multer');
const _ = require('lodash');
var path = require('path');

//profile pic upload
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/images/profile-pic')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + _.now() + path.extname(file.originalname));
    }
});

var upload = multer({
    storage: storage
});

//Middleware
const signupMiddleware = require('../../../middleware/signup.middleware');
const authMiddleware = require('../../../middleware/auth.middleware');

// Get user controller instance.
const userController = require('../../../controllers/v1/user.controller');

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});

router.post('/user-signup', upload.single('profilePic'), signupMiddleware.validateSignupData, userController.signup);

router.post('/add-measurements', authMiddleware.authenticateApi, userController.addMeasurements)

router.post('/user-update', upload.single('profilePic'), authMiddleware.authenticateApi, userController.updateUser);

router.post('/save-dashboard-data', authMiddleware.authenticateApi, userController.saveDashboardData);

router.post('/get-dashboard-data', authMiddleware.authenticateApi, userController.getDashboardData);

router.post('/user-dashboards-list', authMiddleware.authenticateApi, userController.userDashboardsList);

router.post('/update-user-session', authMiddleware.authenticateApi, userController.updateUserSession);

router.post('/get-user-session-details', authMiddleware.authenticateApi, userController.getSessionDetails);

module.exports = router;
