var express = require('express');
var router = express.Router();
const multer = require('multer');
const _ = require('lodash');
var path = require('path');
const constants = require('../../../config/constants');

//profile pic upload
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
       
        if (file.fieldname == "profilePic") {
            cb(null, 'public/images/profile-pic');
        } else if (file.fieldname == "coverPic") {
            console.log(file.fieldname)
            cb(null, 'public/images/cover-pic');
        } else {
            cb(null, 'public/certificates')
        }
    },
    filename: function (req, file, cb) {
 
        cb(null, file.fieldname + _.now() + path.extname(file.originalname));
 
    }
});

var upload = multer({
    storage: storage
});

//Middleware
const signupMiddleware = require('../../../middleware/signup.middleware');
const authMiddleware = require('../../../middleware/auth.middleware');

// Get user controller instance.
const ptdtController = require('../../../controllers/v1/ptdt.controller');

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});

router.post('/ptdt-signup', upload.fields([{ name: 'certificate', maxCount: 10 }, { name: 'profilePic', maxCount: 1 }]), signupMiddleware.validateSignupData, ptdtController.signup);
// router.post('/ptdt-update', upload.fields([{ name: 'certificate', maxCount: 10 }, { name: 'profilePic', maxCount: 1 }]), authMiddleware.authenticateApi, ptdtController.updatePtdtDetails);

router.post('/add-training-program', upload.single('coverPic'), authMiddleware.authenticateApi, ptdtController.addTrainingProgram);
router.post('/update-training-program', upload.single('coverPic'), authMiddleware.authenticateApi, ptdtController.updateTrainingProgram);
router.post('/training-programs-listing', authMiddleware.authenticateApi, ptdtController.getAllProgramsList);
router.post('/training-program-details', authMiddleware.authenticateApi, ptdtController.getProgramDetails);
router.post('/delete-training-program', authMiddleware.authenticateApi, ptdtController.deleteProgram);
router.post('/edit-session', authMiddleware.authenticateApi, ptdtController.editSession);
router.post('/delete-session', authMiddleware.authenticateApi, ptdtController.deleteSession);
router.post('/get-sessions', authMiddleware.authenticateApi, ptdtController.getSessions);

router.post('/add-diet-plan', upload.single('coverPic'), authMiddleware.authenticateApi, ptdtController.addDietPlan);
router.post('/edit-diet-plan-meals', authMiddleware.authenticateApi, ptdtController.editDietPlanMeals);
router.post('/delete-diet-plan-meals', authMiddleware.authenticateApi, ptdtController.deleteDietPlanMeals);
router.post('/diet-plan-meals-listing', authMiddleware.authenticateApi, ptdtController.dietPlanMealsListing);
router.post('/diet-plan-meal-details', authMiddleware.authenticateApi, ptdtController.dietPlanMealDetails);
router.post('/update-diet-plan', upload.single('coverPic'), authMiddleware.authenticateApi, ptdtController.updateDietPlan);
router.post('/diet-plan-detail', authMiddleware.authenticateApi, ptdtController.dietPlanDetail);
router.post('/all-diet-plan-details', authMiddleware.authenticateApi, ptdtController.allDietPlanDetails);
router.post('/delete-diet-plan', authMiddleware.authenticateApi, ptdtController.deleteDietPlan);

router.post('/add-training-classes', upload.single('coverPic'), authMiddleware.authenticateApi, ptdtController.addTrainingClasses);
router.post('/edit-training-classes', upload.single('coverPic'), authMiddleware.authenticateApi, ptdtController.editTrainingClasses);
router.post('/delete-training-classes', authMiddleware.authenticateApi, ptdtController.deleteTrainingClasses);

router.post('/add-customize-training-program', upload.single('coverPic'), authMiddleware.authenticateApi, ptdtController.addCustomizeProgram);
router.post('/edit-customize-training-program', upload.single('coverPic'), authMiddleware.authenticateApi, ptdtController.editCustomizeProgram);

router.post('/add-customize-diet-plan', upload.single('coverPic'), authMiddleware.authenticateApi, ptdtController.addCustomizeDietPlan);
router.post('/edit-customize-diet-plan', upload.single('coverPic'), authMiddleware.authenticateApi, ptdtController.editCustomizeDietPlan);

router.post('/add-training-workshop', upload.single('coverPic'), authMiddleware.authenticateApi, ptdtController.addTrainingWorkshop);
router.post('/edit-training-workshop', upload.single('coverPic'), authMiddleware.authenticateApi, ptdtController.editTrainingWorkshop);
router.post('/delete-training-workshop', authMiddleware.authenticateApi, ptdtController.deleteTrainingWorkshop);

router.post('/get-user-purchase-programs', authMiddleware.authenticateApi, ptdtController.getUserPurchasePrograms);
router.post('/download-personal-information', authMiddleware.authenticateApi, ptdtController.downloadPersonalInformation);
router.post('/get-user-purchase-dietplan', authMiddleware.authenticateApi, ptdtController.getUserPurchaseDietPlan);

router.post('/add-subscriptionplan', authMiddleware.authenticateApi, ptdtController.addSubscriptionPlan);
router.post('/get-subscription-plans', authMiddleware.authenticateApi, ptdtController.getSubscriptionPlan);
router.post('/get-programs-diets-goals-types', authMiddleware.authenticateApi, ptdtController.getProgramsDietsGoalsTypes);
router.post('/get-ptdt-invoice',authMiddleware.authenticateApi,ptdtController.generatePtdtInvoice);

// router.post('/dummy-data',authMiddleware.authenticateApi,ptdtController.dummyData)
module.exports = router;