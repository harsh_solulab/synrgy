var express = require('express');
var router = express.Router();

//Middleware
const authMiddleware = require('../../../middleware/auth.middleware');

// Get common controller instance.
const commonController = require('../../../controllers/v1/common.controller');


router.post('/follow', authMiddleware.authenticateApi, commonController.follow);

router.post('/get-countries', commonController.getCountries);

router.post('/get-cities', commonController.getCities);

// router.post('/change-password', authMiddleware.authenticateApi, commonController.changePwd);

router.post('/get-followers', authMiddleware.authenticateApi, commonController.getFollowers);

router.post('/get-followings', authMiddleware.authenticateApi, commonController.getFollowings);

router.post('/avatar-listing', commonController.getAvatars);

router.post('/user-pictures-listing', authMiddleware.authenticateApi, commonController.getUserPictures);

router.post('/notifications-listing', authMiddleware.authenticateApi, commonController.getNotifications);

router.post('/search', authMiddleware.authenticateApi, commonController.getSearchResult);

router.post('/search-details', authMiddleware.authenticateApi, commonController.getPersonalizedSearchResults);

router.post('/resend-email', commonController.resendVerificationMail);

router.post('/clear-all-notifications', authMiddleware.authenticateApi, commonController.clearAllNotifications);

router.post('/measurements', authMiddleware.authenticateApi, commonController.getMeasurements);

router.post('/purchase-plan', authMiddleware.authenticateApi, commonController.purchasePlan);

router.post('/get-calendar-events', authMiddleware.authenticateApi, commonController.getCalendarEvents);

router.post('/add-conversation-id', authMiddleware.authenticateApi, commonController.addConversationId);

router.post('/get-conversation-id', authMiddleware.authenticateApi, commonController.getConversationId);

router.post('/add-feed', authMiddleware.authenticateApi, commonController.addFeed);

router.post('/like-post', authMiddleware.authenticateApi, commonController.likePost);

router.post('/add-comment', authMiddleware.authenticateApi, commonController.addComment);

router.post('/edit-feed', authMiddleware.authenticateApi, commonController.editFeed);

router.post('/delete-feed', authMiddleware.authenticateApi, commonController.deleteFeed);

router.post('/get-user-feeds', authMiddleware.authenticateApi, commonController.getUserFeeds);

router.post('/get-feed-details', authMiddleware.authenticateApi, commonController.getFeedDetails);

router.post('/get-feed-comments', authMiddleware.authenticateApi, commonController.getFeedComments);

router.post('/get-feeds', authMiddleware.authenticateApi, commonController.getFeeds);

router.post('/get-followers-followings', authMiddleware.authenticateApi, commonController.getFollowersFollowings);

module.exports = router;
