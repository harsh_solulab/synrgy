var express = require('express');
var router = express.Router();
const _ = require('lodash');

// Get user controller instance.
const indexController = require('../../../controllers/v1/index.controller');

//Middleware
const authMiddleware = require('../../../middleware/auth.middleware');

router.post('/signin', indexController.signin);

router.post('/freez-account', authMiddleware.authenticateApi, indexController.freezAccount);

router.post('/delete-account', authMiddleware.authenticateApi, indexController.deleteAccount);

//forgot password api
router.post('/forgot-password', indexController.forgotPwd);

router.post('/logout', authMiddleware.authenticateApi, indexController.logout);

router.post('/get-user-profile', authMiddleware.authenticateApi, indexController.getUserInfo);

//router.post('/forgotPwd', indexController.forgotpwd);
//
//router.post('/reset-password', indexController.resetpassword);
//
//router.get('/updatePassword', indexController.updatePassword);

module.exports = router;
