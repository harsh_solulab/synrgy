var express = require('express')
var router = express.Router()

const adminController = require('../controllers/admin.controller')
const adminMiddleware = require('../middleware/admin.middleware')

const indexController = require('../controllers/v1/index.controller')
const paytabs = require('../services/v1/paytabs.service');
/* GET home page. */
router.get('/', (req, res, next) => {
// res.render('index', { title: 'Express' });
    res.redirect('login')
})

/* user login */
router.get('/login', (req, res, callback) => {
    if (req.session.isLoggedIn == undefined) {
        //  res.send("Login........");
        // var messages = req.flash('messages');
        // var errors = req.flash('errors');
        // var renderParams = {
        //     layout: false,
        //     messages: messages,
        //     errors: errors,
        //     constants: constants
        // };
        res.render('login')
    } else {
        res.redirect('dashboard')
    }
})

router.get('/test', paytabs.createPayment);

router.get('/reset-password/:token', indexController.checkForgotPwdToken)
router.post('/reset-password/:userId/:token', indexController.resetPwd)
router.get('/verify-email/:token', indexController.verifyEmail)

router.post('/register', adminController.register)
router.post('/login', adminController.adminLogin)
router.get('/logout', adminMiddleware.authenticateAdmin, adminController.logout)
router.get('/dashboard', adminMiddleware.authenticateAdmin, adminController.dashboard)
router.get('/userList', adminMiddleware.authenticateAdmin, adminController.userList)
router.get('/userDeletedList', adminMiddleware.authenticateAdmin, adminController.userDeletedList)
router.get('/personalTrainersList', adminMiddleware.authenticateAdmin, adminController.personalTrainersList)
router.get('/personalTrainersRegistrationRequests', adminMiddleware.authenticateAdmin, adminController.personalTrainersRegistrationRequests)
router.get('/personalTrainersDeletedList', adminMiddleware.authenticateAdmin, adminController.personalTrainersDeletedList)
router.get('/dietitiansList', adminMiddleware.authenticateAdmin, adminController.dietitiansList)
router.get('/payments', adminMiddleware.authenticateAdmin, adminController.payments)
router.get('/payments/:id', adminMiddleware.authenticateAdmin, adminController.paidByAdmin)
router.get('/dietitiansRegistrationRequests', adminMiddleware.authenticateAdmin, adminController.dietitiansRegistrationRequests)
router.get('/dietitiansDeletedList', adminMiddleware.authenticateAdmin, adminController.dietitiansDeletedList)
router.get('/financeStatistics', adminMiddleware.authenticateAdmin,adminController.financeStatistics)
router.get('/participantsStatistics/', adminMiddleware.authenticateAdmin, adminController.participantsStatistics)
router.get('/deviceStatistics', adminMiddleware.authenticateAdmin, adminController.deviceStatistics)
router.get('/citiesStatistics', adminMiddleware.authenticateAdmin, adminController.citiesStatistics)
router.get('/complaintsManagement', adminMiddleware.authenticateAdmin, adminController.complaintsManagement)
router.get('/measurementsManagement', adminMiddleware.authenticateAdmin, adminController.measurementsManagement)
router.get('/ptSubscriptionList', adminMiddleware.authenticateAdmin, adminController.ptSubscriptionList)
router.get('/dtSubscriptionList', adminMiddleware.authenticateAdmin, adminController.dtSubscriptionList)
router.get('/cmsPages', adminMiddleware.authenticateAdmin, adminController.cmsPages)
router.get('/settings', adminMiddleware.authenticateAdmin, adminController.settings)
router.get('/plans', adminMiddleware.authenticateAdmin, adminController.plans);
router.get('/dtplans', adminMiddleware.authenticateAdmin, adminController.dtplans)

router.get('/dietitiansDietPlan/:id', adminMiddleware.authenticateAdmin, adminController.dietitiansDietPlan)
router.get('/dietitiansCustomizeDietPlan/:id', adminMiddleware.authenticateAdmin, adminController.dietitiansCustomizeDietPlan)

router.get('/userDetails/:id?', adminMiddleware.authenticateAdmin, adminController.userDetails)
router.get('/personalTrainersDetails/:id?', adminMiddleware.authenticateAdmin, adminController.personalTrainersDetails)
router.get('/personalTrainersRequestDetails/:id?', adminMiddleware.authenticateAdmin, adminController.personalTrainersRequestDetails)
router.get('/personalTrainersDeletedDetails/:id?', adminMiddleware.authenticateAdmin, adminController.personalTrainersDeletedDetails)
router.get('/dietitiansDetails/:id?', adminMiddleware.authenticateAdmin, adminController.dietitiansDetails)
router.get('/dietitiansRegistrationRequestsDetails/:id?', adminMiddleware.authenticateAdmin, adminController.dietitiansRegistrationRequestsDetails)
router.get('/dietitiansDeletedDetails/:id?', adminMiddleware.authenticateAdmin, adminController.dietitiansDeletedDetails)
router.get('/userDeletedDetails/:id?', adminMiddleware.authenticateAdmin, adminController.userDeletedDetails)
router.get('/complaintsManagementViewDetails', adminMiddleware.authenticateAdmin, adminController.complaintsManagementViewDetails)
router.get('/editAboutUs', adminMiddleware.authenticateAdmin, adminController.editAboutUs)
router.get('/editPrivacyPolicy', adminMiddleware.authenticateAdmin, adminController.editPrivacyPolicy)
router.get('/editPrivacyPolicy', adminMiddleware.authenticateAdmin, adminController.editPrivacyPolicy)
router.post('/editPrivacyPolicy', adminMiddleware.authenticateAdmin, adminController.editPrivacyPolicy)
router.get('/editTermsAndCoditions', adminMiddleware.authenticateAdmin, adminController.editTermsAndCoditions)
router.get('/createPTSubscription', adminMiddleware.authenticateAdmin, adminController.createPTSubscription)
router.get('/createDTSubscription', adminMiddleware.authenticateAdmin, adminController.createDTSubscription)
router.get('/viewPTSubscription', adminMiddleware.authenticateAdmin, adminController.viewPTSubscription)
router.get('/viewDTSubscription', adminMiddleware.authenticateAdmin, adminController.viewDTSubscription)
router.get('/editPTSubscription', adminMiddleware.authenticateAdmin, adminController.editPTSubscription)
router.get('/editDTSubscription', adminMiddleware.authenticateAdmin, adminController.editDTSubscription)


router.post('/resetPassword', adminMiddleware.authenticateAdmin, adminController.resetPassword)
router.post('/userStatusChange', adminMiddleware.authenticateAdmin, adminController.userStatusChange)
router.post('/certificateApproveDecline', adminMiddleware.authenticateAdmin, adminController.certificateApproveDecline)

router.post('/saveCMSInformation', adminMiddleware.authenticateAdmin, adminController.saveCMSInformation)

router.post('/restoreAccount', adminMiddleware.authenticateAdmin, adminController.restoreAccount) 


router.get('/aboutUs', adminController.aboutUs)
router.get('/privacyPolicy', adminController.privacyPolicy)
router.get('/termsAndConditions', adminController.termsAndConditions)


router.get('/userPlans/:id?', adminMiddleware.authenticateAdmin, adminController.userPlans)
router.get('/personalTrainersPrograms/:id?', adminMiddleware.authenticateAdmin,adminController.personalTrainersPrograms)
router.get('/personalTrainersCustomPrograms/:id?', adminMiddleware.authenticateAdmin,adminController.personalTrainersCustomPrograms)
router.get('/personalTrainersClass/:id?',adminMiddleware.authenticateAdmin, adminController.personalTrainersClass)
router.get('/personalTrainersWorkshops/:id?', adminMiddleware.authenticateAdmin,adminController.personalTrainersWorkshops)

router.post('/financeStatisticsDate', adminController.financeStatisticsDate)
router.post('/updateCommision/:id', adminController.updateCommision);
router.post('/updatePlan', adminController.updatePlan)

// router.get('/aboutUs', (req, res, next) => {
//     // res.render('index', { title: 'Express' });
//         res.redirect('admin/aboutUs')
//     })


module.exports = router
