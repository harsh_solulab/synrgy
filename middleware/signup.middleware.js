// Load user model.
const User = require('../models/user.model');
const sendResponse = require('../modules/response.helper');

// A middleware to check if posted signup data is valid or not.
exports.validateSignupData = function (req, res, next) {
    var email = req.body.email.toLowerCase();
    User.find({'email': email}, function (err, user) {
        if (err) {
            console.log("err", err);
            return sendResponse.sendJsonResponse(req, res, 201, err, "1", "error");
        } else {
            console.log("user", user);
            // Send an error response if a user with this email already exists.
            if (user.length > 0) {
                return sendResponse.sendJsonResponse(req, res, 201, "", "1", "User already exists.");
            } else {
                // All ok. Move forward.
                next();
            }
        }
    });
};
