// Load user model.
const User = require('../models/user.model');
const sendResponse = require('../modules/response.helper');

exports.verifyToken = function (req, res, next) {
    User.findOne({ '_id': req.body.userId, 'accessToken': req.headers.accesstoken, 'status': 1 }, function (err, user) {
        // Handle internal server error.
        if (err) {
            return sendResponse.sendJsonResponse(req, res, 203, {}, "1", "Invalid Input");
        } else {
            if (user != null) {
                // move to next
                next();
            } else {
                return sendResponse.sendJsonResponse(req, res, 202, {}, "1", "You are not authorized user.");
            }
        }
    });
};

exports.authenticateApi = function (req, res, next) {
    if (req.headers.authorization) {
        var authArr = req.headers.authorization.split(' ');
        var authToken = authArr[1];
        User.findOne({ 'accounts': { $elemMatch: { accessToken: authToken } } }).lean().exec(function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 202, {}, "1", "error");
            }
            else {
                if (result && result.isCertifiedAndApproved == 1) {
                    if (result.isFreezed == true) {
                        return sendResponse.sendJsonResponse(req, res, 202, {}, "1", "Your account has been Freezed, please login to continue.");
                    } else if (result.isDeleted == true) {
                        return sendResponse.sendJsonResponse(req, res, 202, {}, "1", "Your account has been Deleted, please contact Admin to restore account.");
                    } else if (result.isBlocked == true) {
                        return sendResponse.sendJsonResponse(req, res, 202, {}, "1", "Your account has been Blocked, please contact Admin to restore account.");
                    } else {
                        req.authenticationId = result._id;
                        req.authToken = authToken;
                        req.userType = result.type;
                        next();
                    }
                } else {
                    return sendResponse.sendJsonResponse(req, res, 202, {}, "1", "You are not authorized user.");
                }
            }
        });
    } else {
        return sendResponse.sendJsonResponse(req, res, 202, {}, "1", "You are not authorized user.");
    }
};
