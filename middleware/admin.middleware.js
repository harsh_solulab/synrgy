/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//const commonService = require('../services/common.service');

exports.authenticateAdmin = function (req, res, next) {
    if (req.session.isLoggedIn) {
        console.log('admin session', req.session.isLoggedIn);
        next();
    } else {
        res.redirect('/login');
    }
};

exports.isLogin = function (req, res, next) {
    if (req.session.adminLogin) {
        res.redirect('/dashboard');
    } else {
        next();
    }
};

