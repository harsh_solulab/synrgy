const functions = require('firebase-functions');
const admin = require('firebase-admin');
const async = require('async');
admin.initializeApp();

function fetchUsers(users) {
    let token = "";
    let deviceType = "";
    let result = {};
    console.log(users);
    let userresolve = new Promise((resolve, reject) => {
        for (var prop in users) {
            let userId = users[prop];
            // console.log("userid: ", userId);
            let userRef = admin.database().ref(`/users/${userId}`);

            userRef.on('value', (snapShot) => {
                var snapVal = snapShot.val();
                if (snapVal.accounts) {
                    result = snapVal.accounts;
                }
//                if (snapVal.token != '' && snapVal.token != null) {
//                    result.token = snapVal.token;
//                    result.deviceType = snapVal.device_type;
//                }

                setTimeout(function () {
                    resolve(result);
                }, 1000);
            }, (err) => {
                reject(err);
            });
        }
    });

    return userresolve;
}

exports.sendMessageNotification = functions.database.ref('/conversations/{conversationsID}/last_message').onWrite((change, context) => {
    //const optionType = event.params.optionType;
    console.log("change", change.after);
    console.log("context", context);
    let conversationData = change.after.val();
    console.log("conversationData", conversationData);
    // Exit when the data is deleted.

    if (conversationData.text === "" || conversationData.text === null) {
        console.log("Stop Execution on blank value");
        return "Blank  Message";
        // break;
    }
    const message = conversationData.text;
    const senderName = conversationData.senderName;
    const senderId = conversationData.senderId;
    // const isGroup = conversationData.isGroup;
    const messageUId = conversationData.msgID;

    const conversationUid = context.params.conversationsID;         // conversion id which gives the  conversation id to fetch  message data from messages table
    const parentId = change.after.ref.parent.key    // same as conversionID

    console.log(message);

    // fetch the users of the conversations:
    let conversationUsers = [];
    let conversationUsersArray = [];

    let obj = change.after.ref.parent.child('chat_history');   // get the field value of the same table with the different key
    //console.log("obj",obj);
    obj.on('value', (snap) => {
        //console.log("snap",snap);
        snap.forEach((snapLoop) => {
            console.log("snapLoop.key", snapLoop.key);
            console.log("snapLoop.val", snapLoop.val);
            if (conversationData.senderId !== snapLoop.key) {
                conversationUsersArray.push(snapLoop.val());
                conversationUsers.push(snapLoop.key);
            }
        });

        console.log("convo", conversationUsers);
        console.log("array", conversationUsersArray);

        return fetchUsers(conversationUsers).then(result => {


            console.log("result individual value: ", result);
            //console.log("individual conversion timestamp value: ", conversationUsersArray[key]);

            notificationTitle = `${senderName} sent a new message!`;
            notificationMesssage = message;
            /************************************** For loop *******************************************/
            
            console.log(notificationTitle)
            console.log(notificationMesssage)

            async.forEach(result, function (results, callback1) {
                if (results.fcmToken) {
                    if (results.deviceType == "1") {

                        var payload = {
                            // data: {
                            //     title: title,
                            //     messages: message,
                            //     type: "invitation_message",
                            //     sessionId: sessionId
                            // }
                            data: {
                                conv_id: conversationUid,
                                msgid: messageUId,
                                senderId: senderId,
                                // timestamp: conversationUsersArray[key],
                                //title: `${senderName} sent a new message!`,
                                title: notificationTitle,
                                messages: notificationMesssage,
                                type: "chat_message",
                            }
                        };
                    } else {

                        var payload = {

                            notification: {
                                // conv_id: conversationUid,
                                id: conversationUid,
                                msgid: messageUId,
                                senderId: senderId,
                                //title: `${senderName} sent a new message!`,
                                title: notificationTitle,
                                // title: `You have a new message from !`,
                                body: notificationMesssage,
                                sound: 'default',
                                badge: '1',
                                priority: "high",
                                type: "chat_message",

                            },
                            data: {
                                conv_id: conversationUid,
                                msgid: messageUId,
                                senderId: senderId,
                                // timestamp: conversationUsersArray[key],
                                //title: `${senderName} sent a new message!`,
                                title: notificationTitle,
                                message: notificationMesssage,
                                type: "chat_message",
                                "content-available": "1"
                            }
                        };
                    }

                    console.log("payload", payload);

                    const options = {
                        priority: "high",
                        alert: "true",
                        timeToLive: 60 * 60 * 24
                    };

                    admin.messaging().sendToDevice(results.fcmToken, payload, options)
                            .then(function (response) {
                                console.log("Successfully sent message:", response);
                                console.log(response.results[0].error);
                            })
                            .catch(function (error) {
                                console.log("Error sending message:", error);
                            });
                    callback1();
                } else {
                    callback1();
                }
                // tell async that that particular element of the iterator is done
            }, function (err) {
                if (err) {
                } else {

                }
            });
            /************************************** For loop *******************************************/            
        });
        return conversationUsers;

    });

});
