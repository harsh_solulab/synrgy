const constants = require('../config/constants');
var appRoot = require('app-root-path');
const admin = require("firebase-admin");
const user = require('../models/user.model');
const feeds = require('../models/feeds.model');
var serviceAccount = require(appRoot + '/firebase/fitnessapp-d4ed8-firebase-adminsdk-yy6ym-e2d522b6c2.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: constants.FB_DATABASE
});

var db = admin.database();

var addUserInFirebase = function (userId, callback) {
    try {
        user.findById(userId).exec(function (err, userFound) {
            if (err) {
                callback("err");
            } else {
                var ref = db.ref(`/users/${userId}`);
                ref.set({
                    id: userId.toString(),
                    last_seen: "",
                    first_name: userFound.firstName,
                    last_name: userFound.lastName,
                    profile_pic: userFound.profilePic,
                    accounts: [],
                    type: userFound.type
                });
                callback("success");
            }
        });
    } catch (exception) {
        console.log("exceptiom", exception);
        throw Error("Error while adding user in firebase database");
    }
}

exports.addUserInFirebase = addUserInFirebase;

var addTokenInFirebase = async function (userId) {
    try {
        var userFound = await user.findById(userId).lean();
        console.log("userFound............", userFound);
        console.log("accounts............", userFound.accounts);
        var ref = db.ref(`/users/${userId}`);
        ref.update({
            accounts: userFound.accounts,
        });
    } catch (exception) {
        console.log("exceptiom", exception);
        throw Error("Error while updating user token in firebase database");
    }
}

exports.addTokenInFirebase = addTokenInFirebase;

var updateFeedData = function (id, callback) {
    try {
        feeds.findById(id).populate('userId', '_id firstName lastName profilePic type').exec(function (err, postFound) {
            if (err) {
                callback("err");
            } else {
                if (postFound.type != 2) {
                    var obj = {
                        caption: postFound.caption,
                        media: [postFound.media[0]],
                        postId: postFound._id,
                        type: postFound.type,
                        user: {_id: postFound.userId._id,
                            firstName: postFound.userId.firstName,
                            lastName: postFound.userId.lastName,
                            profilePic: postFound.userId.profilePic,
                            type: postFound.userId.type}
                    };
                } else {
                    var obj = {
                        caption: postFound.caption,                        
                        postId: postFound._id,
                        type: postFound.type,
                        user: {_id: postFound.userId._id,
                            firstName: postFound.userId.firstName,
                            lastName: postFound.userId.lastName,
                            profilePic: postFound.userId.profilePic,
                            type: postFound.userId.type}
                    };
                }
                console.log(obj);
                var ref = db.ref(`/posts/${id}`);
                ref.update(obj);
                callback("success");
            }
        });
    } catch (exception) {
        console.log("exceptiom", exception);
        throw Error("Error while updating feed in firebase database");
    }
}

exports.updateFeedData = updateFeedData;

var sendSessionNotification = async function (deviceTokens, title, message, sessionId, deviceType, callback) {

    try {

        //0-ios and 1-android
        if (deviceType == "1") {

            var payload = {
                data: {
                    title: title,
                    messages: message,
                    type: "invitation_message",
                    sessionId: sessionId
                }
            };
        } else {

            var payload = {
                notification: {
                    title: title,
                    body: message,
                    type: "invitation_message"
                },
                data: {sessionId: sessionId}
            };
        }

        // const options = {
        //     priority: "high",
        //     alert: "true",
        //     timeToLive: 60 * 60 * 24
        // };

        admin.messaging().sendToDevice(deviceTokens, payload)
                .then(function (response) {
                    console.log("Successfully sent message:", response);
                    console.log(response.results[0].error);
                    callback(null, response);
                })
                .catch(function (error) {
                    console.log("Error sending message:", error);
                    callback(error, error);
                });
        console.log("payload", payload);
    } catch (exception) {
        console.log("exceptiom", exception);
        throw Error("Error while sending notifications to the coach");
    }

}

exports.sendSessionNotification = sendSessionNotification;