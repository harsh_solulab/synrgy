const constants = require('../config/constants');
var FCM = require('fcm-node');
var serverKey = constants.FCM_SERVER_KEY;
var fcm = new FCM(serverKey);
const async = require('async');

var sendNotifications = function (fcm_token_data, title, messages, data, callback) {

    async.forEach(fcm_token_data, function (results, callback1) {
        var message = {};
        //0-ios and 1-android
        if (results.deviceType == "1") {
            data.title = title;
            data.messages = messages;
            message = {
                to: results.fcmToken,
                data: data
            };
        } else {
            message = {
                to: results.fcmToken,
                notification: {
                    title: title,
                    body: messages,
                },
                data: data
            };
        }


        console.log(message);

        fcm.send(message, function (err, response) {
            if (err) {
                console.log("Something has gone wrong!", err);
                callback1();
            } else {
                console.log("Successfully sent with response: ", response);
                callback1();
            }
        });

    }, function (err) {
        if (err) {
            callback('err');
        } else {
            callback('sucess');
        }
    });
};
exports.sendNotifications = sendNotifications;