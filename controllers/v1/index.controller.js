const userService = require('../../services/v1/user.service');
const mailer = require('../../modules/mailer.helper');
const constants = require('../../config/constants');
const sendResponse = require('../../modules/response.helper');

exports.signin = async function (req, res, next) {
    console.log(req.body);
    // Get posted request data for the user sign in.
    var user = {
        email: req.body.email.toLowerCase()
    };
    try {
        // Create a new user account using user service object.
        var userSignInChk = await userService.signinData(user);
        var getPass = await userService.getPass(user);
        if (userSignInChk !== null) {
            var userData = await userService.chkSignIn(userSignInChk, req.body.password, getPass.password, req.body.deviceId, req.body.latitude, req.body.longitude, req.body.deviceType, req.body.fcmToken);
            if (userData.error == 0) {
                return sendResponse.sendJsonResponse(req, res, 200, userData.data, "0", "User signed in successfully.");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", userData.msg);
            }
        } else {
            return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "User not found.");
        }
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
};


exports.freezAccount = function (req, res, next) {
    //    console.log('user ID', req.authenticationId);
    try {
        userService.freezUserAccount(req, function (err, freezAc) {
            if (freezAc) {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "User Account Freez Successfully");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            }
        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
};

exports.deleteAccount = function (req, res, next) {
    //    console.log('user ID', req.authenticationId);
    try {
        userService.deleteUserAccount(req, function (err, deleteAc) {
            if (deleteAc) {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "User Account Deleted Successfully");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            }
        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
};

exports.getUserInfo = function (req, res) {
    try {
        userService.getUserInfo(req, function (userInfo) {
            if (userInfo != "err") {
                return sendResponse.sendJsonResponse(req, res, 200, userInfo, "0", "User Profile");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            }
        })
    } catch (exception) {
        console.log(exception)
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
};


exports.verifyEmail = function (req, res) {
    try {
        userService.verifyEmail(req, function (tokenValidation) {
            if (tokenValidation != 'err') {
                if (tokenValidation.type < 1) {
                    var dataString = 'Your email has been verified. You can login with your email address and password to access the Application.'
                } else {
                    var dataString = 'Your email has been verified. We will notify you once we approve your registration request.'
                }
                return res.render("verifyemail", {
                    BASE_URL: constants.BASE_URL,
                    data: dataString,
                    firstName: tokenValidation.firstName,
                    lastName: tokenValidation.lastName
                });
            } else {
                return res.render("error", { BASE_URL: constants.BASE_URL,emailError: 'Invalid link' });
            }
        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}

exports.forgotPwd = function (req, res) {
    try {
       
        userService.forgotPwd(req, function (forgotPwdResult) {

            if (forgotPwdResult != 'err') {
                /** remove comment when need send mail  
                 /******* mail code **/
                var link = constants.BASE_URL + "reset-password/" + forgotPwdResult.verificationToken;
                var templateVariable = {
                    templateURL: 'mailtemplate/success',
                    fullName: `${forgotPwdResult.firstName} ${forgotPwdResult.lastName}`,
                    link: link,
                    email: req.body.email,
                    BASE_URL: constants.BASE_URL,
                    API_VERSION: constants.API_VERSION,
                    layout: false
                };
                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: req.body.email,
                    subject: "Forgot Password"
                };
                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
                    } else {
                        return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Password reset mail sent successfully,please check your email.");
                    }
                });
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Invalid email address");
            }

        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}

//Check forgot password mail link authorize or not
exports.checkForgotPwdToken = function (req, res) {
    try {
        userService.checkForgotPwdToken(req, function (checkForgotPwdResult) {
            console.log("checkForgotPwdResult", checkForgotPwdResult)
            if (checkForgotPwdResult != 'err') {
                return res.render('forgotpwd', {BASE_URL:constants.BASE_URL,data:checkForgotPwdResult})
            } else {
                return res.render('error', { BASE_URL:constants.BASE_URL,data: 'Invalid token' })
            }

        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}

//reset password 
exports.resetPwd = function (req, res) {
    try {
        userService.resetPwd(req, function (resetPwdResult) {
            console.log("resetPwdResult", resetPwdResult);
            if (resetPwdResult != 'err') {
                return res.render('mailtemplate/resetPwdSuccess', { BASE_URL:constants.BASE_URL,passwordSucccessMsg: 'Password reset successfully' })
            } else {
                return res.render('error', { data: 'Invalid token' })
            }

        })
    } catch (exception) {
        return res.render('error', { data: 'Invalid token' })
    }
};

exports.logout = function (req, res, next) {
    try {
        userService.logout(req, function (err, logedout) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", err);
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Logout Successfully!");
            }
        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
};
