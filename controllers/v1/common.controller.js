const commonService = require('../../services/v1/common.service');
const cronService = require('../../services/v1/cron.service');
const constants = require('../../config/constants');
const sendResponse = require('../../modules/response.helper');
const _ = require('lodash');

exports.follow = function (req, res) {
    try {
        commonService.follow(req, function (err, followUser) {
            if (followUser) {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", followUser);
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", 'error');
            }
        })
    } catch (exception) {
        console.log(exception)
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}

exports.getCountries = function (req, res) {
    try {
        //Logic
        commonService.getCountries(req, function (getCountriesResult) {
            if (getCountriesResult != 'err') {
                return sendResponse.sendJsonResponse(req, res, 200, getCountriesResult, "0", "Countries fetched successfully.");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, [], "1", "No data found");
            }
        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}

exports.getCities = function (req, res) {
    try {

        commonService.getCities(req, function (err, citiesResult) {
            if (citiesResult) {
                return sendResponse.sendJsonResponse(req, res, 200, citiesResult, "0", "Cities fetched successfully.");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, [], "1", "No data found");
            }
        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}


//Update Password
exports.changePwd = function (req, res, next) {
    try {
        commonService.changePwd(req, function (err, newUser) {
            if (newUser) {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Password Changed Successfully!");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Password not changed");
            }
        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}

var getFollowers = function (req, res, next) {
    try {
        commonService.getFollowers(req, function (result) {
            if (result != "err") {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "List fetched Successfully!");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", 'error');
            }
        })
    } catch (exception) {
        console.log(exception)
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
};
exports.getFollowers = getFollowers;

var getFollowings = function (req, res, next) {
    try {
        commonService.getFollowings(req, function (result) {
            if (result != "err") {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "List fetched Successfully!");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", 'error');
            }
        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
};
exports.getFollowings = getFollowings;


var getAvatars = function (req, res) {
    try {
        commonService.getAvatars(req, function (err, getAvatarResult) {
            if (getAvatarResult != "err") {
                return sendResponse.sendJsonResponse(req, res, 200, getAvatarResult, "0", "Avatars fetched successfully!");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Not fetched avatars");
            }


        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error")
    }
}
exports.getAvatars = getAvatars;

var getUserPictures = function (req, res) {
    try {
        commonService.getUserPictures(req, function (err, userPictures) {
            if (userPictures != "err") {
                return sendResponse.sendJsonResponse(req, res, 200, userPictures, "0", "Pictures fetched Successfully!");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Not fetched user pictures");
            }
        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.getUserPictures = getUserPictures;


var getNotifications = function (req, res) {
    try {
        commonService.getNotifications(req, function (err, notificationsResult) {
            if (notificationsResult != 'err') {
                return sendResponse.sendJsonResponse(req, res, 200, notificationsResult, "0", "Notifications fetched Successfully!");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Notifications not  fetched successfully");
            }

        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "Error!");
    }
}
exports.getNotifications = getNotifications;

var getSearchResult = function (req, res) {
    try {
        commonService.getSearchResult(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Search Results not fetched");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Search Results fetched Successfully!");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "Error!");
    }
}
exports.getSearchResult = getSearchResult;

var getPersonalizedSearchResults = function (req, res) {
    try {
        commonService.getPersonalizedSearchResults(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", 'error');
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Personalized Search Results fetched Successfully!");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "Error!");
    }
}
exports.getPersonalizedSearchResults = getPersonalizedSearchResults;

var resendVerificationMail = function (req, res) {
    try {
        commonService.resendVerificationMail(req, res, function (err, result) {
            if (err) {
                if (err == "err") {
                    return sendResponse.sendJsonResponse(req, res, 201, {}, "1", 'error');
                } else {
                    return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Email is already verified.");
                }
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Verification Mail Sent Successfully!");

            }
        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "Error!");

    }
}
exports.resendVerificationMail = resendVerificationMail;

var clearAllNotifications = function (req, res) {
    try {
        commonService.clearAllNotifications(req, res, function (result) {
            if (result == "err") {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Notifications cleared Successfully!");

            }
        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "Error!");

    }
}
exports.clearAllNotifications = clearAllNotifications;

var getMeasurements = function (req, res) {
    try {
        commonService.getMeasurements(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Measurements fetched Successfully!");
            }
        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "Error!");
    }
}
exports.getMeasurements = getMeasurements;

var purchasePlan = function (req, res) {
    try {
        commonService.purchasePlan(req, res, function (purchasePlanResult) {

            if (purchasePlanResult == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Error");
            } else if (purchasePlanResult == 'err1') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "This program is not assigned to you");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, purchasePlanResult, "0", "Plan purchased successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Error");
    }
}
exports.purchasePlan = purchasePlan;

var addConversationId = function (req, res, next) {

    try {
        commonService.addConversation(req, res, function (req, res, convo) {
            if (convo != "err") {
                return sendResponse.sendJsonResponse(req, res, 200, convo, "0", "Conversation Id added successfully");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            }
        });

    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
};

exports.addConversationId = addConversationId;

var getConversationId = function (req, res, next) {

    try {
        commonService.getConversation(req, res, function (req, res, convo) {
            if (convo != "err") {
                return sendResponse.sendJsonResponse(req, res, 200, convo, "0", "Conversation fetched successfully");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            }
        });

    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
};

exports.getConversationId = getConversationId;

var getCalendarEvents = function (req, res) {
    try {
        commonService.getCalendarEvents(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Calendar Events fetched successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
}
exports.getCalendarEvents = getCalendarEvents;

var addFeed = function (req, res) {
    try {
        commonService.addFeed(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Post added successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
};
exports.addFeed = addFeed;

var likePost = function (req, res) {
    try {
        commonService.likePost(req, function (err, result) {
            if (err == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Error");
            } else if (err == 'err1') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Please follow this user to Like and Add Comments");
            } else if (err == 'err2') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Feed has been removed");
            }else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Post liked/unliked successfully !");
            }
        });
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
};
exports.likePost = likePost;

var addComment = function (req, res) {
    try {
        commonService.addComment(req, function (err, result) {
            if (err == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Error");
            } else if (err == 'err1') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Please follow this user to Like and Add Comments");
            } else if (err == 'err2') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Feed has been removed");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Comment added successfully !");
            }
        });
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
};
exports.addComment = addComment;

var editFeed = function (req, res) {
    try {
        commonService.editFeed(req, function (err, result) {
            if (err == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Error");
            } else if (err == 'err1') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Feed has been removed");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Post edited successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
};
exports.editFeed = editFeed;

var deleteFeed = function (req, res) {
    try {
        commonService.deleteFeed(req, function (err, result) {
            if (err == "err") {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Error");
            } else if (err == 'err1') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Feed has been already removed");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Post deleted successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
};
exports.deleteFeed = deleteFeed;

var getUserFeeds = function (req, res) {
    try {
        commonService.getUserFeeds(req, function (err, result) {
            if (err == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Error");
            } else if (err == 'err1') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Follow the user to view feed.");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Feeds fetched successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
};
exports.getUserFeeds = getUserFeeds;

var getFeedDetails = function (req, res) {
    try {
        commonService.getFeedDetails(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Error");
            } else {
                if (result == "") {
                    return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Feed has been removed");
                } else {
                    return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Feed details fetched successfully");
                }
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
};
exports.getFeedDetails = getFeedDetails;

var getFeedComments = function (req, res) {
    try {
        commonService.getFeedComments(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Feed comments fetched successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
};
exports.getFeedComments = getFeedComments;

var getFeeds = function (req, res) {
    try {
        commonService.getFeeds(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Feeds fetched successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
};
exports.getFeeds = getFeeds;

var getFollowersFollowings = function (req, res)
{
    try {
        commonService.getFollowersFollowings(req, function (err, result) {
            if (err == "err") {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "followers/following fetched successfully");
            }
        })
    } catch (exception)
    {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
}
exports.getFollowersFollowings = getFollowersFollowings;