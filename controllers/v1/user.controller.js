const userService = require('../../services/v1/user.service');
const mailer = require('../../modules/mailer.helper');
const constants = require('../../config/constants');
const sendResponse = require('../../modules/response.helper');
const _ = require('lodash');

exports.signup = function (req, res) {
    try {
        // Create a new user account using user service object.
        userService.signup(req, function (createdUser) {
            if (createdUser != "err") {
                if (createdUser != null) {
                    token = createdUser.verificationToken;
                }
                /******* mail code *******/

                var templateVariable = {
                    templateURL: "mailtemplate/registration",
                    fullName: `${req.body.firstName} ${req.body.lastName}`,
                    token: token,
                    BASE_URL: constants.BASE_URL,
                    API_VERSION: constants.API_VERSION,
                    layout: false
                };

                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: req.body.email,
                    subject: "Registration Successful."
                };
                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log('error', err);
                    } else {
                        console.log('Success sending email');
                    }
                });

                if (createdUser.profilePic != "") {
                    var saveImage = {
                        userId: createdUser._id,
                        image: createdUser.profilePic,
                        createdDate: _.now()
                    };

                    userService.saveUserImage(saveImage, function (data) {
                        if (data == "err") {
                            return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error saving user image");
                        } else {
                            return sendResponse.sendJsonResponse(req, res, 200, { _id: createdUser._id }, "0", "Registered successfully");
                        }
                    });
                } else {
                    return sendResponse.sendJsonResponse(req, res, 200, { _id: createdUser._id }, "0", "Registered successfully");
                }
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            }
        });
        /******** mail code *******/

    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
};

exports.addMeasurements = function (req, res, next) {
    try {
        userService.addMeasurement(req, function (err, measurements) {
            if (measurements) {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Measurements Added successfully!");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            }
        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
};

var updateUser = function (req, res) {
    try {
        userService.updateUser(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "User Updated successfully!!!");
            }
        });
    } catch (exception) {
        console.log(exception)
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
};
exports.updateUser = updateUser;

var saveDashboardData = function (req, res) {
    try {
        userService.saveDashboardData(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Dashboard data saved successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.saveDashboardData = saveDashboardData;

var getDashboardData = function (req, res) {
    try {
        userService.getDashboardData(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Dashboard data fetched successfully");
            }
        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.getDashboardData = getDashboardData;

var userDashboardsList = function (req, res) {
    try {
        userService.userDashboardsList(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "User Dashboards List fetched successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.userDashboardsList = userDashboardsList;

var updateUserSession = function (req, res) {
    try {
        userService.updateUserSession(req, function (err, result) {
            if (err) {
                if (err == "started") {
                    return sendResponse.sendJsonResponse(req, res, 401, {}, "1", "Session Already Started !");
                } else if (err == "ended") {
                    return sendResponse.sendJsonResponse(req, res, 401, {}, "1", "Session has been Completed !");
                } else if (err == "can't pause") {
                    return sendResponse.sendJsonResponse(req, res, 401, {}, "1", "Unfortunately, Session can't paused !");
                } else if (err == "can't end") {
                    return sendResponse.sendJsonResponse(req, res, 401, {}, "1", "Unfortunately, Session can't be completed !");
                }
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "User Session updated successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.updateUserSession = updateUserSession;

var getSessionDetails = function (req, res) {
    try {
        userService.getSessionDetails(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "User Session fetched successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.getSessionDetails = getSessionDetails;