const userService = require('../../services/v1/user.service');
const ptdtService = require('../../services/v1/ptdt.service');
const mailer = require('../../modules/mailer.helper');
const constants = require('../../config/constants');
const sendResponse = require('../../modules/response.helper');
const _ = require('lodash');
var fs = require('fs');
const path = require('path');
const mime = require('mime');
var pdf = require('dynamic-html-pdf');
exports.signup = function (req, res) {
    try {
        // Create a new user account using user service object.
        ptdtService.signup(req, function (createdUser) {
            console.log(createdUser);
            if (createdUser != "err") {
                if (createdUser != null) {
                    token = createdUser.verificationToken;
                }
                /******* mail code *******/

                var templateVariable = {
                    templateURL: "mailtemplate/registration",
                    fullName: req.body.firstName,
                    token: token,
                    BASE_URL: constants.BASE_URL,
                    API_VERSION: constants.API_VERSION,
                    layout: false
                };

                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: req.body.email,
                    subject: "Registration Successful."
                };
                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log('error', err);
                    } else {
                        console.log('Success sending email');
                    }
                });

                if (createdUser.profilePic != "") {
                    var saveImage = {
                        userId: createdUser._id,
                        image: createdUser.profilePic,
                        createdDate: _.now()
                    };

                    userService.saveUserImage(saveImage, function (data) {
                        if (data == "err") {
                            return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error saving user image");
                        } else {
                            return sendResponse.sendJsonResponse(req, res, 200, {_id: createdUser._id}, "0", "Registered successfully");
                        }
                    });
                } else {
                    return sendResponse.sendJsonResponse(req, res, 200, {_id: createdUser._id}, "0", "Registered successfully");
                }
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            }
        });
        /******** mail code *******/

    } catch (exception) {
        console.log("exception", exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
};

exports.addMeasurements = function (req, res, next) {
    try {
        userService.addMeasurement(req, function (err, measurements) {
            if (measurements) {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Measurements Added successfully!");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            }
        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
};

var updatePtdtDetails = function (req, res) {
    try {
        ptdtService.updatePtdtDetails(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "PTDT Information Updated successfully!!!");
            }
        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
};
exports.updatePtdtDetails = updatePtdtDetails;

var addTrainingProgram = function (req, res) {
    try {
        console.log("HELLO")
        ptdtService.addTrainingProgram(req, function (err, result) {
            if (err) {
                console.log(err)
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                ptdtService.addTrainingSessions(req, result._id, function (err1, data) {
                    if (err1) {
                        console.log(err1)
                        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
                    } else {
                        return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Training Program added successfully!!!");
                    }
                })
            }
        })
    } catch (exception) {
        console.log(exception)
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error")
    }
};
exports.addTrainingProgram = addTrainingProgram;

var addDietPlan = function (req, res, callback) {
    try {
        ptdtService.addDietPlan(req, function (result) {
            if (result != 'err') {
                console.log('Controller Result');
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Diet plan added successfully!!!");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Diet plan not added successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.addDietPlan = addDietPlan;

var editDietPlanMeals = function (req, res) {
    try {
        ptdtService.editDietPlanMeals(req, function (mealsResult) {
            if (mealsResult == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else if (mealsResult == 'err1') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Diet plan already purchased");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Meals edited successfully");
            }
        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
}
exports.editDietPlanMeals = editDietPlanMeals;

var deleteDietPlanMeals = function (req, res) {
    try {
        ptdtService.deleteDietPlanMeals(req, function (deleteMealsResult) {
            if (deleteMealsResult == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else if (deleteMealsResult == 'err1') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Diet plan already purchased");
            } else if (deleteMealsResult == 'err2') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Can't delete the one and only diet meal");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Meals deleted successfully");
            }
        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
}
exports.deleteDietPlanMeals = deleteDietPlanMeals;

var dietPlanMealsListing = function (req, res) {
    try {
        ptdtService.dietPlanMealsListing(req, function (dietMealsListing) {
            if (dietMealsListing == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, dietMealsListing, "0", "Successfully fetch dietMeals");
            }

        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
}
exports.dietPlanMealsListing = dietPlanMealsListing;


var dietPlanMealDetails = function (req, res) {
    try {
        ptdtService.dietPlanMealDetails(req, function (dietPlanDetailResult) {
            if (dietPlanDetailResult == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, dietPlanDetailResult, "0", "Successfully fetch dietMeals");
            }
        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
}
exports.dietPlanMealDetails = dietPlanMealDetails;

var updateTrainingProgram = function (req, res) {
    try {
        ptdtService.updateTrainingProgram(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", result.msg);
            }
        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.updateTrainingProgram = updateTrainingProgram;

var getAllProgramsList = function (req, res) {
    try {
        ptdtService.getAllProgramsList(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Fetched All Training Programs successfully!!!");
            }
        });
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.getAllProgramsList = getAllProgramsList;

var getProgramDetails = function (req, res) {
    try {
        ptdtService.getProgramDetails(req, function (err, result) {
            if (err == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else if (err == 'err1') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Sorry!, This program has been deleted");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Fetched Training Programs Details successfully!!!");
            }
        });
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.getProgramDetails = getProgramDetails;

var updateDietPlan = function (req, res) {
    try {
        ptdtService.updateDietPlan(req, function (updateDietPlanResult) {
            if (updateDietPlanResult == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", updateDietPlanResult.msg);
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }

}
exports.updateDietPlan = updateDietPlan;

var dietPlanDetail = function (req, res) {
    try {
        ptdtService.dietPlanDetail(req, function (dietPlanResult) {
            if (dietPlanResult !== 'err') {
                return sendResponse.sendJsonResponse(req, res, 200, dietPlanResult, "0", "Diet Plan Fetched Successfully");
            } else if (dietPlanResult !== 'err1') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Sorry!, This diet plan has been deleted");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
}
exports.dietPlanDetail = dietPlanDetail;

var allDietPlanDetails = function (req, res) {
    try {
        ptdtService.allDietPlanDetails(req, function (allPlanDetails) {
            if (allPlanDetails != 'err') {
                return sendResponse.sendJsonResponse(req, res, 200, allPlanDetails, "0", "Successfully fetched all diet plans");
            } else {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
}
exports.allDietPlanDetails = allDietPlanDetails

var deleteProgram = function (req, res) {
    try {
        ptdtService.deleteProgram(req, function (err, result) {
            if (err == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, err, "1", "error");
            } else if (err == 'err1') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Program already purchased");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Program deleted successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error")
    }
}
exports.deleteProgram = deleteProgram;

var deleteDietPlan = function (req, res) {
    try {
        ptdtService.deleteDietPlan(req, function (err, result) {
            if (err == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "User already purchased");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Diet Plan deleted successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error")
    }
}
exports.deleteDietPlan = deleteDietPlan;

var editSession = function (req, res) {
    try {
        ptdtService.editSession(req, function (err, result) {
            if (err == "err") {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else if (err == "err1") {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Program already purchased");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Session edited successfully");
            }
        });
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error")
    }
}
exports.editSession = editSession;

var deleteSession = function (req, res) {
    try {
        ptdtService.deleteSession(req, function (err, result) {
            if (err == "err") {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else if (err == "err1") {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Program already purchased");
            } else if (err == "err2") {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Can't delete the one and only session");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Session deleted successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error")
    }
}
exports.deleteSession = deleteSession;

var getSessions = function (req, res) {
    try {
        ptdtService.getSessions(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Sessions fetched successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error")
    }
}
exports.getSessions = getSessions;

var addTrainingClasses = function (req, res) {
    try {
        ptdtService.addTrainingClasses(req, function (result) {
            console.log("2", result);
            if (result == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error")
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Class added successfully")
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error")
    }
}
exports.addTrainingClasses = addTrainingClasses;

var editTrainingClasses = function (req, res) {
    try {
        ptdtService.editTrainingClasses(req, function (result) {
            if (result == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error")
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", result.msg)
            }
        })
    } catch (exception) {
        console.log(exception)
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error")
    }
}
exports.editTrainingClasses = editTrainingClasses;

var deleteTrainingClasses = function (req, res) {
    try {
        ptdtService.deleteTrainingClasses(req, function (result) {
            console.log("1", result)
            if (result == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "Class already purchased so can't be deleted")
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Successfully deleted class");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
}
exports.deleteTrainingClasses = deleteTrainingClasses;

var addTrainingWorkshop = function (req, res) {
    try {
        ptdtService.addTrainingWorkshop(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Workshop Created Successfully");
            }
        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.addTrainingWorkshop = addTrainingWorkshop;

var editTrainingWorkshop = function (req, res) {
    try {
        ptdtService.editTrainingWorkshop(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", result.msg);
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.editTrainingWorkshop = editTrainingWorkshop;

var deleteTrainingWorkshop = function (req, res) {
    try {
        ptdtService.deleteTrainingWorkshop(req, function (err, result) {
            console.log("Error.........1..........", err, result);
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Workshop deleted successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.deleteTrainingWorkshop = deleteTrainingWorkshop;

var addCustomizeProgram = function (req, res) {
    try {
        ptdtService.addCustomizeProgram(req, function (err, result) {
            console.log("err", err);
            if (err == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                ptdtService.addTrainingSessions(req, result._id, function (err1, data) {
                    if (err1) {
                        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
                    } else {
                        return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Customised Training Program added successfully!!!");
                    }
                })
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.addCustomizeProgram = addCustomizeProgram;

var editCustomizeProgram = function (req, res) {
    try {
        ptdtService.editCustomizeProgram(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", result.msg);
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.editCustomizeProgram = editCustomizeProgram;

var addCustomizeDietPlan = function (req, res) {
    try {
        ptdtService.addCustomizeDietPlan(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                ptdtService.addDietMeals(result._id, req, function (err, data) {
                    if (err) {
                        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
                    } else {
                        return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Customised Diet Plan added successfully!!!");
                    }
                })
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.addCustomizeDietPlan = addCustomizeDietPlan;

var editCustomizeDietPlan = function (req, res) {
    try {
        ptdtService.editCustomizeDietPlan(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", result.msg);
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.editCustomizeDietPlan = editCustomizeDietPlan;

var getUserPurchasePrograms = function (req, res) {
    try {
        ptdtService.getUserPurchasePrograms(req, function (result) {
            if (result == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "successfully fetched userplans");
            }
        })
    } catch (exception) {
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.getUserPurchasePrograms = getUserPurchasePrograms;

var getUserPurchaseDietPlan = function (req, res) {
    try {
        ptdtService.getUserPurchaseDietPlan(req, function (result) {
            console.log(result)
            if (result == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "successfully fetched userdietPlan");
            }
        })
    } catch (exception) {
        console.log(exception)
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
}
exports.getUserPurchaseDietPlan = getUserPurchaseDietPlan;

var downloadPersonalInformation = function (req, res) {
    var html = fs.readFileSync('views/demo.hbs', 'utf8');
    try {
        ptdtService.downloadPersonalInformation(req, function (result) {
            if (result == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                // console.log("result/////////",result)
                // console.log("result[2]",result[4]);
                var options = {
                    format: "A2",
                    orientation: "portrait",
                    border: "10mm"
                };
                var document = {
                    type: 'file', // 'file' or 'buffer'
                    template: html,
                    context: {
                        data: result[0],
                        programData: result[1],
                        customProgramData: result[2],
                        dietProgram: result[3],
                        customDietProgram: result[4],
                        classData: result[5],
                        workshopData: result[6],
                        BASE_URL: constants.BASE_URL,
                    },
                    path: "./public/pdf/userInformation/" + req.authenticationId + ".pdf"
                };
                pdf.create(document, options)
                        .then(ress => {
                            var a = constants.BASE_URL + "pdf/userInformation/" + req.authenticationId + ".pdf";
                            return sendResponse.sendJsonResponse(req, res, 200, a, "0", "successfully fetched userdietPlan");
                        })
                        .catch(error => {
                            console.error(error);
                            return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
                        });
            }
        })
    } catch (exception) {
        console.log(exception)
        return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
    }
}
exports.downloadPersonalInformation = downloadPersonalInformation;

exports.getSubscriptionPlan = function (req, res, next) {
    try {
        ptdtService.getSubscriptionPlan(req, function (resultScriptionPlan) {
            if (resultScriptionPlan == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, resultScriptionPlan, "0", "Fetch subscription plan");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}

exports.addSubscriptionPlan = function (req, res, next) {
    try {
        ptdtService.addSubscriptionPlan(req, function (resultScriptionPlan) {
            if (resultScriptionPlan == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, resultScriptionPlan, "0", "Fetch subscription plan");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}

var getProgramsDietsGoalsTypes = function (req, res) {
    try {
        ptdtService.getProgramsDietsGoalsTypes(req, function (err, result) {
            if (err) {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                return sendResponse.sendJsonResponse(req, res, 200, result, "0", "Data Fetched Successfully");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.getProgramsDietsGoalsTypes = getProgramsDietsGoalsTypes;

var generatePtdtInvoice = function (req, res) {
    try {
        ptdtService.generatePtdtInvoice(req, res, req.body.invoiceId, function (invoiceDetails) {
            if (invoiceDetails == 'err') {
                return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
            } else {
                console.log('Invoice Details...................', invoiceDetails);
                return sendResponse.sendJsonResponse(req, res, 200, {}, "0", "Invoice mail sent successfuly");
            }
        })
    } catch (exception) {
        console.log(exception);
        return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
    }
}
exports.generatePtdtInvoice = generatePtdtInvoice;

// exports.dummyData = function (req, res,next) {
//     try {
//         ptdtService.dummyData(req, function (resultuDummyData) {
//             if (resultuDummyData == 'err') {
//                 return sendResponse.sendJsonResponse(req, res, 201, {}, "1", "error");
//             }
//             else {
//                 return sendResponse.sendJsonResponse(req, res, 200, resultuDummyData, "0", "Successfully add ptdtSubscription Plan");
//             }
//         })
//     }
//     catch (exception) {
//         console.log(exception)
//         return sendResponse.sendJsonResponse(req, res, 201, exception, "1", "error");
//     }
// }
