const adminService = require('../services/admin.service');
const ptdtService = require('../services/v1/ptdt.service');
const CONST = require('../config/constants');
// const _ = require('lodash')
var moment = require('moment');
var _ = require("lodash");




const register = async (req, res, next) => {
    try {
        const userRegister = await adminService.register(req.body)
        res.send(userRegister)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.register = register

const adminLogin = async (req, res, next) => {
    try {
        var userLogin = await adminService.adminLogin(req.body, res)

        if (userLogin === true) {
            var obj = {}
            obj.username = userLogin.userName
            obj.id = userLogin._id
            req.session.isLoggedIn = true
            req.session.adminLogin = userLogin
            res.send(userLogin)
            //    res.redirect('dashboard');
            //   res.render('dashboard');
        } else {
            res.send(userLogin)
        }
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.adminLogin = adminLogin

const logout = async (req, res, next) => {
    try {
        req.session.isLoggedIn = null
        res.header('Cache-Control', 'no-cache');
        res.redirect('/login')
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.logout = logout

const dashboard = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'dashboard',
            pageTilte: 'Dashboard',
            BASE_URL: CONST.BASE_URL
        }

        let dashboardData = await adminService.getDashboard(res)

        renderParams.data = dashboardData
        res.render('admin/dashboard', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.dashboard = dashboard

const userList = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'userList',
            pageTilte: 'User List'
        }

        var typeObj = {
            type: 0,
            isDeleted: false
        }

        var userListData = await adminService.userList(typeObj, res)

        renderParams.data = userListData
        res.render('admin/userList', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.userList = userList

const userDeletedList = async (req, res, next) => {
    try {

        let renderParams = {
            layout: 'layouts/main',
            title: 'userDeletedList',
            pageTilte: 'User Deleted List',
            BASE_URL: CONST.BASE_URL
        }

        let typeObj = {
            type: 0,
            isDeleted: true
        }

        let userDeletedListData = await adminService.userList(typeObj)
        renderParams.data = userDeletedListData

        res.render('admin/userDeletedList', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.userDeletedList = userDeletedList



const personalTrainersList = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'personalTrainersList',
            pageTilte: 'Personal Trainers List'
        }
        var typeObj = {
            type: 2,
            isCertifiedAndApproved: 1,
            isDeleted: false
        }

        let personalTrainersListData = await adminService.userList(typeObj, res)

        renderParams.data = personalTrainersListData
        res.render('admin/personalTrainersList', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.personalTrainersList = personalTrainersList

const personalTrainersRegistrationRequests = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'personalTrainersRegistrationRequests',
            pageTilte: 'Personal Trainers Registration Requests'
        }

        let typeObj = {
            $and: [{
                    $or: [
                        {'isCertifiedAndApproved': 0},
                        {'isCertifiedAndApproved': 2}]
                }, {type: 2}]
        }

        let ptRequestListData = await adminService.userList(typeObj, res)

        renderParams.data = ptRequestListData
        res.render('admin/personalTrainersRegistrationRequests', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.personalTrainersRegistrationRequests = personalTrainersRegistrationRequests

const personalTrainersDeletedList = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'personalTrainersDeletedList',
            pageTilte: 'Personal Trainers Deleted List',
            BASE_URL: CONST.BASE_URL

        }

        let typeObj = {
            type: 2,
            isDeleted: true
        }

        let ptDeletedListData = await adminService.userList(typeObj, res)

        renderParams.data = ptDeletedListData
        res.render('admin/personalTrainersDeletedList', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.personalTrainersDeletedList = personalTrainersDeletedList

const dietitiansList = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'dietitiansList',
            pageTilte: 'Dietitians List'
        }

        const typeObj = {
            type: 1,
            isCertifiedAndApproved: 1,
            isDeleted: false
        }

        let dtListData = await adminService.userList(typeObj, res)

        renderParams.data = dtListData

        res.render('admin/dietitiansList', renderParams)
    } catch (exception) {
        res.send(exception)
    }
}
exports.dietitiansList = dietitiansList



const payments = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'payments',
            pageTilte: 'payments'
        }

        let userList = await adminService.ptdtUpcomingPaymentList(function (userList) {
            // console.log("userList", userList)
            // let dtListData = await adminService.userList(typeObj, res)

            renderParams.upcomingPayment = userList[0];

            renderParams.upcomingPaymentAmount = userList[0].length <= 0 ? 0 : userList[0][0].total.toFixed(2)
            renderParams.completedPayment = userList[1];
            renderParams.completedPaymentAmount = userList[1].length <= 0 ? 0 : userList[1][0].total.toFixed(2)
            renderParams.totalEarningPayment = userList[2];
            renderParams.totalEarningPaymentAmount = userList[2].length <= 0 ? 0 : userList[2][0].totalAmountOfEarning.toFixed(2)
            console.log("userList[2]", userList[2]);
            res.render('admin/payments', renderParams)
        });
    } catch (exception) {
        res.send(exception)
    }
}
exports.payments = payments

const paidByAdmin = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'paidByAdmin',
            pageTilte: 'paidByAdmin'
        }
        var obj = {
            id: req.params.id
        }
        var updatePaidByAdminFlag = await adminService.ptdtPaidByAdminFlag(obj);
        res.redirect('/payments');
    } catch (exception) {
        res.send(exception);
    }
}
exports.paidByAdmin = paidByAdmin;

const dietitiansRegistrationRequests = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'dietitiansRegistrationRequests',
            pageTilte: 'Dietitians Registration Requests'
        }

        let typeObj = {
            $and: [{
                    $or: [
                        {'isCertifiedAndApproved': 0},
                        {'isCertifiedAndApproved': 2}]
                }, {type: 1}]
        }

        let dtListData = await adminService.userList(typeObj, res)

        renderParams.data = dtListData

        res.render('admin/dietitiansRegistrationRequests', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.dietitiansRegistrationRequests = dietitiansRegistrationRequests

const dietitiansDeletedList = async (req, res, next) => {
    try {
        let renderParams = {
            layout: 'layouts/main',
            title: 'dietitiansDeletedList',
            pageTilte: 'Dietitians Deleted List',
            BASE_URL: CONST.BASE_URL
        }

        let typeObj = {
            type: 1,
            isDeleted: true
        }

        let ptDeletedListData = await adminService.userList(typeObj, res)

        renderParams.data = ptDeletedListData
        res.render('admin/dietitiansDeletedList', renderParams)

    } catch (exception) {
        res.send(exception.message)
    }
}

exports.dietitiansDeletedList = dietitiansDeletedList

const financeStatistics = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'financeStatistics',
            pageTilte: 'Finance Statistics'
        }

        let getFinanceStatistics = await adminService.getFinanceStatistics(null, res)
        console.log("_.sortBy(getFinanceStatisticss, 'date')", _.sortBy(getFinanceStatistics, 'date'))
        renderParams.data = JSON.stringify(_.sortBy(getFinanceStatistics, 'date'));
        console.log(">>>>>>>>>>>.", renderParams.data)
        res.render('admin/financeStatistics', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.financeStatistics = financeStatistics

const participantsStatistics = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'participantsStatistics',
            pageTilte: 'Participants Statistics'
        }

        res.render('admin/dashboard', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.participantsStatistics = participantsStatistics

const deviceStatistics = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main'
        }

        res.render('admin/dashboard', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.deviceStatistics = deviceStatistics

const citiesStatistics = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main'
        }

        res.render('admin/dashboard', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.citiesStatistics = citiesStatistics

const complaintsManagement = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'complaintsManagement',
            pageTilte: 'Complaints Management'
        }

        res.render('admin/complaintsManagement', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.complaintsManagement = complaintsManagement

const measurementsManagement = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'measurementsManagement',
            pageTilte: 'Measurements Management'
        }

        res.render('admin/measurementsManagement', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.measurementsManagement = measurementsManagement

const ptSubscriptionList = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'ptSubscriptionList',
            pageTilte: 'PT Subscription List'
        }

        res.render('admin/ptSubscriptionList', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.ptSubscriptionList = ptSubscriptionList

const dtSubscriptionList = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'dtSubscriptionList',
            pageTilte: 'DT Subscription List'
        }

        res.render('admin/dtSubscriptionList', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.dtSubscriptionList = dtSubscriptionList

const dietitiansDietPlan = async (req, res, next) => {
    // var dietPlanDetail=await adminService.dietitiansDietPlan(userObj,res);
    var renderParams = {
        layout: 'layouts/main',
        title: 'dietitiansDietPlan',
        pageTilte: 'DT dietitiansDietPlan List',
        BASE_URL: CONST.BASE_URL
    }

    var obj = {
        _id: req.params.id,
        dietType: 4
    }
    var dietPlanResult = await adminService.dietitiansDietPlan(obj);
    renderParams.data = req.params.id;
    renderParams.dietPlanResult = dietPlanResult;
    res.render('admin/dietitiansDietPlan', renderParams)
}
exports.dietitiansDietPlan = dietitiansDietPlan;

const dietitiansCustomizeDietPlan = async (req, res, next) => {
    var renderParams = {
        layout: 'layouts/main',
        title: 'dietitiansCustomizeDietPlan',
        pageTilte: 'DT dietitiansDietPlan List',
        BASE_URL: CONST.BASE_URL
    }

    var obj = {
        _id: req.params.id,
        dietType: 5
    }
    var customizeDietPlanResult = await adminService.dietitiansCustomizeDietPlan(obj);
    renderParams.data = req.params.id;
    renderParams.customizeDietPlanResult = customizeDietPlanResult;
    res.render('admin/dietitiansCustomizeDietPlan', renderParams)
}
exports.dietitiansCustomizeDietPlan = dietitiansCustomizeDietPlan;

const cmsPages = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'cmsPages',
            pageTilte: 'CMS Pages',
            BASE_URL: CONST.BASE_URL
        }

        res.render('admin/cmsPages', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.cmsPages = cmsPages

const settings = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'settings',
            pageTilte: 'Settings'
        }

        res.render('admin/setting', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.settings = settings

const aboutUs = async (req, res, next) => {
    let renderParams = {
        layout: 'layouts/cmsPages',
        pageTitle: 'About Us',
        BASE_URL: CONST.BASE_URL,
    }

    cmsObj = {
        pageType: 'aboutUs'
    }

    let aboutUsData = await adminService.getCMSPagesDetails(cmsObj)
    renderParams.data = (aboutUsData.details).toString();

    res.render('admin/aboutUs', renderParams)
}

exports.aboutUs = aboutUs

const privacyPolicy = async (req, res, next) => {
    let renderParams = {
        layout: 'layouts/cmsPages',
        pageTitle: 'Privacy And Policy',
        BASE_URL: CONST.BASE_URL,
    }

    cmsObj = {
        pageType: 'privacyPolicy'
    }

    let privacyPolicyData = await adminService.getCMSPagesDetails(cmsObj)

    renderParams.data = (privacyPolicyData.details).toString();
    res.render('admin/privacyPolicy', renderParams)
}

exports.privacyPolicy = privacyPolicy

const termsAndConditions = async (req, res, next) => {
    let renderParams = {
        layout: 'layouts/cmsPages',
        pageTitle: 'Terms And Conditions',
        BASE_URL: CONST.BASE_URL,
    }

    cmsObj = {
        pageType: 'termsAndCondition'
    }

    let termsAndConditionsData = await adminService.getCMSPagesDetails(cmsObj)

    renderParams.data = (termsAndConditionsData.details).toString();
    res.render('admin/termsAndConditions', renderParams)
}

exports.termsAndConditions = termsAndConditions

const userDetails = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            BASE_URL: CONST.BASE_URL,
            title: 'userDetails',
            pageTilte: 'User Details'

        }
        var userObj = {
            _id: req.params.id
        }

        let userDetailsData = await adminService.personalDetails(userObj, res)
        renderParams.data = userDetailsData
        res.render('admin/userDetails', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.userDetails = userDetails

const userDeletedDetails = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            BASE_URL: CONST.BASE_URL,
            title: 'userDetails',
            pageTilte: 'User Details'

        }
        var userObj = {
            _id: req.params.id
        }

        let userDetailsData = await adminService.personalDetails(userObj, res)
        renderParams.data = userDetailsData

        res.render('admin/userDeletedDetails', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.userDeletedDetails = userDeletedDetails

const personalTrainersDetails = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'personalTrainersDetails',
            BASE_URL: CONST.BASE_URL,
            pageTilte: 'Personal Trainers Details'
        }

        var userObj = {
            _id: req.params.id
        }

        let personalTrainersDetailsData = await adminService.personalDetails(userObj)
        renderParams.data = personalTrainersDetailsData

        res.render('admin/personalTrainersDetails', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.personalTrainersDetails = personalTrainersDetails

const personalTrainersRequestDetails = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'personalTrainersRequestDetails',
            BASE_URL: CONST.BASE_URL,
            pageTilte: 'Personal Trainers RequestDetails'
        }

        let userObj = {
            _id: req.params.id
        }

        let ptrdData = await adminService.personalDetails(userObj)

        renderParams.data = ptrdData
        res.render('admin/personalTrainersRequestDetails', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.personalTrainersRequestDetails = personalTrainersRequestDetails

const personalTrainersDeletedDetails = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'personalTrainersDeletedDetails',
            BASE_URL: CONST.BASE_URL,
            pageTilte: 'Personal Trainers DeletedDetails'
        }

        let userObj = {
            _id: req.params.id
        }

        let ptrdData = await adminService.personalDetails(userObj, res)

        renderParams.data = ptrdData
        res.render('admin/personalTrainersDeletedDetails', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.personalTrainersDeletedDetails = personalTrainersDeletedDetails

const dietitiansDetails = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'dietitiansDetails',
            BASE_URL: CONST.BASE_URL,
            pageTilte: 'Dietitians Details'
        }

        let userObj = {
            _id: req.params.id
        }

        let ptrdData = await adminService.personalDetails(userObj)

        renderParams.data = ptrdData

        res.render('admin/dietitiansDetails', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.dietitiansDetails = dietitiansDetails

const dietitiansRegistrationRequestsDetails = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'dietitiansRegistrationRequestsDetails',
            pageTilte: 'Dietitians Registration RequestsDetails',
            BASE_URL: CONST.BASE_URL
        }

        let userObj = {
            _id: req.params.id
        }

        let dietitiansData = await adminService.personalDetails(userObj)

        renderParams.data = dietitiansData

        res.render('admin/dietitiansRegistrationRequestsDetails', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.dietitiansRegistrationRequestsDetails = dietitiansRegistrationRequestsDetails

const dietitiansDeletedDetails = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'dietitiansDeletedDetails',
            BASE_URL: CONST.BASE_URL,
            pageTilte: 'Dietitians Deleted Details'
        }

        let userObj = {
            _id: req.params.id
        }

        let ptrdData = await adminService.personalDetails(userObj, res)

        renderParams.data = ptrdData
        res.render('admin/dietitiansDeletedDetails', renderParams)

    } catch (exception) {
        res.send(exception.message)
    }
}

exports.dietitiansDeletedDetails = dietitiansDeletedDetails

const complaintsManagementViewDetails = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'complaintsManagementViewDetails',
            pageTilte: 'Complaints Management View Details',
            BASE_URL: CONST.BASE_URL
        }

        res.render('admin/complaintsManagementViewDetails', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}
exports.complaintsManagementViewDetails = complaintsManagementViewDetails

const editAboutUs = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'editAboutUs',
            pageTilte: 'Edit About Us',
            BASE_URL: CONST.BASE_URL
        }

        let cmsObj = {
            pageType: 'aboutUs'
        }
        let editAboutUs = await adminService.getCMSPages(cmsObj)
        renderParams.data = editAboutUs
        res.render('admin/editAboutUs', renderParams)

    } catch (exception) {
        res.send(exception)
    }
}
exports.editAboutUs = editAboutUs


const editPrivacyPolicy = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'editPrivacyPolicy',
            pageTilte: 'Edit Privacy Policy',
            BASE_URL: CONST.BASE_URL
        }

        let cmsObj = {
            pageType: 'privacyPolicy'
        }

        let editAboutUs = await adminService.getCMSPages(cmsObj)
        renderParams.data = editAboutUs
        res.render('admin/editPrivacyPolicy', renderParams)

    } catch (exception) {
        res.send(exception, message)
    }
}
exports.editPrivacyPolicy = editPrivacyPolicy

const editTermsAndCoditions = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'editTermsAndConditions',
            pageTilte: 'Edit Terms And Conditions',
            BASE_URL: CONST.BASE_URL
        }

        let cmsObj = {
            pageType: 'termsAndCondition'
        }

        let editAboutUs = await adminService.getCMSPages(cmsObj)
        renderParams.data = editAboutUs
        res.render('admin/editTermsAndCoditions', renderParams)

    } catch (exception) {
        res.send(exception, message)
    }
}
exports.editTermsAndCoditions = editTermsAndCoditions

const createPTSubscription = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'createPTSubscription',
            pageTilte: 'Create PT Subscription',
            BASE_URL: CONST.BASE_URL
        }

        res.render('admin/createPTSubscription', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.createPTSubscription = createPTSubscription

const createDTSubscription = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'createDTSubscription',
            pageTilte: 'Create DT Subscription',
            BASE_URL: CONST.BASE_URL
        }
        res.render('admin/createDTSubscription', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.createDTSubscription = createDTSubscription

const viewPTSubscription = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'viewPTSubscription',
            pageTilte: 'View PT Subscription',
            BASE_URL: CONST.BASE_URL
        }

        res.render('admin/viewPTSubscription', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.viewPTSubscription = viewPTSubscription

const viewDTSubscription = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'viewDTSubscription',
            pageTilte: 'View DT Subscription',
            BASE_URL: CONST.BASE_URL
        }

        res.render('admin/viewDTSubscription', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.viewDTSubscription = viewDTSubscription

const editPTSubscription = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'editPTSubscription',
            pageTilte: 'Edit DT Subscription',
            BASE_URL: CONST.BASE_URL
        }

        res.render('admin/editPTSubscription', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.editPTSubscription = editPTSubscription

const editDTSubscription = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'editDTSubscription',
            pageTilte: 'Edit DT Subscription',
            BASE_URL: CONST.BASE_URL
        }

        res.render('admin/editDTSubscription', renderParams)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.editDTSubscription = editDTSubscription

const resetPassword = async (req, res, next) => {
    try {
        const changePasswordData = await adminService.changePassword(req.body, res)
        console.log('changePasswordData...', changePasswordData)

        res.send(changePasswordData)
    } catch (exception) {
        res.send(exception)
    }
}

exports.resetPassword = resetPassword

const userStatusChange = async (req, res, next) => {
    try {
        let statusChangeData = await adminService.userStatusChange(req, req.body, res)

        if (req.body.page === 'userList') {
            res.redirect('userList')
        } else {
            res.redirect(req.body.page + '/' + req.body.userId)
        }
    } catch (exception) {
        res.send(exception)
    }
}

exports.userStatusChange = userStatusChange

const certificateApproveDecline = async (req, res, next) => {
    try {

        let certificateApproveDeclineUpdate = await adminService.certificateApproveDecline(req, req.body, res)
        res.redirect(req.body.page)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.certificateApproveDecline = certificateApproveDecline

const restoreAccount = async (req, res, next) => {
    try {
        let restoreAccount = await adminService.restoreAccount(req.body, res)

        res.redirect(req.body.page)
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.restoreAccount = restoreAccount

const saveCMSInformation = async (req, res, next) => {
    try {

        let saveData = await adminService.saveCMSInformation(req.body, res)

        res.redirect('/cmsPages')
    } catch (exception) {
        res.send(exception.message)
    }
}

exports.saveCMSInformation = saveCMSInformation

const userPlans = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'userPlans',
            BASE_URL: CONST.BASE_URL,
            pageTilte: 'User Plans'
        }

        let userPlans = await adminService.userPlans(req.params.id, res)

        renderParams.data = userPlans
        renderParams.userId = req.params.id

        res.render('admin/userPlans', renderParams)

    } catch (exception) {
        res.send(exception.message)
    }
}

exports.userPlans = userPlans


const personalTrainersPrograms = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'personalTrainersPrograms',
            BASE_URL: CONST.BASE_URL,
            pageTilte: 'personalTrainersPrograms'
        }

        let typeObj = {
            createdBy: req.params.id,
            trainingType: 0
        }

        let ptPlans = await adminService.personalTrainersPlans(typeObj, res)

        renderParams.data = ptPlans
        renderParams.ptId = req.params.id

        res.render('admin/personalTrainersPrograms', renderParams)

    } catch (exception) {
        res.send(exception.message)
    }
}

exports.personalTrainersPrograms = personalTrainersPrograms

const personalTrainersCustomPrograms = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'personalTrainersCustomPrograms',
            BASE_URL: CONST.BASE_URL,
            pageTilte: 'personalTrainersCustomPrograms'
        }


        let typeObj = {
            createdBy: req.params.id,
            trainingType: 3
        }

        let ptPlans = await adminService.personalTrainersPlans(typeObj, res)

        renderParams.data = ptPlans
        renderParams.ptId = req.params.id

        res.render('admin/personalTrainersCustomPrograms', renderParams)

    } catch (exception) {
        res.send(exception.message)
    }
}

exports.personalTrainersCustomPrograms = personalTrainersCustomPrograms

const personalTrainersClass = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'personalTrainersClass',
            BASE_URL: CONST.BASE_URL,
            pageTilte: 'User Plans'
        }

        let typeObj = {
            createdBy: req.params.id,
            trainingType: 1
        }

        let ptPlans = await adminService.personalTrainersPlans(typeObj, res)

        renderParams.data = ptPlans
        renderParams.ptId = req.params.id

        res.render('admin/personalTrainersClass', renderParams)

    } catch (exception) {
        res.send(exception.message)
    }
}

exports.personalTrainersClass = personalTrainersClass

const personalTrainersWorkshops = async (req, res, next) => {
    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'personalTrainersWorkshops',
            BASE_URL: CONST.BASE_URL,
            pageTilte: 'User Plans'
        }

        let typeObj = {
            createdBy: req.params.id,
            trainingType: 2
        }

        let ptPlans = await adminService.personalTrainersPlans(typeObj, res)

        renderParams.data = ptPlans
        renderParams.ptId = req.params.id
        res.render('admin/personalTrainersWorkshops', renderParams)

    } catch (exception) {
        res.send(exception.message)
    }
}

exports.personalTrainersWorkshops = personalTrainersWorkshops



const financeStatisticsDate = async (req, res) => {

    try {
        var renderParams = {
            layout: 'layouts/main',
            title: 'financeStatistics',
            pageTilte: 'Finance Statistics'
        }

        let getFinanceStatisticss = await adminService.getFinanceStatistics(req.body, res)

        console.log("getFinanceStatistics....", getFinanceStatisticss)

        // renderParams.data = getFinanceStatisticss
        // console.log("renderParams.data",renderParams.data)
        // 
        // console.log("getFinanceStatistics....", getFinanceStatisticss)
        renderParams.data = JSON.stringify({"data": getFinanceStatisticss})
        //res.render('admin/financeStatistics', renderParams)
        console.log("_.sortBy(getFinanceStatisticss, 'date')", _.sortBy(getFinanceStatisticss, 'date'))
        res.write(JSON.stringify({
            "data": _.sortBy(getFinanceStatisticss, 'date'),
            // "draw": req.draw,
            // "recordsTotal": (arrReports) ? arrReports.length : 0,
            // "recordsFiltered": (arrReports) ? arrReports.length : 0
        }));
        res.end();

    } catch (exception) {
        console.log("exception", exception);
        res.send(exception.message)
    }
}
exports.financeStatisticsDate = financeStatisticsDate

var updateCommision = async (req, res) => {
    try {
        var obj = {
            id: req.params.id,
            commission: req.body.commission
        }

        let updateCommisionVar = await adminService.updateCommisionData(obj);
        res.send(updateCommisionVar);
        // res.write(JSON.stringify({
        //   "data": updateCommisionVar
        //   // "draw": req.draw,
        //   // "recordsTotal": (arrReports) ? arrReports.length : 0,
        //   // "recordsFiltered": (arrReports) ? arrReports.length : 0
        // }));
    } catch (exception) {
        console.log("exception", exception);
        res.send(exception.message)
    }
}
exports.updateCommision = updateCommision;

const plans = async (req, res, next) => {
    try {
        ptdtService.getAllSubscriptionPlan("2", function (ptScriptionPlan) {            
            var renderParams = {
                layout: 'layouts/main',
                title: 'plan',
                pageTilte: 'plan',
                ptPlan : ptScriptionPlan
            };
            res.render('admin/plan', renderParams);
        });
    } catch (exception) {
        res.send(exception.message);
    }
}
exports.plans = plans;

const dtplans = async (req, res, next) => {
    try {
        ptdtService.getAllSubscriptionPlan("1", function (ptScriptionPlan) {            
            var renderParams = {
                layout: 'layouts/main',
                title: 'plan',
                pageTilte: 'plan',
                ptPlan : ptScriptionPlan
            };
            res.render('admin/dtplan', renderParams);
        });
    } catch (exception) {
        res.send(exception.message);
    }
}
exports.dtplans = dtplans;

var updatePlan = async (req, res) => {
    try {             
        let updatePlanData = await adminService.updatePlanData(req.body);        
        res.send(updatePlanData);       
    } catch (exception) {
        console.log("exception", exception);
        res.send(exception.message)
    }
}
exports.updatePlan = updatePlan;