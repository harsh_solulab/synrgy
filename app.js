var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mailer = require('express-mailer');
var constants = require('./config/constants');
var session = require('express-session');
var mongoose = require('mongoose');
var expressHbs = require('express-handlebars');
var cron = require('node-cron');
var handleBarHelpers = require('./modules/handlebar.helper');

const cronService = require('./services/v1/cron.service');
/************ Database Connection *************/
mongoose.set('useCreateIndex', true)
mongoose.set("debug", true); // debugging is on        
mongoose.connect('mongodb://127.0.0.1:27017/synrgy', { useNewUrlParser: true })
    .then(() => {
        console.log(`Succesfully Connected to the Mongodb Database at URL : mongodb://127.0.0.1:27017/synrgy`);
    })
    .catch(() => {
        console.log(`Error Connecting to the Mongodb Database at URL : mongodb://127.0.0.1:27017/synrgy`);
    });

/************ Database Connection *************/

var index = require('./routes/index');
var users = require('./routes/users');

// Get the API routes.
var apiRoutes = require('./routes/api.routes');


var app = express();

app.use(session({
    secret: 'ssssssssshhhhhhhhhhhhhhhhhh',
    resave: false,
    saveUninitialized: true,
    cookie: {
        path: '/',
        httpOnly: false,
        secure: false,
        maxAge: 31536000000
    }
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public/assets/images', 'favicon.png')));
app.use(logger('dev'));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);
app.use('/api', apiRoutes);

// mailer configuration
mailer.extend(app, {
    from: constants.MAIL_FROM,
    host: constants.MAIL_HOST, // hostname 
    secureConnection: constants.MAIL_SECURE, // use SSL 
    port: constants.MAIL_PORT, // port for secure SMTP 
    transportMethod: constants.MAIL_METHOD, // default is SMTP. Accepts anything that nodemailer accepts 
    auth: {
        user: constants.MAIL_FROM_AUTH,
        pass: constants.MAIL_PASSWORD
    }
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

//crone-job for check dietplan expiry uncomment if needed
cron.schedule('*/30 * * * *', function (req, res, err) {
    cronService.checkDietPlanExpiry(req, function (data) {
        if (data == 'err') {
            console.log("Error");
        } else {
            console.log(data);
        }
    });
});
cron.schedule('*/15 * * * *', function (req, res, err) {
    cronService.userSessionCompleted(req, function (data) {
        if (data == 'err') {
            console.log("Error");
        } else {
            console.log(data);
        }
    })
})
cron.schedule('10 * * * * *', function (req, res, err) {
    cronService.workShopClassExpiry(req, function (data) {
        if (data == 'err') {
            console.log("error");
        } else {
            console.log(data);
        }
    });
    cronService.ptdtSubscriptionExpiry(req, function (data) {
        if (data == 'err') {
            console.log("error");
        } else {
            console.log(data);
        }
    });
    cronService.updateUserSessionExpiry(req, function (data) {
        if (data == 'err') {
            console.log("error");
        } else {
            console.log(data);
        }
    });
    cronService.deleteFreezedAccount(req, function (data) {
        if (data == 'err') {
            console.log("error");
        } else {
            console.log(data);
        }
    })
    cronService.permanentDeleteAccount(req, function (data) {
        if (data == 'err') {
            console.log("error");
        } else {
            console.log(data);
        }
    })
})
cron.schedule('* * * * *', function (req, res, err) {
    cronService.sendClassNotification(req, function (data) {
        if (data == 'err') {
            console.log("error");
        } else {
            console.log(data);
        }
    });
    cronService.planExpirationNotification(req, function (data) {
        if (data == 'err') {
            console.log("error");
        } else {
            console.log(data);
        }
    });
    cronService.dietMealsExpiry(req, function (data) {
        if (data == 'err') {
            console.log("error");
        } else {
            console.log(data);
        }
    });
})
// view engine setup
app.engine('hbs', expressHbs({
    //  defaultLayout: 'layout',
    extname: '.hbs',
    helpers: handleBarHelpers.helperFunction,
    layoutsDir: path.join(__dirname, 'views'),
    partialsDir: [
        __dirname + '/views/partials',
    ]
}));
app.set('view engine', '.hbs');
app.set('views', path.join(__dirname, './views'));

module.exports = app;